function alertRamsesNon(){
	Swal.fire({
  		title: 'Error!',
  		text: 'Do you want to continue',
  		icon: 'error',
  		confirmButtonText: 'Cool'
	})
}


function alertDeleteElement(id,path) {
	// body...
  // alert(path);
const swalWithBootstrapButtons = Swal.mixin({
  customClass: {
    confirmButton: 'btn btn-success',
    cancelButton: 'btn btn-danger'
  },
  buttonsStyling: true
})

swalWithBootstrapButtons.fire({
  title: 'Confirmer La Suppression?',
  text: "Cette action est irreversible!",
  icon: 'warning',
  width: 600,
  padding: '3em',
  backdrop: `
        rgba(68,68,68,0.7)
        `
    ,

  showCancelButton: true,
  confirmButtonText: 'Oui, Supprimer!',
  cancelButtonText: 'Non!',
  reverseButtons: true
}).then((result) => {

  if (result.isConfirmed) {
    var data = {
      "_token": $('input[name=_token]').val(),
      "id": id,
    }
    $.ajax({
      type: "DELETE",
      url: path,
      data: data,
      success: function(response){
        swalWithBootstrapButtons.fire(
          response['status'],
          '....',
          response['type'],
        ).then((result) => {
          if(response['type'] !== 'error'){
            location.reload();
          }
        });
      }
    });
  } else if (
    /* Read more about handling dismissals below */
    result.dismiss === Swal.DismissReason.cancel
  ) {
    swalWithBootstrapButtons.fire(
      'Annulé',
      'Suppression annulée',
      'error'
    )
  }
});

}



$(document).ready(function() {
    $('.js-example-basic-multiple').select2({
    	 width: 'resolve'
    });
});



$(function(){
    // var dtToday = new Date();
    
    // var month = dtToday.getMonth() + 1;
    // var day = dtToday.getDate();
    // var year = dtToday.getFullYear();
    // if(month < 10)
    //     month = '0' + month.toString();
    // if(day < 10)
    //     day = '0' + day.toString();
    
    // var maxDate = year + '-' + month + '-' + day;
    // $('.dateramses').attr('min', maxDate);
});


// function isTel() {
//   var values = document.getElementsByClassName("numero");
//   for (var i = values.length - 1; i >= 0; i--) {
//     if(values[i].value !== null && !('/^(237)?(6|2)(5|6|7|8|9)[0-9]{7}$/'.test(values[i].values))){
//       alert('correct');

//     }
//   }
//   return false;
// }

$(document).ready(function() {
  $("#success-alert").hide();
  $("#myWish").click(function showAlert() {
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
      $("#success-alert").slideUp(500);
    });
  });
});



function addEvenement() {
  // body...
  var title = document.getElementById('title').value;
  var start = document.getElementById('start').value;
  var end = document.getElementById('end').value;
  var background = document.getElementById('background').value;
  if(title.trim() === "" || start.trim() === ""){
    document.getElementById('error').innerHTML = "Remplissez tout les champs marquer en rouge";
  }

   var data = {
      "_token": $('input[name=_token]').val(),
      "title": title,
      "start": start,
      "end": end,
      'backgroundColor': background, //red
      'borderColor'    : background, //red
      'allDay'         : 1
    }

    $.ajax({
      type: "POST",
      url: '/admin/addEvent',
      data: data,
      success: function(response){
        Swal.fire({
          icon: 'success',
          title : response['status'],
          showConfirmButton: true,
          timer: 4000
        }).then((result) => {
            location.reload();
        });
      }
    });
}