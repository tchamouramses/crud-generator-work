

$('.toast').toast(option);

$(document).ready(function() {
        alert("Settings page was loaded");
    });
function text() {
  toastr.success("{{session('flash_message')}}");
}




function confirmation() {
  // body...
  var elements = document.getElementsByClassName('getvalue');
  var value = [];
  console.log(elements);
  for (var i = 0; i < elements.length; i++) {
    if(elements[i].checked)
      value.push(elements[i].value)
  }

  console.log(value);
  if (value.length !== 0) {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: true
    })

    swalWithBootstrapButtons.fire({
      title: 'Confirmer La Suppression?',
      text: "Cette action est irreversible!",
      icon: 'warning',

      width: 600,
      padding: '3em',
      backdrop: `
            rgba(68,68,68,0.7)
            `
        ,

      showCancelButton: true,
      confirmButtonText: 'Oui, Supprimé!',
      cancelButtonText: 'Non Annulé!',
      reverseButtons: true
    }).then((result) => {

      if (result.isConfirmed) {
        var data = {
          "_token": $('input[name=_token]').val(),
          "value": value,
        }
        $.ajax({
          type: "POST",
          url: "/admin/event/delete",
          data: data,
          success: function(response){
            swalWithBootstrapButtons.fire(
              response['status'],
              '....',
              'success',
            ).then((result) => {
                location.reload();
            });
          }
        });
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Annulé',
          'Suppression annulée',
          'error'
        )
      }
    });










  }else{
    Swal.fire({
      icon: 'error',
      // title: 'Mise A Jour De l\'Agenda Effectuée Avec Succes',
      title : 'selectioner un evenement avant de supprimer',
      showConfirmButton: true,
      timer: 4000
    })
  return false;
  }
}

