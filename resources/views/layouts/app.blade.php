<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>IMMOB 237</title>



  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&amp;display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ url('assets/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{{ url('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ url('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ url('assets/plugins/jqvmap/jqvmap.min.css') }}">

  <!-- Theme style -->
  <link rel="stylesheet" href="{{ url('assets/dist/css/adminlte.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ url('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ url('assets/plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ url('assets/plugins/summernote/summernote-bs4.min.css') }}">

  <link rel="stylesheet" href="{{ url('assets/plugins/toastr/toastr.min.css') }}">
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

<link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />

  <link rel="stylesheet" href="{{ url('assets/css/style.css') }}">

  <link rel="stylesheet" href="{{ url('assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css') }}">
  <!-- Tempusdominus Bootstrap 4 -->
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ url('assets/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ url('assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="{{ url('assets/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css') }}">

  <!-- full calendar* -->


  <link rel="stylesheet" href="{{ url('/assets/plugins/fullcalendar/main.min.css') }}">
  <link rel="stylesheet" href="{{ url('/assets/plugins/fullcalendar-daygrid/main.min.css') }}">
  <link rel="stylesheet" href="{{ url('/assets/plugins/fullcalendar-timegrid/main.min.css') }}">
  <link rel="stylesheet" href="{{ url('/assets/plugins/fullcalendar-bootstrap/main.min.css') }}">


<style type="text/css">/* Chart.js */
@keyframes chartjs-render-animation{from{opacity:.99}to{opacity:1}}.chartjs-render-monitor{animation:chartjs-render-animation 1ms}.chartjs-size-monitor,.chartjs-size-monitor-expand,.chartjs-size-monitor-shrink{position:absolute;direction:ltr;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1}.chartjs-size-monitor-expand>div{position:absolute;width:1000000px;height:1000000px;left:0;top:0}.chartjs-size-monitor-shrink>div{position:absolute;width:200%;height:200%;left:0;top:0}</style>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="sidebar-mini layout-fixed" style="height: auto;" >
<?php 
  use App\Assistan\Assistan;
  $notification = Assistan::getNotification();
  $cal = Assistan::getCalendarEvent();
 ?>
    <div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ url('/home')}}" class="nav-link">Home</a>
      </li>
    </ul>
    <span >
      
    </span>

      






    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

<li class="nav-item dropdown mr-3" >
        <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false" title="evenement prevu dans moin de {{ $notification->duree }} jour(s)">
          @if($notification->total >= 1)
          <span class="badge badge-pill badge-danger navbar-badge plop"> {{ $notification->total }} Notification(s)</span>
          @endif
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px; ">
          <span class="dropdown-item dropdown-header"> {{ $notification->total }} Notifications <strong><sub>(- {{ $notification->duree }} jour(s))</sub></strong></span>
          
          @if(count($notification->demandes) != 0)
          <div class="dropdown-divider"></div>
          <a href="{{ url('admin/notification/demande')}}" class="dropdown-item">
            <i class="fas fa-envelope mr-2" style="color: red;"></i> {{ count($notification->demandes) }} Demande(s)  
          </a>
          @endif


          @if(count($notification->visites) != 0)
          <div class="dropdown-divider"></div>
          <a href="{{ url('admin/notification/visite')}}" class="dropdown-item">
            <i class="fas fa-calendar-alt mr-2" style="color: red;"></i> {{ count($notification->visites) }} Visite(s)  
          </a>
          @endif


          @if(count($notification->descentes) != 0)
          <div class="dropdown-divider"></div>
          <a href="{{ url('admin/notification/descente')}}" class="dropdown-item">
            <i class="fas fa-hiking mr-2" style="color: red;"></i> {{ count($notification->descentes) }} Descente(s)  
          </a>
          @endif

          @if(count($notification->derogations) != 0)
          <div class="dropdown-divider"></div>
          <a href="{{ url('admin/notification/derogations')}}" class="dropdown-item">
            <i class="fas fa-people-arrows mr-2" style="color: red;"></i> {{ count($notification->derogations) }} Derogation(s)  
          </a>
          @endif

          @if(count($notification->bornages) != 0)
          <div class="dropdown-divider"></div>
          <a href="{{ url('admin/notification/bornages')}}" class="dropdown-item">
            <i class="fas fa-lock mr-2" style="color: red;"></i> {{ count($notification->bornages) }} Bornage(s)  
          </a>
          @endif

          @if(count($notification->protocols) != 0)
          <div class="dropdown-divider"></div>
          <a href="{{ url('admin/notification/protocols')}}" class="dropdown-item">
            <i class="fas fa-handshake mr-2" style="color: red;"></i> {{ count($notification->protocols) }} Protocol(s)  
          </a>
          @endif

          @if(count($notification->receptions) != 0)
          <div class="dropdown-divider"></div>
          <a href="{{ url('admin/notification/receptions')}}" class="dropdown-item">
            <i class="fas fa-glass-cheers mr-2" style="color: red;"></i> {{ count($notification->receptions) }} Reception(s)  
          </a>
          @endif
      </li>

      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="fas fa-th-large">
          </i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          @if(Auth::check())
                    <a class="dropdown-item" href="{{ url('/admin/calendar') }}">Agenda &nbsp;&nbsp; <i class="fas fa-pen-alt" style="color: blue;" aria-hidden="true"></i></a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ url('/admin/user/profile') }}">Profile &nbsp;&nbsp; <i class="fas fa-user" style="color: blue;" aria-hidden="true"></i></a>
                    <div class="dropdown-divider"></div>

                        <a class="dropdown-item" href="{{ url('/logout') }}"
                          onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
                            Deconnexion <i class="fas fa-power-off" style="color: red;" aria-hidden="true"></i>
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                @endif
        </div>
      </li>    


    </ul>
  </nav>

<aside class="main-sidebar elevation-4 sidebar-dark-warning">
    <!-- Brand Logo -->
    <a href="{{ url('home')}}" class="brand-link">
      <img src="{{ url('assets/dist/img/ngc.jpg')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">IMMOB 237</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar os-host os-theme-light os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition"><div class="os-resize-observer-host observed"><div class="os-resize-observer" style="left: 0px; right: auto;"></div></div><div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;"><div class="os-resize-observer"></div></div><div class="os-content-glue" style="margin: 0px -8px;"></div><div class="os-padding"><div class="os-viewport os-viewport-native-scrollbars-invisible" style="overflow-y: scroll;"><div class="os-content" style="padding: 0px 8px; height: 100%; width: 100%;">
      <!-- Sidebar user panel (optional) -->
      @if(Auth::check())
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ url(isset(Auth::user()->profile)? 'storage/'.Auth::user()->profile :'assets/dist/img/ngc.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><b>{{ Auth::user()->name }}</b></a>
        </div>
      </div>
      @endif

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon text-info class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Intervenant
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="fas fa-circle text-danger"></i>
                  <p>
                    Ajout intervenants
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{ url('/admin/demarcheur/create') }}" class="nav-link">
                      <i class="fas fa-dot-circle nav-icon text-info text-info"></i>
                      <p>Demarcheurs</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ url('/admin/geometre/create') }}" class="nav-link">
                      <i class="fas fa-dot-circle nav-icon text-info"></i>
                      <p>Geometres</p>
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="{{ url('/admin/facilitateur/create') }}" class="nav-link">
                      <i class="fas fa-dot-circle nav-icon text-info"></i>
                      <p>Facilitateurs</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ url('/admin/layonnage/create') }}" class="nav-link">
                      <i class="fas fa-dot-circle nav-icon text-info"></i>
                      <p>Layonneurs</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ url('/admin/proprietaire/create') }}" class="nav-link">
                      <i class="fas fa-dot-circle nav-icon text-info"></i>
                      <p>Proprietaires</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ url('/admin/membre-comission/create') }}" class="nav-link">
                      <i class="fas fa-dot-circle nav-icon text-info"></i>
                      <p>Administration</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="fas fa-circle text-danger"></i>
                  <p>
                    Liste intervenants
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="{{ url('/admin/demarcheur/') }}" class="nav-link">
                      <i class="fas fa-dot-circle nav-icon text-info"></i>
                      <p>Liste Demarcheurs</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ url('/admin/geometre/') }}" class="nav-link">
                      <i class="fas fa-dot-circle nav-icon text-info"></i>
                      <p>Liste Geometres</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ url('/admin/facilitateur/') }}" class="nav-link">
                      <i class="fas fa-dot-circle nav-icon text-info"></i>
                      <p>Liste Facilitateurs</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ url('/admin/layonnage/') }}" class="nav-link">
                      <i class="fas fa-dot-circle nav-icon text-info"></i>
                      <p>Liste Layonneurs</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ url('/admin/proprietaire/') }}" class="nav-link">
                      <i class="fas fa-dot-circle nav-icon text-info"></i>
                      <p>Liste Proprietaires</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ url('/admin/membre-comission') }}" class="nav-link">
                      <i class="fas fa-dot-circle nav-icon text-info"></i>
                      <p>Liste Administration</p>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon  fas fa-globe"></i>
              <p>
                Sites
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/admin/site/create') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Ajout d'un site</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('/admin/site/') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Liste des sites</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon  fas fa-atom"></i>
              <p>
                Processus
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/admin/processus/create') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Ajout d'un processus</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('/admin/processus/') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Liste des processus</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-ruler-combined"></i>
              <p>
                Bornage
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/admin/bornage/create') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Ajout Bornage</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('/admin/bornage/') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Liste Bornage</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-map-signs"></i>
              <p>
                protocol
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/admin/protocol/create') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Ajout protocol</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('/admin/protocol/') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Liste protocol</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-anchor"></i>
              <p>
                Layonnage
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/admin/layonner/create') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Ajout Layonnage</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('/admin/layonner/') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Liste Layonnage</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-cubes"></i>
              <p>
                Derogation
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/admin/derogation/create') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Ajout Derogation</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('/admin/derogation/') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Liste Derogation</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-envelope"></i>
              <p>
                Demande
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/admin/demande/create') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Ajout Demande</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('/admin/demande/') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Liste Demande</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-dollar"></i>
              <p>
                Commission
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/admin/commission/create') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Ajout Commission</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('/admin/commission/') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Liste Commission</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon  fas fa-archive"></i>
              <p>
                Documents
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/admin/archive/create') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Ajout d'un Document</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('/admin/archive/') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Liste des Documents</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Visites
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/admin/visite/create') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Ajout d'une visite</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('/admin/visite/') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Liste des visites</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-hiking"></i>
              <p>
                Descente sur le site
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/admin/descente/create') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Ajout d'une descente</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('/admin/descente/') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Liste des descentes</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{ url('/admin/reception/') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Reception</p>
                </a>
              </li>


              <li class="nav-item">
                <a href="{{ url('/admin/riverin/') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Participant</p>
                </a>
              </li>
            </ul>
          </li>

@if(Auth::check() && Auth::user()->admin == 'ADMIN')
          <li class="nav-header">UTILISATEURS</li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-circle"></i>
              <p>
                Gestion
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/admin/user/create') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Ajouter</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('/admin/user/') }}" class="nav-link">
                  <i class="fas fa-circle nav-icon text-info"></i>
                  <p>Liste des utilisateurs</p>
                </a>
              </li>
            </ul>
          </li>
@endif
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div></div></div><div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable os-scrollbar-auto-hidden"><div class="os-scrollbar-track"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar os-scrollbar-vertical os-scrollbar-auto-hidden"><div class="os-scrollbar-track"><div class="os-scrollbar-handle" style="height: 14.7854%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar-corner"></div></div>
    <!-- /.sidebar -->
  </aside>
    </div>






<div class="content-wrapper" style="min-height: 129px;" data-select2-id="52">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><b>IMMOB</b> 237</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <div id="app">
            @if(isset($ariane))
                <a href=" {{ url('/home')}} ">Home</a>
                @for ($i = 0; $i < count($ariane)-1; $i++)
                <img src=" {{ url('assets/images/go.png') }} ">  <a href=" {{ url('admin/' .  $ariane[$i]) }} "> {{ $ariane[$i] }} </a>
                @endfor
                @if(count($ariane) > 0)
                  <img src=" {{ url('assets/images/go.png') }} "><b>{{ $ariane[ count($ariane)-1] }}</b>
               @endif
            @endif

    </div>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content" data-select2-id="51">
    <div id="app" class="container-fluid" data-select2-id="50">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    </section>
</div>
    <!-- Scripts -->
    <div class="wrapper">
<footer class="main-footer">
    <div class="float-right d-none d-sm-block">

      <p><strong>Version Pro: </strong>1.0.0.1</p>
    </div>
    <strong>Copyright © NGC2020 <a href="#">New Generation Coorporation</a></strong>
  </footer>

</div>





<script src="{{ asset('js/app.js') }}"></script>
<!-- jQuery -->
<script src="{{ url('/assets/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ url('/assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ url('/assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ url('/assets/plugins/chart.js/Chart.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ url('/assets/plugins/sparklines/sparkline.js') }}"></script>
<!-- JQVMap -->
<script src="{{ url('/assets/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ url('/assets/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ url('/assets/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ url('/assets/plugins/moment/moment.min.js') }}"></script>
<script src="{{ url('/assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ url('/assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ url('/assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ url('/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ url('/assets/dist/js/adminlte.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ url('/assets/dist/js/pages/dashboard.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ url('/assets/dist/js/demo.js') }}"></script>

<script src="{{ url('/assets/plugins/toastr/toastr.min.js') }}"></script>

<script src="{{ url('/assets/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<script src="{{ url('/assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
<script src="{{ url('/assets/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ url('/assets/plugins/inputmask/jquery.inputmask.min.js') }}"></script>
<script src="{{ url('/assets/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js') }}"></script>
<script src="{{ url('/assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<!-- pour l'alerte -->
<script src="{{ url('assets/js/script.js') }}"></script>
<!-- toast -->
<script src="{{ url('assets/js/toast.js') }}"></script>

<script>
    @if(session('flash_message'))
      toastr.success("{{session('flash_message')}}");
    @endif

    @if(session('error_message'))
      toastr.error("{{session('error_message')}}");
    @endif
</script>


<script src="{{url('/assets/plugins/fullcalendar/main.min.js')}}"></script>
<script src="{{url('/assets/plugins/fullcalendar-daygrid/main.min.js')}}"></script>
<script src="{{url('/assets/plugins/fullcalendar-timegrid/main.min.js')}}"></script>
<script src="{{url('/assets/plugins/fullcalendar-interaction/main.min.js')}}"></script>
<script src="{{url('/assets/plugins/fullcalendar-bootstrap/main.min.js')}}"></script>


<script>
  

  var calendar;

$(function () {
    /* initialize the external events
     -----------------------------------------------------------------*/
    function ini_events(ele) {
      ele.each(function () {

        // create an Event Object (https://fullcalendar.io/docs/event-object)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        }

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject)

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex        : 1070,
          revert        : true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        })

      })
    }

    ini_events($('#external-events div.external-event'))

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendarInteraction.Draggable;

    var containerEl = document.getElementById('external-events');
    var checkbox = document.getElementById('drop-remove');
    var calendarEl = document.getElementById('calendar');

    // initialize the external events
    // -----------------------------------------------------------------

    new Draggable(containerEl, {
      itemSelector: '.external-event',
      eventData: function(eventEl) {
        console.log(eventEl);
        return {
          title: eventEl.innerText,
          backgroundColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
          borderColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
          textColor: window.getComputedStyle( eventEl ,null).getPropertyValue('color'),
        };
      }
    });
    var ev = [];

    <?php 

      foreach (session('cal') as $item) {
        # code...
     ?>

      var title = "{{ $item['title'] }}",
          id = "{{ $item['id'] }}",
          start = "{{ $item['start'] }}",
          backgroundColor = "{{ $item['backgroundColor'] }}",
          borderColor = "{{ $item['borderColor'] }}",
          end = "{{ $item['end'] }}",
          allDay = "{{ $item['allDay'] }}";

      ev.push({
          id             : id,
          title          : title,
          start          : start,
          end            : end,
          backgroundColor: backgroundColor, //red
          borderColor    : borderColor, //red
          allDay         : allDay
      });

<?php 
  
      }
 ?>

   calendar = new Calendar(calendarEl, {
      plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      'themeSystem': 'bootstrap',
      //Random default events
      events    : ev,
      editable  : true,
      droppable : true, // this allows things to be dropped onto the calendar !!!
      drop      : function(info) {
        // is the "remove after drop" checkbox checked?
        if (checkbox.checked) {
          // if so, remove the element from the "Draggable Events" list
          info.draggedEl.parentNode.removeChild(info.draggedEl);
        }
      }
    });
    calendar.render();
    // $('#calendar').fullCalendar()

    /* ADDING EVENTS */
    var currColor = '#3c8dbc' //Red by default
    //Color chooser button
    $('#color-chooser > li > a').click(function (e) {
      e.preventDefault()
      //Save color
      currColor = $(this).css('color')
      //Add color effect to button
      $('#add-new-event').css({
        'background-color': currColor,
        'border-color'    : currColor
      })
    })
    $('#add-new-event').click(function (e) {
      e.preventDefault()
      //Get value and make sure it is not null
      var val = $('#new-event').val()
      if (val.length == 0) {
        return
      }

      //Create events
      var event = $('<div />')
      event.css({
        'background-color': currColor,
        'border-color'    : currColor,
        'color'           : '#fff'
      }).addClass('external-event')
      event.html(val)
      $('#external-events').prepend(event)

      //Add draggable funtionality
      ini_events(event)

      //Remove event from text input
      $('#new-event').val('')
    })
  })



function addEvent() {
  // body...
  var events = calendar.getEvents();
  var ev = [];
  for (var i = 0; i < events.length; i++) {
    var date1 = events[i].start;
    var title = events[i].title,
          id = events[i].id,
          start = date1.getFullYear()+'-' + (date1.getMonth()+1) + '-'+date1.getDate(),
          backgroundColor = events[i].backgroundColor,
          borderColor = events[i].borderColor,
          end = events[i].end,
          allDay = events[i].allDay;


        if(end !== null){
          end = end.getFullYear()+'-' + (end.getMonth()+1) + '-'+end.getDate();
        }


      ev.push({
          id             : id,
          title          : title,
          start          : start,
          end            : end,
          backgroundColor: backgroundColor, //red
          borderColor    : borderColor, //red
          allDay         : allDay
      });
  }


    var data = {
      "_token": $('input[name=_token]').val(),
      "value": ev,
    }

    $.ajax({
      type: "POST",
      url: '/admin/createEvent',
      data: data,
      success: function(response){
        Swal.fire({
          icon: 'success',
          // title: 'Mise A Jour De l\'Agenda Effectuée Avec Succes',
          title : response['status'],
          showConfirmButton: true,
          timer: 4000
        }).then((result) => {
            location.reload();
        });
      }
    });
    console.log(data);
}
</script>

</body>
</html>
