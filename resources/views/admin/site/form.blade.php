<div class="row col-sm-12">
<div class="col-sm-6">
<div class="form-group required {{ $errors->has('region') ? 'has-error' : ''}}">
    {!! Form::label('region', 'Region', ['class' => 'control-label']) !!}
    {!! Form::text('region', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('region', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('departement') ? 'has-error' : ''}}">
    {!! Form::label('departement', 'Departement', ['class' => 'control-label']) !!}
    {!! Form::text('departement', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('departement', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('arrondissement') ? 'has-error' : ''}}">
    {!! Form::label('arrondissement', 'Arrondissement', ['class' => 'control-label']) !!}
    {!! Form::text('arrondissement', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('arrondissement', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('village') ? 'has-error' : ''}}">
    {!! Form::label('village', 'Village', ['class' => 'control-label']) !!}
    {!! Form::text('village', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('village', '<p class="help-block">:message</p>') !!}
</div>
</div>

<div class="col-sm-6">

<div class="form-group required {{ $errors->has('quartier') ? 'has-error' : ''}}">
    {!! Form::label('quartier', 'Quartier', ['class' => 'control-label']) !!}
    {!! Form::text('quartier', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('quartier', '<p class="help-block">:message</p>') !!}
</div>
    <div class="form-group {{ $errors->has('longitude') ? 'has-error' : ''}}">
    <label class="control-label">
        Longitude
    </label>
    <input type="number" min="0" name="longitude" class="form-control">
    {!! $errors->first('longitude', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('latitude') ? 'has-error' : ''}}">
    <label class="control-label">
        Latitude
    </label>
    <input type="number" min="0" name="latitude" class="form-control">
    {!! $errors->first('latitude', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    <label class="control-label">
        Selectionner Les Images
    </label>
    <input type="file" name="file[]" multiple class="form-control">
</div>

</div>
</div>
<div class="form-group required {{ $errors->has('surface') ? 'has-error' : ''}}">
    <label for="surface" class="control-label">Surface</label>
    <div class="input-group mb-3">
    <input type="number" min="0" name="surface" class="form-control" id="surface" value="{{ isset($site->surface) ? $site->surface : '' }}" required>
    <div class="input-group-append">
        <span class="input-group-text">m<sup>2</sup></span>
        </div>
    </div>
    {!! $errors->first('surface', '<p class="help-block">:message</p>') !!}
        
</div>
<div class="form-group required {{ $errors->has('situation_Geopraphique') ? 'has-error' : ''}}">
    {!! Form::label('situation_Geopraphique', 'Situation Geopraphique', ['class' => 'control-label']) !!}
    {!! Form::select('situation_Geopraphique', json_decode('{"habitable":"habitable","inhabitable":"inhabitable"}', true), null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('situation_Geopraphique', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('situation_De_Ville') ? 'has-error' : ''}}">
    {!! Form::label('situation_De_Ville', 'Situation De Ville', ['class' => 'control-label']) !!}
    {!! Form::select('situation_De_Ville', json_decode('{"campagne":"campagne","ville":"ville"}', true), null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('situation_De_Ville', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group ">
    <label for="proprietaire" class="control-label">Choix Des Proprietaires</label>
    <select multiple="multiple"  name="proprietaire[]" id="proprietaire" class="form-control js-example-basic-multiple" >
        @foreach($proprietaire as $item)
            <option value="{{$item->id}}">{{$item->noms}}</option>
        @endforeach
    </select>
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Modifier' : 'Ajouter', ['class' => 'btn btn-primary']) !!}
</div>
