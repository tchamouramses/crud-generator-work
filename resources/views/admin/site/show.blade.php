@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Site {{ $site->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/site') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/site/' . $site->id . '/edit') }}" title="Edit Site"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>

                        <a href="{{ url('/admin/site/' . $site->id . '/galerie') }}" title="images du site"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Images</button></a>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $site->id }}</td>
                                    </tr>
                                    <tr><th> Region </th><td> {{ $site->region }} </td></tr><tr><th> Departement </th><td> {{ $site->departement }} </td></tr><tr><th> Arrondissement </th><td> {{ $site->arrondissement }} </td></tr>
                                    <tr><th> Quartier </th><td> {{ $site->quartier }} </td></tr>
                                    <tr><th> Surface </th><td> {{ $site->surface }}  m<sup>2</sup> </td></tr>
                                    <tr><th> Situation Geographique </th><td> {{ $site->situation_Geopraphique   }} </td></tr>
                                    <tr><th> Situation de Vie </th><td> {{ $site->situation_De_Ville }} </td></tr>
                                    <tr><th colspan="2"> <h5 align="center">Proprietaire(s) </h5></th></tr>
                                    @foreach($proprietaire as $item)
                                        <tr>
                                            <th>
                                                {{ $item->prop->noms }} ({{ $item->prop->telephone }})
                                            </th>
                                            <td>
                                                <button type="submit" class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $item->id }},'/admin/site/' + {{ $item->id }} +'/delete')"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button>
                                            </td>
                                        </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
