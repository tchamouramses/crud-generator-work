@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Image du Site {{ $site->quartier }}</div>
                    <di v class="card-body">
                      <a href="{{ url('/admin/site/' . $site->id) }}" title="Back" ><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>



                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#staticBackdrop"><i class="fa fa-plus" aria-hidden="true"></i>
                          Nouvelle image
                        </button>

                        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Ajouter Une  Nouvelle Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

<form method="POST" action="{{ url('/admin/galerie') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

              <div class="modal-body">

                                
                                    {{ csrf_field() }}
 <div class="form-group required {{ $errors->has('image') ? 'has-error' : ''}}">
    <label for="image" class="control-label">image</label>
    <input class="form-control" name="image" type="file" id="image" required>
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
 <div class="form-group required {{ $errors->has('id_visite') ? 'has-error' : ''}}">
    <label for="id_visite" class="control-label">Site</label>
    <input class="form-control" name="id_visite" type="hidden" id="id_visite" value="{{ $site->id }}">

    <input class="form-control" type="text" value="{{ $site->quartier }}" readonly="readonly">
</div>
</div>
                              <div class="modal-footer">
                                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                              </div>
</form>
    </div>
  </div>
</div>
</div>






                        <div class="row col-md-12" >
                          @foreach($galerie as $item)
                            <div style="margin-top: 10px;" class="col-md-4 border border-success rounded" align="center">
                              <a href="{{ url('storage/'.$item->image)}}">
                              <img src="{{ url('storage/'.$item->image)}}" style="width: 100%; margin-top: 10px;">
                              </a>
                              <div class="row">
                                <div class="col-md-12"  style="margin-bottom: 3px; margin-top: 3px;">

                                            <button type="submit" class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $item->id }},'/admin/galerie/' + {{ $item->id }})"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button>
                                </div>

                              </div>

                            </div>
                          @endforeach
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
