@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Site</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/site/create') }}" class="btn btn-success btn-sm" title="Add New Site">
                            <i class="fa fa-plus" aria-hidden="true"></i> Ajouter Nouveau
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/site', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Departement</th><th>Arrondissement</th><th>Quartier</th><th>Surface</th><th>Situation Geo</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($site as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->departement }}</td><td>{{ $item->arrondissement }}</td><td>{{ $item->quartier }}</td>
                                        <td>{{ $item->surface }} m<sup>2</sup> </td>
                                        <td>{{ $item->situation_Geopraphique }}</td>
                                        <td>
                                            <a href="{{ url('/admin/site/' . $item->id) }}" title="View Site"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/site/' . $item->id . '/edit') }}" title="Edit Site"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                            
                                            <button type="submit" class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $item->id }},'/admin/site/' + {{ $item->id }})"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $site->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
