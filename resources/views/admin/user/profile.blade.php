@extends('layouts.app')

@section('content')
    <?php 
        $ariane = ['Profile'];
     ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-body box-profile">
                        <a href="{{ url(isset($user->profile)? 'storage/'.$user->profile :'assets/dist/img/avatar.png')}}" style="text-decoration: none;">
                            <div class="row">
                                <img src="{{ url(isset($user->profile)? 'storage/'.$user->profile :'assets/dist/img/ngc.jpg')}}" class="profile-user-img img-fluid img-circle elevation-3">
                            </div>
                            <h3 class="profile-username text-center">{{ $user->name }}</h3>
                        </a>

            <div class="card">
              <div class="card-header p-2 col-md-12">
                <ul class="nav nav-pills">
                  <li class="nav-item col-md-4"><a class="nav-link active" href="#timeline" data-toggle="tab"><b>Information personnelle</b></a></li>
                  <li class="nav-item col-md-4"><a class="nav-link" href="#activity" data-toggle="tab"><b>Modifier Votre profile</b></a></li>
                  <li class="nav-item col-md-4"><a class="nav-link" href="#settings" data-toggle="tab"><b>Modifier le mot de passe</b></a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane" id="activity">
<div class="container">
                        

<form method="post" action="{{ url('/admin/user/update') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

              <div class="modal-body">

{{ csrf_field() }}
<div class="form-group required {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Nom' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{$user->name}}" required>
</div>
<div class="form-group required {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="email" id="email" value="{{$user->email}}" required>
    {!! $errors->first('email', '<p class="help-block" style="color: red;">:message</p>') !!}
</div>

<div class="form-group">
    <label for="profile" class="control-label">{{ 'Photo de Profile' }}</label>
    <input class="form-control" name="profile" type="file" id="pass"  value="{{$user->profile}}">
    {!! $errors->first('profile', '<p class="help-block" style="color: red;">:message</p>') !!}
</div>

<div class="form-group required {{ $errors->has('ville') ? 'has-error' : ''}}">
    <label for="ville" class="control-label">{{ 'Ville' }}</label>
    <input class="form-control" name="ville" type="text" id="ville"  value="{{$user->ville}}">
    {!! $errors->first('ville', '<p class="help-block" style="color: red;">:message</p>') !!}
</div>
<div class="form-group required">
    <label for="quartier" class="control-label">{{ 'Quartier' }}</label>
    <input class="form-control" name="quartier" type="text" id="quartier"  value="{{$user->quartier}}">
    {!! $errors->first('quartier', '<p class="help-block" style="color: red;">:message</p>') !!}
</div>

</div>
    <div class="form-group"> 
    <button type="submit" class="btn btn-primary col-md-12">Modifier</button>
    </div>
</form>

                    </div>

                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane active" id="timeline">
                    
                    <div class="row col-md-12">
                            <div class="col-md-6">
                                <label>Email :</label>
                            </div>
                            <div class="col-md-6">
                                <span>{{$user->email}}</span>
                            </div>
                            <div class="col-md-6">
                                <label>Telephone :</label>
                            </div>
                            <div class="col-md-6">
                                <span>{{isset($user->telephone) ? $user->telephone : 'Pas de numero de telephone'}}</span>
                            </div>
                            <div class="col-md-6">
                                <label>Ville :</label>
                            </div>
                            <div class="col-md-6">
                                <span>{{$user->ville}}</span>
                            </div>
                            <div class="col-md-6">
                                <label>Quartier :</label>
                            </div>
                            <div class="col-md-6">
                                <span>{{$user->quartier}}</span>
                            </div>
                    </div>










                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="settings">
<div class="container">
                        

            <form method="post" action="{{ url('/admin/user/password') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

              <div class="modal-body">

                {{ csrf_field() }}
                <div class="form-group required">
                    <label for="oldpassword" class="control-label">{{ 'Mot De Passe Actuel' }}</label>
                    <input class="form-control" name="oldpassword" type="password" id="oldpassword"  required>
                </div>
                <div class="form-group required">
                    <label for="password" class="control-label">{{ 'Nouveau Mot De Passe' }}</label>
                    <input class="form-control" name="password" type="password" id="password"  required>
                </div>
                <div class="form-group required">
                    <label for="password_confirmation" class="control-label">{{ 'Confirmer Le Nouveau Mot De Passe' }}</label>
                    <input class="form-control" name="password_confirmation" type="password" id="password_confirmation"  required>
                </div>
                    <div class="form-group"> 
                        <button type="submit" class="btn btn-primary col-md-12">Modifier</button>
                    </div>
                </div>
            </form>

                    </div>


                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
