<div class="form-group required {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Nom' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ isset($user->name) ? $user->name : ''}}" required>
    {!! $errors->first('name', '<p class="help-block" style="color: red;">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="email" id="email" value="{{ isset($user->email) ? $user->email : ''}}" required>
    {!! $errors->first('email', '<p class="help-block" style="color: red;">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('profile') ? 'has-error' : ''}}">
    <label for="profile" class="control-label">{{ 'Photo' }}</label>
    <input class="form-control" name="profile" type="file" id="profile" value="{{ isset($user->profile) ? $user->profile : ''}}">
    {!! $errors->first('profile', '<p class="help-block" style="color: red;">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('telephone') ? 'has-error' : ''}}">
    <label for="telephone" class="control-label">{{ 'Telephone' }}</label>
    <input class="form-control" name="telephone" type="text" id="telephone" value="{{ isset($user->telephone) ? $user->telephone : ''}}">
    {!! $errors->first('telephone', '<p class="help-block" style="color: red;">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('ville') ? 'has-error' : ''}}">
    <label for="ville" class="control-label">{{ 'Ville de residence' }}</label>
    <input class="form-control" name="ville" type="text" id="ville" value="{{ isset($user->ville) ? $user->ville : ''}}">
    {!! $errors->first('ville', '<p class="help-block" style="color: red;">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('quartier') ? 'has-error' : ''}}">
    <label for="quartier" class="control-label">{{ 'Quartier' }}</label>
    <input class="form-control" name="quartier" type="text" id="quartier" value="{{ isset($user->quartier) ? $user->quartier : ''}}">
    {!! $errors->first('quartier', '<p class="help-block" style="color: red;">:message</p>') !!}
</div>

<div class="form-group required {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="control-label">{{ 'Mot de Passe' }}</label>
    <input class="form-control" name="password" type="password" id="password" required>
    {!! $errors->first('password', '<p class="help-block" style="color: red;">:message</p>') !!}
</div>
<div class="form-group required ">
    <label for="password_confirmation" class="control-label">{{ 'Confirmer le mot de passe' }}</label>
    <input class="form-control" name="password_confirmation" type="password" id="password_confirmation" required>
    {!! $errors->first('password', '<p class="help-block" style="color: red;">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
