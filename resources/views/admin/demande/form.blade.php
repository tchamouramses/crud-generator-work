<div class="form-group required {{ $errors->has('date_demande') ? 'has-error' : ''}}">
    <label for="date_demande" class="control-label">{{ 'Date Demande' }}</label>
    <input class="form-control dateramses" name="date_demande" type="date" id="date_demande" value="{{ isset($demande->date_demande) ? $demande->date_demande : ''}}" required>
    {!! $errors->first('date_demande', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('frais') ? 'has-error' : ''}}">
    <label for="frais" class="control-label">{{ 'Frais' }}</label>
    
    <div class="input-group mb-3">
        <input class="form-control" name="frais" type="number" min="0" id="frais" value="{{ isset($demande->frais) ? $demande->frais : ''}}" required>
    {!! $errors->first('frais', '<p class="help-block">:message</p>') !!}


    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
</div>
<div class="form-group required {{ $errors->has('nature') ? 'has-error' : ''}}">
    <label for="nature" class="control-label">{{ 'Nature' }}</label>
    <select name="nature" class="form-control" id="nature" required>
    @foreach (json_decode('{"Espece":"Espece","Surface":"Surface"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($demande->nature) && $demande->nature == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('nature', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group required {{ $errors->has('processus') ? 'has-error' : ''}}">
    <label for="Processus" class="control-label">{{ 'Processus' }}</label>
    @if(!empty($processus))
    <select class="form-control" name="processus" required="required">
        @foreach($processus as $item)
            <option

                @if(isset($demande->processus) && $item->id == $demande->processus)
                    selected
                @endif
            value=" {{ $item->id }} "> {{ $item->nom_processus }} </option>
        @endforeach
    </select>
    @else
         <select class="form-control" name="Processus" disabled="disabled" required="required">
            <option value=""> Aucun Processus </option>
        </select>
    @endif

    {!! $errors->first('Processus', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input  @if(empty($processus)) 
                disabled 
            @endif
             class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
