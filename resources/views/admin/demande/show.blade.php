@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Demande {{ $demande->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/demande') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/demande/' . $demande->id . '/edit') }}" title="Modifier Demande"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $demande->id }}</td>
                                    </tr>
                                    <tr><th> Date Demande </th><td> {{ $demande->date_demande }} </td></tr><tr><th> Frais </th><td> {{ $demande->frais }} FCFA</td></tr><tr><th> Nature </th><td> {{ $demande->nature }} </td></tr><tr><th> Processus </th><td> {{ $processus->nom_processus }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
