@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Reception {{ $reception->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/reception') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/reception/' . $reception->id . '/edit') }}" title="Modifier Reception"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $reception->id }}</td>
                                    </tr>
                                    <tr><th> Date De Reception </th><td> {{ $reception->date_de_reception }} </td></tr><tr><th> Frais De Reception </th><td> {{ $reception->frais_de_reception }} FCFA</td></tr><tr><th> Descente </th><td> {{ $descente->nom_processus }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
