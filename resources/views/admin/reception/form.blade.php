<div class="form-group required {{ $errors->has('date_de_reception') ? 'has-error' : ''}}">
    <label for="date_de_reception" class="control-label">{{ 'Date De Reception' }}</label>
    <input class="form-control dateramses" name="date_de_reception" type="date" id="date_de_reception" value="{{ isset($reception->date_de_reception) ? $reception->date_de_reception : ''}}" required>
    {!! $errors->first('date_de_reception', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('frais_de_reception') ? 'has-error' : ''}}">
    <label for="frais_de_reception" class="control-label">{{ 'Frais De Reception' }}</label>
    

    <div class="input-group mb-3">
     <input class="form-control" name="frais_de_reception" type="number" min="0" id="frais_de_reception" value="{{ isset($reception->frais_de_reception) ? $reception->frais_de_reception : ''}}" required>
    {!! $errors->first('frais_de_reception', '<p class="help-block">:message</p>') !!}

    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
</div>
<div class="form-group required {{ $errors->has('descente') ? 'has-error' : ''}}">
    <label for="descente" class="control-label">{{ 'Descente' }}</label>
    
    <select class="form-control" name="descente" required>
        @foreach($descente as $item)
            <option value=" {{ $item->id }} ">
                Descente ' {{ $item->nom_processus }} '
            </option>
        @endforeach
    </select>
    {!! $errors->first('descente', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
