<div class="form-group required {{ $errors->has('fichier') ? 'has-error' : ''}}">
    <label for="fichier" class="control-label">{{ 'Fichier' }}</label>
    <input class="form-control" name="fichier" type="file" id="fichier" value="{{ isset($image->fichier) ? $image->fichier : ''}}" required>
    {!! $errors->first('fichier', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('visite') ? 'has-error' : ''}}">
    <label for="visite" class="control-label">{{ 'Visite' }}</label>
    <input class="form-control" name="visite" type="number" id="visite" value="{{ isset($image->visite) ? $image->visite : ''}}" required>
    {!! $errors->first('visite', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
