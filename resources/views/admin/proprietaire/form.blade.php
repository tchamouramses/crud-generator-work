<div class="form-group required {{ $errors->has('noms') ? 'has-error' : ''}}">
    <label for="noms" class="control-label">{{ 'Noms' }}</label>
    <input class="form-control" name="noms" type="text" id="noms" value="{{ isset($proprietaire->noms) ? $proprietaire->noms : ''}}" required>
    {!! $errors->first('noms', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('telephone') ? 'has-error' : ''}}">
    <label for="telephone" class="control-label">{{ 'Telephone' }}</label>
    
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-phone"></i></span>
        </div>
       <input class="form-control" name="telephone" type="text" id="telephone" value="{{ isset($proprietaire->telephone) ? $proprietaire->telephone : ''}}" required>
    {!! $errors->first('telephone', '<p class="help-block">:message</p>') !!}

    </div>
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
