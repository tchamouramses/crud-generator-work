@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Proprietaire {{ $proprietaire->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/proprietaire') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/proprietaire/' . $proprietaire->id . '/edit') }}" title="Edit Proprietaire"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>
                        
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $proprietaire->id }}</td>
                                    </tr>
                                    <tr><th> Noms </th><td> {{ $proprietaire->noms }} </td></tr><tr><th> Telephone </th><td> {{ $proprietaire->telephone }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
