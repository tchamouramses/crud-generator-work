@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Layonnage {{ $layonner->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/layonner') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/layonner/' . $layonner->id . '/edit') }}" title="Modifier Layonner"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $layonner->id }}</td>
                                    </tr>
                                    <tr><th> Date Debut </th><td> {{ $layonner->date_debut }} </td></tr><tr><th> Processus </th><td> {{ $processus->nom_processus }} </td></tr><tr><th> Frais </th><td> {{ $layonner->frais }} FCFA</td></tr><tr><th> Durée </th><td> {{ $layonner->duree }} jr(s)</td></tr><tr><th> Surface </th><td> {{ $layonner->Surface }} m<sup>2</sup></td></tr>
                                    <tr><td colspan="2"> <h4 align="center">Layonneur(s)</h4></td></tr>
                                    
                                        @foreach($layonneur as $item)
                                        <tr>
                                            <th> {{ $item->noms_Layonneur }} ({{ $item->telephone__Layonneur1 }}) </th>
                                            <td>
                                                <button type="submit" class="btn btn-danger btn-sm  deleted_element" title="Delete Layonneur" onclick="return alertDeleteElement({{ $item->id }},'/admin/layonner/' + {{ $item->id }} + '/delete')"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button>
                                            </td>
                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
