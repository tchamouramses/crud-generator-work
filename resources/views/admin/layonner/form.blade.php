<div class="form-group required {{ $errors->has('date_debut') ? 'has-error' : ''}}">
    <label for="date_debut" class="control-label">{{ 'Date Debut' }}</label>
    <input class="form-control dateramses" name="date_debut" type="date" id="date_debut" value="{{ isset($layonner->date_debut) ? $layonner->date_debut : ''}}" required>
    {!! $errors->first('date_debut', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('processus') ? 'has-error' : ''}}">
    <label for="processus" class="control-label">{{ 'Processus' }}</label>
    @if(!empty($processus))
    <select class="form-control" name="processus" required >
        @foreach($processus as $item)
            <option 
                @if(isset($layonner->processus) && $item->id == $layonner->processus)
                    selected
                @endif

            value=" {{ $item->id }} "> {{ $item->nom_processus }} </option>
        @endforeach
    </select>
    @else
         <select class="form-control" name="processus" disabled="disabled">
            <option value=""> Aucun Processus </option>
        </select>
    @endif
    {!! $errors->first('processus', '<p class="help-block">:message</p>') !!}
</div>



<div class="form-group required {{ $errors->has('frais') ? 'has-error' : ''}}">
    <label for="frais" class="control-label">{{ 'Frais' }}</label>
    

    <div class="input-group mb-3">
        <input class="form-control" name="frais" type="number" min="0" id="frais" value="{{ isset($layonner->frais) ? $layonner->frais : ''}}" required>
    {!! $errors->first('frais', '<p class="help-block">:message</p>') !!}

    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
</div>
<div class="form-group required {{ $errors->has('duree') ? 'has-error' : ''}}">
    <label for="duree" class="control-label">{{ 'Duree' }}</label>
    

    <div class="input-group mb-3">
        <input class="form-control" name="duree" type="number" min="0" id="duree" value="{{ isset($layonner->duree) ? $layonner->duree : ''}}" required>
    {!! $errors->first('duree', '<p class="help-block">:message</p>') !!}

    <div class="input-group-append">
        <span class="input-group-text">Jour(s)</span>
        </div>
    </div>
</div>
<div class="form-group required {{ $errors->has('Surface') ? 'has-error' : ''}}">
    <label for="Surface" class="control-label">{{ 'Surface' }}</label>
    
    <div class="input-group mb-3">
        <input class="form-control" name="Surface" type="number" min="0" id="Surface" value="{{ isset($layonner->Surface) ? $layonner->Surface : ''}}" required>
    {!! $errors->first('Surface', '<p class="help-block">:message</p>') !!}


    <div class="input-group-append">
        <span class="input-group-text">m<sup>2</sup></span>
        </div>
    </div>
</div>


<div class="form-group">
    <label for="facilitateur" class="control-label">Choix Des Layonneurs</label>
    <select multiple="multiple"  name="layonnages[]" id="facilitateur" class="form-control js-example-basic-multiple">
        @foreach($processus->layon as $item)

            <option value="{{$item->id}}">{{$item->noms_Layonneur}}</option>

        @endforeach
    </select>
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
