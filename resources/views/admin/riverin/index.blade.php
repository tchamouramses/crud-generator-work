@extends('layouts.app')

@section('content')
    <div class="container"> 
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Participant</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/riverin/create') }}" class="btn btn-success btn-sm" title="Ajouter Nouveau Riverin">
                            <i class="fa fa-plus" aria-hidden="true"></i> Ajouter Nouveau
                        </a>

                        <form method="GET" action="{{ url('/admin/riverin') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Rechercher..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Noms</th><th>Telephone</th><th>Processus</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($riverin as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->noms }}</td><td>{{  $item->telephone }}</td><td>{{ $item->desc->nom_processus }}</td>
                                        <td>
                                            <a href="{{ url('/admin/riverin/' . $item->id) }}" title="Details Riverin"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/riverin/' . $item->id . '/edit') }}" title="Modifier Riverin"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>

                                            <button type="submit" class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $item->id }},'/admin/riverin/' + {{ $item->id }})"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $riverin->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
