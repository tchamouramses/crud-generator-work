@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Participant {{ $riverin->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/riverin') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/riverin/' . $riverin->id . '/edit') }}" title="Modifier Riverin"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $riverin->id }}</td>
                                    </tr>
                                    <tr><th> Noms </th><td> {{ $riverin->noms }} </td></tr><tr><th> Telephone </th><td> {{ $riverin->telephone }} </td></tr>
                                    <tr><th> Status </th><td> {{ $riverin->status }} </td></tr>
                                    <tr><th> Descente Du Processus </th><td> {{ $descente->noms }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
