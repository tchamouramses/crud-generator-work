<div class="form-group required {{ $errors->has('noms') ? 'has-error' : ''}}">
    <label for="noms" class="control-label">{{ 'Noms' }}</label>
    <input class="form-control" name="noms" type="text" id="noms" value="{{ isset($riverin->noms) ? $riverin->noms : ''}}" required>
    {!! $errors->first('noms', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('telephone') ? 'has-error' : ''}}">
    <label for="telephone" class="control-label">{{ 'Telephone' }}</label>
    

    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-phone"></i></span>
        </div>
        <input class="form-control" name="telephone" type="text" id="telephone" value="{{ isset($riverin->telephone) ? $riverin->telephone : ''}}" >
    {!! $errors->first('telephone', '<p class="help-block">:message</p>') !!}

    </div>
</div>
<div class="form-group required {{ $errors->has('descente') ? 'has-error' : ''}}">
    <label for="descente" class="control-label">{{ 'Descente' }}</label>
    <select class="form-control" name="descente" required>
        @foreach($descente as $item)
            <option value=" {{ $item->id }} ">
                Descente ' {{ $item->nom_processus }} '
            </option>
        @endforeach
    </select>
    {!! $errors->first('descente', '<p class="help-block">:message</p>') !!}

</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">{{ 'Status' }}</label>
    <select class="form-control" name="status">
        <option value="Notable" selected="selected">Notable</option>
        <option value="Riverin">Riverin</option>
    </select>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
