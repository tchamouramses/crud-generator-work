<div class="form-group required {{ $errors->has('date_derogation') ? 'has-error' : ''}}">
    <label for="date_derogation" class="control-label">{{ 'Date Derogation' }}</label>
    <input class="form-control dateramses" name="date_derogation" type="date" id="date_derogation" value="{{ isset($derogation->date_derogation) ? $derogation->date_derogation : ''}}" required>
    {!! $errors->first('date_derogation', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('frais') ? 'has-error' : ''}}">
    <label for="frais" class="control-label">{{ 'Frais' }}</label>
    
    <div class="input-group mb-3">
        <input class="form-control" name="frais" type="number" min="0" id="frais" value="{{ isset($derogation->frais) ? $derogation->frais : ''}}" required>
    {!! $errors->first('frais', '<p class="help-block">:message</p>') !!}

    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
</div>
<div class="form-group required {{ $errors->has('processus') ? 'has-error' : ''}}">
    <label for="Processus" class="control-label">{{ 'Processus' }}</label>
    @if(!empty($proces))
    <select class="form-control" name="processus" required="required">
        @foreach($proces as $item)
            <option
                @if(isset($derogation->processus) && $item->id == $derogation->processus)
                    selected
                @endif

             value="{{ $item->id }}"> {{ $item->nom_processus }} </option>
        @endforeach
    </select>
    @else
         <select class="form-control" name="processus" disabled="disabled" required="required">
            <option value=""> Aucun Processus </option>
        </select>
    @endif
</div>
<div class="form-group {{ $errors->has('facilitateur') ? 'has-error' : ''}}">
    <label for="Processus" class="control-label">{{ 'Facilitateurs' }}</label>
    @if(!empty($facilitateur))
    <select multiple="multiple" class="form-control js-example-basic-multiple" name="facilitateur[]" id="facilitateur">
        @foreach($facilitateur as $item)
            <option value="{{ $item->id }}"> {{ $item->noms_Facilitateur }} </option>
        @endforeach
    </select>
    @else
         <select class="form-control" name="facilitateur" disabled="disabled">
            <option value=""> Aucun Facilitateur </option>
        </select>
    @endif

    {!! $errors->first('Processus', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input  @if(empty($proces)) 
                disabled 
            @endif
            class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
