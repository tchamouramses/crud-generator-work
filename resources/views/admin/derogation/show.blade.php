@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Derogation {{ $derogation->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/derogation') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/derogation/' . $derogation->id . '/edit') }}" title="Modifier Derogation"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $derogation->id }}</td>
                                    </tr>
                                    <tr><th> Date Derogation </th><td> {{ $derogation->date_derogation }} </td></tr><tr><th> Frais </th><td> {{ $derogation->frais }} FCFA</td></tr><tr><th> Processus </th><td> {{ $derogation->Process->nom_processus }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
