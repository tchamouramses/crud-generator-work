@extends('layouts.app')

@section('content')

  
    <!-- Main content -->
    <section class="content">
<?php 
  $ariane = ['calendar','Gestion'];
  use App\Assistan\Assistan;
  $cal = Assistan::getCalendarEvent();
?>
      <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Evenement(s)</div>
                    <div class="card-body">
                        <button type="button" title="Ajouter d'un Nouveau Document" class="btn btn-success btn-sm" data-toggle="modal" data-target="#staticBackdrop"><i class="fa fa-plus" aria-hidden="true"></i>
                            Nouvel Evenement
                        </button>

                                        <!-- Modal -->
                        <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="staticBackdropLabel">Ajouter Un  Nouvel Evenement</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                                <div class="modal-body">
                                    <div class="form-group required">
                                        <label for="title" class="control-label">Titre de l'evenement</label>
                                        <input type="text" class="form-control"  name="title" id="title" required>
                                    </div>
                                    <div class="form-group required">
                                        <label for="start" class="control-label">date de debut</label>
                                        <input type="date" class="form-control" name="start" id="start" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="end" class="control-label">date de fin</label>
                                        <input type="date" class="form-control" name="end" id="end" >
                                    </div>
                                    <div class="form-group">
                                        <label for="end" class="control-label">Couleur De Fond</label>
                                        <input type="color" class="form-control" name="background" id="background" >
                                    </div>
                                    <span style="color: red; background-color: rgb(195, 230, 246);" > <i id="error"></i></span>
                                </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                    <button onclick="addEvenement()" class="btn btn-primary">Ajouter</button>
                                </div>
                            </div>
                          </div>
                        </div>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Date De Debut</th><th>Date De Fin</th><th>Titre </th><th>Selection</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach(session('cal') as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->start }}</td>
                                        <td>{{ $item->end }}</td>
                                        <td>{{ $item->title }} </td>
                                        <td> 
                                            <input type="checkbox" name="value[]" value=" {{ $item->id }} " class="getvalue">
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="row mr-2" style="float: right;">
                            <button class="btn btn-danger" style="float: right;" onclick="confirmation()"> Supprimer les selections</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>

    @endsection