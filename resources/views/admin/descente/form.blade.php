<div class="form-group required {{ $errors->has('date_descente') ? 'has-error' : ''}}">
    <label for="date_descente" class="control-label">{{ 'Date Descente' }}</label>
    <input class="form-control dateramses" name="date_descente" type="date" id="date_descente" value="{{ isset($descente->date_descente) ? $descente->date_descente : ''}}" required>
    {!! $errors->first('date_descente', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('Frais_Descente') ? 'has-error' : ''}}">
    <label for="Frais_Descente" class="control-label">{{ 'Frais Descente' }}</label>
    

    <div class="input-group mb-3">
       <input class="form-control" name="Frais_Descente" type="number" min="0" id="Frais_Descente" value="{{ isset($descente->Frais_Descente) ? $descente->Frais_Descente : ''}}" required>
    {!! $errors->first('Frais_Descente', '<p class="help-block">:message</p>') !!}

    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
</div>
<div class="form-group {{ $errors->has('Chef_De_Quartier') ? 'has-error' : ''}}">
    <label for="Chef_De_Quartier" class="control-label">{{ 'Chef De Quartier' }}</label>
    <input class="form-control" name="Chef_De_Quartier" type="text" id="Chef_De_Quartier" value="{{ isset($descente->Chef_De_Quartier) ? $descente->Chef_De_Quartier : ''}}" >
    {!! $errors->first('Chef_De_Quartier', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('processus') ? 'has-error' : ''}}">
    <label for="processus" class="control-label">{{ 'Processus' }}</label>
    
    @if(count($processus) >= 1)
    <select class="form-control" name="processus" required>
        @foreach($processus as $item)
            <option

            @if(isset($descente->processus) && $descente->processus == $item->id)
                selected
            @endif
             value=" {{ $item->id }} "> {{ $item->nom_processus }} </option>
        @endforeach
    </select>
    @else
         <select class="form-control" name="processus" disabled="disabled">
            <option value=""> Aucun Processus </option>
        </select>
    @endif
    {!! $errors->first('processus', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $formMode === 'create' ? 'required' : ''}}  ">
    <label for="proprietaire" class="control-label">Choix Des Proprietaires</label>
    <select multiple="multiple"  name="proprietaire[]" id="proprietaire" class="form-control js-example-basic-multiple" @if($formMode === 'create') required @endif>
        @foreach($proprietaire as $item)

            <option value="{{$item->id}}">{{$item->noms}}</option>

        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="facilitateur" class="control-label">Choix Des Facilitateurs</label>
    <select multiple="multiple"  name="facilitateur[]" id="facilitateur" class="form-control js-example-basic-multiple">
        @foreach($facilitateur as $item)

            <option value="{{$item->id}}">{{$item->noms_Facilitateur}}</option>

        @endforeach
    </select>
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
