@extends('layouts.app')

@section('content')
    <div class="container"> 
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Reception</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/descente/'.$descente->id) }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                       

                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#staticBackdrop"><i class="fa fa-plus" aria-hidden="true"></i>
                          Ajouter Nouveau
                        </button>

                        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Ajouter Un Nouveau Frais</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

<form method="POST" action="{{ url('/admin/descente/reception') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

              <div class="modal-body">

                                
                                    {{ csrf_field() }}
 <div class="form-group required {{ $errors->has('date_de_reception') ? 'has-error' : ''}}">
    <label for="date_de_reception" class="control-label">{{ 'Date De Reception' }}</label>
    <input class="form-control" name="date_de_reception" type="date" id="date_de_reception" value="{{ isset($reception->date_de_reception) ? $reception->date_de_reception : ''}}" required>
    {!! $errors->first('date_de_reception', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('frais_de_reception') ? 'has-error' : ''}}">
    <label for="frais_de_reception" class="control-label">{{ 'Frais De Reception' }}</label>
    

    <div class="input-group mb-3">
       <input class="form-control" name="frais_de_reception" type="number" min="0" id="frais_de_reception" value="{{ isset($reception->frais_de_reception) ? $reception->frais_de_reception : ''}}" required>
    {!! $errors->first('frais_de_reception', '<p class="help-block">:message</p>') !!}

    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
</div>
<div class="form-group required {{ $errors->has('descente') ? 'has-error' : ''}}">
    <label for="descente" class="control-label">Descente</label>
    <input class="form-control" name="descente" type="hidden" id="descente" value="{{ $descente->id }}">

    <input class="form-control" type="text" value="Descente Du Processus '{{ $descente->nom_processus }}'" readonly="readonly">
</div>

                                
                              </div>
                              <div class="modal-footer">
                                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                              </div>
</form>
                            </div>
                          </div>
                        </div>











                        <form method="GET" action="{{ url('/admin/descente/'. $descente->id .'/reception') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Rechercher..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Date De Reception</th><th>Frais De Reception</th><th>Descente du processus</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($reception as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->date_de_reception }}</td><td>{{ $item->frais_de_reception }} FCFA</td><td>{{ $item->proc->nom_processus }}</td>
                                        <td>
                                            <a href="{{ url('/admin/reception/' . $item->id . '/edit') }}" title="Modifier Reception"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>

                                            
                                            <button type="submit" class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $item->id }},'/admin/reception/' + {{ $item->id }})"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $reception->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
