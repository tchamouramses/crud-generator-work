@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Descente {{ $descente->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/descente') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/descente/' . $descente->id . '/edit') }}" title="Modifier Descente"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>

                        <a href="{{ url('/admin/descente/' . $descente->id . '/participant') }}" title="Participant de la Descente"><button class="btn btn-primary btn-sm"><i class="fa fa-users" aria-hidden="true"></i> Participant</button></a>

                        <a href="{{ url('/admin/descente/' . $descente->id . '/opposition') }}" title="Opposition a la Descente"><button class="btn btn-primary btn-sm"><i class="fa fa-user-times" aria-hidden="true"></i> Opposition</button></a>

                        <a href="{{ url('/admin/descente/' . $descente->id . '/reception') }}" title="Reception de la Descente"><button class="btn btn-primary btn-sm"><i class="fas fa-glass-cheers" aria-hidden="true"></i> Reception</button></a>

                        <a href="{{ url('/admin/descente/' . $descente->id . '/administration') }}" title="Reception de la Descente"><button class="btn btn-primary btn-sm"><i class="fas fa-handshake" aria-hidden="true"></i> Commission Administrative</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $descente->id }}</td>
                                    </tr>
                                    <tr><th> Date Descente </th><td> {{ $descente->date_descente }} </td></tr><tr><th> Frais Descente </th><td> {{ $descente->Frais_Descente }} FCFA</td></tr><tr><th> Chef De Quartier </th><td> {{ $descente->Chef_De_Quartier }} </td></tr><tr><th> Processus </th><td> {{ $processus->nom_processus }} </td></tr>
                                    <tr>
                                        <th colspan="2"><h4 align="center">Proprietaire(s)</h4></th>
                                    </tr>
                                        @foreach($proprietaire as $item)

                                    <tr>
                                        <th> {{ $item->noms }}({{ $item->telephone }}) </th>
                                        <td>

                                            <button type="submit" class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $item->id }},'/admin/descente/' + {{ $item->id }} + '/deletePropDesc')"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button>
                                        </td>
                                    </tr>
                                        @endforeach
                                    <tr>
                                        <th colspan="2"><h4 align="center">Facilitateur(s)</h4></th>
                                    </tr>
                                        @foreach($facilitateur as $item)
                                    <tr>

                                        <th> {{ $item->noms }}({{ $item->telephone }}) </th>
                                        <td>
                                            
                                            <button type="submit" class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $item->id }},'/admin/descente/' + {{ $item->id }} + '/deleteFacDesc')"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button>
                                        </td>
                                    </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
