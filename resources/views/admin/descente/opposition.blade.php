@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Opposition</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/descente/'.$descente->id) }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#staticBackdrop"><i class="fa fa-plus" aria-hidden="true"></i>
                          Ajouter Opposition
                        </button>

                        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Ajouter Une Nouvelle Opposition</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

<form method="POST" action="{{ url('/admin/descente/opposition') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

              <div class="modal-body">
                {{ csrf_field() }}
         <div class="form-group required {{ $errors->has('noms') ? 'has-error' : ''}}">
    <label for="noms" class="control-label">{{ 'Noms' }}</label>
    <input class="form-control" name="noms" type="text" id="noms" value="{{ isset($opposition->noms) ? $opposition->noms : ''}}" required>
    {!! $errors->first('noms', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('motif') ? 'has-error' : ''}}">
    <label for="motif" class="control-label">{{ 'Motif' }}</label>
    <textarea class="form-control" rows="5" name="motif" type="textarea" id="motif" required>{{ isset($opposition->motif) ? $opposition->motif : ''}}</textarea>
    {!! $errors->first('motif', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('reglement') ? 'has-error' : ''}}">
    <label for="reglement" class="control-label">{{ 'Reglement' }}</label>
    <input class="form-control" name="reglement" type="text" id="reglement" value="{{ isset($opposition->reglement) ? $opposition->reglement : ''}}" >
    {!! $errors->first('reglement', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('Date_Intitule') ? 'has-error' : ''}}">
    <label for="Date_Intitule" class="control-label">{{ 'Date Intitule' }}</label>
    <input class="form-control" name="Date_Intitule" type="date" id="Date_Intitule" value="{{ isset($opposition->Date_Intitule) ? $opposition->Date_Intitule : ''}}" required>
    {!! $errors->first('Date_Intitule', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('descente') ? 'has-error' : ''}}">
    <label for="descente" class="control-label">Descente</label>
    <input class="form-control" name="descente" type="hidden" id="descente" value="{{ $descente->id }}">

    <input class="form-control" type="text" value="Descente Du Processus '{{ $descente->nom_processus }}'" readonly="readonly">
</div>

                                
                              </div>
                              <div class="modal-footer">
                                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                              </div>
</form>
                            </div>
                          </div>
                        </div>

                        <form method="GET" action="{{ url('/admin/descente/'. $descente->id .'/opposition') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Rechercher..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Noms</th><th>Motif</th><th>Reglement</th><th>Date Intitulée</th><th>Processus</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($opposition as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->noms }}</td>
                                        <td>{{ $item->motif }}</td>
                                        <td>{{ $item->reglement }}</td><td>{{ $item->Date_Intitule }}</td><td>{{ $item->proc->nom_processus }}</td>
                                        <td>
                                            <a href="{{ url('/admin/opposition/' . $item->id) }}" title="Details Reception"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/opposition/' . $item->id . '/edit') }}" title="Modifier Reception"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>

                                            
                                            <button type="submit" class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $item->id }},'/admin/opposition/' + {{ $item->id }})"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $opposition->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
