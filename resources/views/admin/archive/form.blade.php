<div class="form-group {{ $errors->has('Nom_Fichier') ? 'has-error' : ''}}">
    <label for="Nom_Fichier" class="control-label">{{ 'Nom Fichier' }} </label>
    <input class="form-control" name="Nom_Fichier" type="text" id="Nom_Fichier" value="{{ isset($archive->Nom_Fichier) ? $archive->Nom_Fichier : ''}}" >
    {!! $errors->first('Nom_Fichier', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('fichier') ? 'has-error' : ''}}">
    <label for="fichier" class="control-label">{{ 'Fichier' }}</label>
    <input class="form-control" multiple name="fichier[]" type="file" id="fichier" value="{{ isset($archive->fichier) ? $archive->fichier : ''}}" required>
    {!! $errors->first('fichier', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('processus') ? 'has-error' : ''}}">
    <label for="processus" class="control-label">{{ 'processus' }}</label>
    @if(!empty($processus))
    <select class="form-control" name="processus" required >
        @foreach($processus as $item)
            <option 
            @if(isset($archive->processus) && $item->id == $archive->processus)
                selected
            @endif

            value=" {{ $item->id }} "> {{ $item->nom_processus }} </option>
        @endforeach
    </select>
    @else
         <select class="form-control" name="processus" disabled="disabled">
            <option value=""> Aucun processus </option>
        </select>
    @endif
    {!! $errors->first('processus', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ 'Description' }}</label>
    <textarea class="form-control" rows="5" name="description" type="textarea" id="description" >{{ isset($archive->description) ? $archive->description : ''}}</textarea>
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
