@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Archive {{ $archive->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/archive') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/archive/' . $archive->id . '/edit') }}" title="Edit Archive"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $archive->id }}</td>
                                    </tr>
                                    <tr><th> Nom Fichier </th><td> {{ $archive->Nom_Fichier }} </td></tr><tr><th> Fichier </th><td> <a href="{{ url('storage/'.$archive->fichier) }}">
                                                <img src=" {{ url('assets/images/eye.png')}} ">
                                            </a> </td></tr><tr><th> Processus </th><td> {{ $archive->process->nom }} </td></tr><tr><th> Description </th><td> {{ $archive->description }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
