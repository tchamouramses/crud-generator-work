@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Facilitateur</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/facilitateur/create') }}" class="btn btn-success btn-sm" title="Add New Facilitateur">
                            <i class="fa fa-plus" aria-hidden="true"></i> Ajouter Nouveau
                        </a>

                        {!! Form::open(['method' => 'GET', 'url' => '/admin/facilitateur', 'class' => 'form-inline my-2 my-lg-0 float-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Noms Facilitateur</th><th>Telephone  Facilitateur1</th><th>Telephone  Facilitateur2</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($facilitateur as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->noms_Facilitateur }}</td><td>{{ $item->telephone_Facilitateur1 }}</td><td>{{ $item->telephone_Facilitateur2 }}</td>
                                        <td>
                                            <a href="{{ url('/admin/facilitateur/' . $item->id) }}" title="View Facilitateur"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/facilitateur/' . $item->id . '/edit') }}" title="Edit Facilitateur"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                              <button type="submit" class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $item->id }},'/admin/facilitateur/' + {{ $item->id }})"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $facilitateur->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
