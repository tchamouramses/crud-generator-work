<div class="form-group required {{ $errors->has('noms_Facilitateur') ? 'has-error' : ''}}">
    {!! Form::label('noms_Facilitateur', 'Noms Facilitateur', ['class' => 'control-label']) !!}
    {!! Form::text('noms_Facilitateur', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('noms_Facilitateur', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('telephone_Facilitateur1') ? 'has-error' : ''}}">
    {!! Form::label('telephone_Facilitateur1', 'Telephone Facilitateur1', ['class' => 'control-label']) !!}
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-phone"></i></span>
        </div>
        {!! Form::text('telephone_Facilitateur1', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('telephone_Facilitateur1', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('telephone_Facilitateur2') ? 'has-error' : ''}}">
    {!! Form::label('telephone_Facilitateur2', 'Telephone Facilitateur2', ['class' => 'control-label']) !!}
    
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-phone"></i></span>
        </div>
        {!! Form::text('telephone_Facilitateur2', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('telephone_Facilitateur2', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email_Facilitateur') ? 'has-error' : ''}}">
    {!! Form::label('email_Facilitateur', 'Email Facilitateur', ['class' => 'control-label']) !!}
    

    <div class="input-group mb-3">
        <div class="input-group-prepend">
         <span class="input-group-text"><i class="fas fa-envelope"></i></span>
         </div>
        {!! Form::text('email_Facilitateur', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('email_Facilitateur', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Modifier' : 'Ajouter', ['class' => 'btn btn-primary']) !!}
</div>
