@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Facilitateur {{ $facilitateur->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/facilitateur') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/facilitateur/' . $facilitateur->id . '/edit') }}" title="Edit Facilitateur"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>
                        
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $facilitateur->id }}</td>
                                    </tr>
                                    <tr><th> Noms Facilitateur </th><td> {{ $facilitateur->noms_Facilitateur }} </td></tr><tr><th> Telephone  Facilitateur1 </th><td> {{ $facilitateur->telephone_Facilitateur1 }} </td></tr><tr><th> Telephone  Facilitateur2 </th><td> {{ $facilitateur->telephone_Facilitateur2 }} </td></tr>
                                    <tr><th> Email </th><td> {{ $facilitateur->email_Facilitateur }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
