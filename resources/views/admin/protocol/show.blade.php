@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Protocol {{ $protocol->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/protocol') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/protocol/' . $protocol->id . '/edit') }}" title="Modifier Protocol"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $protocol->id }}</td>
                                    </tr>
                                    <tr><th> Date Signature </th><td> {{ $protocol->date_signature }} </td></tr><tr><th> Cabinet Notaire </th><td> {{ $protocol->Cabinet_Notaire }} </td></tr><tr><th> Clerc </th><td> {{ $protocol->clerc }} </td></tr><tr><th> Frais </th><td> {{ $protocol->Frais }} FCFA</td></tr><tr><th> Fichier Signe </th><td> {{ isset($protocol->Fichier_Signe) ? 'Confert Archive' : 'Pas de Rapport'}} </td></tr><tr><th> Processus </th><td> {{ $processus->nom_processus }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
