<div class="form-group required {{ $errors->has('date_signature') ? 'has-error' : ''}}">
    <label for="date_signature" class="control-label">{{ 'Date Signature' }}</label>
    <input class="form-control dateramses" name="date_signature" type="date" id="date_signature" value="{{ isset($protocol->date_signature) ? $protocol->date_signature : ''}}" required>
    {!! $errors->first('date_signature', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('Cabinet_Notaire') ? 'has-error' : ''}}">
    <label for="Cabinet_Notaire" class="control-label">{{ 'Cabinet Notaire' }}</label>
    <input class="form-control" name="Cabinet_Notaire" type="text" id="Cabinet_Notaire" value="{{ isset($protocol->Cabinet_Notaire) ? $protocol->Cabinet_Notaire : ''}}" required>
    {!! $errors->first('Cabinet_Notaire', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('clerc') ? 'has-error' : ''}}">
    <label for="clerc" class="control-label">{{ 'Clerc' }}</label>
    <input class="form-control" name="clerc" type="text" id="clerc" value="{{ isset($protocol->clerc) ? $protocol->clerc : ''}}" >
    {!! $errors->first('clerc', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('Frais') ? 'has-error' : ''}}">
    <label for="Frais" class="control-label">{{ 'Frais' }}</label>
    
    <div class="input-group mb-3">
        <input class="form-control" name="Frais" type="number" min="0" id="Frais" value="{{ isset($protocol->Frais) ? $protocol->Frais : ''}}" required>
    {!! $errors->first('Frais', '<p class="help-block">:message</p>') !!}

    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
</div>
<div class="form-group {{ $errors->has('Fichier_Signe') ? 'has-error' : ''}}">
    <label for="Fichier_Signe" class="control-label">{{ 'Fichier Signe' }}</label>
    <input class="form-control" name="Fichier_Signe" type="file" id="Fichier_Signe" value="{{ isset($protocol->Fichier_Signe) ? $protocol->Fichier_Signe : ''}}" >
    {!! $errors->first('Fichier_Signe', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('processus') ? 'has-error' : ''}}">
    <label for="processus" class="control-label">{{ 'Processus' }}</label>
    @if(count($processus) >= 1)
    <select class="form-control" name="processus" required>
        @foreach($processus as $item)
            <option
            @if(isset($protocol->processus) && $item->id == $protocol->processus)
                selected
            @endif
             value=" {{ $item->id }} "> {{ $item->nom_processus }} </option>
        @endforeach
    </select>
    @else
         <select class="form-control" name="processus" disabled="disabled">
            <option value=""> Aucun Processus </option>
        </select>
    @endif
    {!! $errors->first('processus', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
