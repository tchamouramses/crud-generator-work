@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Visite</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/visite/create') }}" class="btn btn-success btn-sm" title="Add New Visite">
                            <i class="fa fa-plus" aria-hidden="true"></i> Ajouter Nouveau
                        </a>

                        <form method="GET" action="{{ url('/admin/visite') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Date Visite</th><th>Frais</th><th>Surface</th><th>Processus</th><th>Effectuer</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($visite as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->Date_Visite }}</td><td>{{ $item->frais }} FCFA</td><td>{{ $item->surface }} m<sup>2</sup></td><td>{{ $item->Process->nom_processus }}</td><td> 
                                            @if($item->Effectuer == 0)
                                                Non 
                                            @else
                                                oui 
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ url('/admin/visite/' . $item->id) }}" title="View Visite"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/admin/visite/' . $item->id . '/edit') }}" title="Edit Visite"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>

                                            <button type="submit" class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $item->id }},'/admin/visite/' + {{ $item->id }})"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $visite->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
