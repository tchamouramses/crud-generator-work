@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Visite {{ $visite->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/visite') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/visite/' . $visite->id . '/edit') }}" title="Edit Visite"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $visite->id }}</td>
                                    </tr>
                                    <tr><th> Date Visite </th><td> {{ $visite->Date_Visite }} </td></tr><tr><th> Frais </th><td> {{ $visite->frais }} FCFA</td></tr><tr><th> Rapport </th><td> @if(!empty($visite->rapport))
                                        <a href="{{ url('storage/'.$visite->rapport) }}"><img src="{{ url('assets/images/eye.png') }}"></a>
                                    @endif </td></tr><tr><th> Surface </th><td> {{ $visite->surface }} m<sup>2</sup></td></tr><tr><th> Nature Du Sol </th><td> {{ $visite->Nature_Du_Sol }} </td></tr><tr><th> Nature Relief </th><td> {{ $visite->Nature_Relief }} </td></tr><tr><th> Observation </th><td> {{ $visite->observation }} </td></tr><tr><th> Processus </th><td> {{ $visite->Process->nom }} </td></tr><tr><th> Effectuer </th><td> 
                                        @if($visite->Effectuer == 0)
                                                Non 
                                            @endif
                                            @if($visite->Effectuer != 0)
                                                oui 
                                            @endif </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
