<div class="form-group required {{ $errors->has('Date_Visite') ? 'has-error' : ''}}">
    <label for="Date_Visite" class="control-label">{{ 'Date Visite' }}</label>

<input class="form-control dateramses" name="Date_Visite" type="date" id="Date_Visite" value="{{ isset($visite->Date_Visite) ? $visite->Date_Visite : ''}}" required>
    {!! $errors->first('Date_Visite', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('frais') ? 'has-error' : ''}}">
    <label for="frais" class="control-label">{{ 'Frais' }}</label>
    
    <div class="input-group mb-3">
        <input class="form-control" name="frais" type="number" min="0" id="frais" value="{{ isset($visite->frais) ? $visite->frais : ''}}" required >
    {!! $errors->first('frais', '<p class="help-block">:message</p>') !!}

    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
</div>
<div class="form-group {{ $errors->has('rapport') ? 'has-error' : ''}}">
    <label for="rapport" class="control-label">{{ 'Rapport' }}</label>
    <input class="form-control" name="rapport" type="file" id="rapport" value="{{ isset($visite->rapport) ? $visite->rapport : ''}}" >
    {!! $errors->first('rapport', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('surface') ? 'has-error' : ''}}">
    <label for="surface" class="control-label">{{ 'Surface' }}</label>
    
    <div class="input-group mb-3">
       <input class="form-control" name="surface" type="number" min="0" id="surface" value="{{ isset($visite->surface) ? $visite->surface : ''}}" required>
    {!! $errors->first('surface', '<p class="help-block">:message</p>') !!}

    <div class="input-group-append">
        <span class="input-group-text">m<sup>2</sup></span>
        </div>
    </div>
</div>
<div class="form-group {{ $errors->has('Nature_Du_Sol') ? 'has-error' : ''}}">
    <label for="Nature_Du_Sol" class="control-label">{{ 'Nature Du Sol' }}</label>
    <select name="Nature_Du_Sol" class="form-control" id="Nature_Du_Sol" >
    @foreach (json_decode('{"sec":"habitable","humide":"inhabitable","marecageux":"marecageux"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($visite->Nature_Du_Sol) && $visite->Nature_Du_Sol == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('Nature_Du_Sol', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('Nature_Relief') ? 'has-error' : ''}}">
    <label for="Nature_Relief" class="control-label">{{ 'Nature Relief' }}</label>
    <select name="Nature_Relief" class="form-control" id="Nature_Relief" >
    @foreach (json_decode('{"plat":"plat","montagneux":"montagneux","autre":"autre"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($visite->Nature_Relief) && $visite->Nature_Relief == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('Nature_Relief', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group required {{ $errors->has('processus') ? 'has-error' : ''}}">
    <label for="processus" class="control-label">{{ 'processus' }}</label>
    @if(!empty($processus))
    <select class="form-control" name="processus" required >
        @foreach($processus as $item)
            <option 

            @if(isset($visite->processus) && $item->id == $visite->processus)
                selected
            @endif

            value=" {{ $item->id }} "> {{ $item->nom_processus }} </option>
        @endforeach
    </select>
    @else
         <select class="form-control" name="processus" disabled="disabled">
            <option value=""> Aucun processus </option>
        </select>
    @endif

    {!! $errors->first('processus', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('Effectuer') ? 'has-error' : ''}}">
    <label for="Effectuer" class="control-label">{{ 'Effectuer?' }}</label>
    <select class="form-control" name="Effectuer" required >
        <option value="1">OUI</option>
        <option value="0">NON</option>
    </select>
    {!! $errors->first('Effectuer', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('observation') ? 'has-error' : ''}}">
    <label for="observation" class="control-label">{{ 'Observation' }}</label>
    <textarea class="form-control" rows="5" name="observation" type="textarea" id="observation" >{{ isset($visite->observation) ? $visite->observation : ''}}</textarea>
    {!! $errors->first('observation', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
