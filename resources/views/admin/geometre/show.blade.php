@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Geometre {{ $geometre->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/geometre') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/geometre/' . $geometre->id . '/edit') }}" title="Edit Geometre"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $geometre->id }}</td>
                                    </tr>
                                    <tr><th> Noms </th><td> {{ $geometre->noms }} </td></tr><tr><th> Telephone Geo1 </th><td> {{ $geometre->telephone_geo1 }} </td></tr><tr><th> Telephone Geo2 </th><td> {{ $geometre->telephone_geo2 }} </td></tr>
                                    <tr><th> Email </th><td> {{ $geometre->email }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
