<div class="form-group required {{ $errors->has('noms') ? 'has-error' : ''}}">
    <label for="noms" class="control-label">{{ 'Noms' }}</label>
    <input class="form-control" name="noms" type="text" id="noms" value="{{ isset($geometre->noms) ? $geometre->noms : ''}}" required>
    {!! $errors->first('noms', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('telephone_geo1') ? 'has-error' : ''}}">
    <label for="telephone_geo1" class="control-label">{{ 'Telephone Geo1' }}</label>
    
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-phone"></i></span>
        </div>
        <input class="form-control" name="telephone_geo1" type="text" id="telephone_geo1" value="{{ isset($geometre->telephone_geo1) ? $geometre->telephone_geo1 : ''}}" >
    {!! $errors->first('telephone_geo1', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('telephone_geo2') ? 'has-error' : ''}}">
    <label for="telephone_geo2" class="control-label">{{ 'Telephone Geo2' }}</label>
    
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-phone"></i></span>
        </div>
        <input class="form-control" name="telephone_geo2" type="text" id="telephone_geo2" value="{{ isset($geometre->telephone_geo2) ? $geometre->telephone_geo2 : ''}}" >
    {!! $errors->first('telephone_geo2', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    
    <div class="input-group mb-3">
        <div class="input-group-prepend">
         <span class="input-group-text"><i class="fas fa-envelope"></i></span>
         </div>
        <input class="form-control" name="email" type="text" id="email" value="{{ isset($geometre->email) ? $geometre->email : ''}}" >
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
