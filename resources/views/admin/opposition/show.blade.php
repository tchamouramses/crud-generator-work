@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Opposition {{ $descente->proces }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/descente/'.$descente->id.'/opposition') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/opposition/' . $opposition->id . '/edit') }}" title="Modifier Opposition"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $opposition->id }}</td>
                                    </tr>
                                    <tr><th> Noms </th><td> {{ $opposition->noms }} </td></tr><tr><th> Motif </th><td> {{ $opposition->motif }} </td></tr><tr><th> Reglement </th><td> {{ $opposition->reglement }} </td></tr><tr><th> Date Intitulé </th><td> {{ $opposition->Date_Intitule }} </td></tr><tr><th> Descente Pour le Processus </th><td> {{ $descente->proces }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
