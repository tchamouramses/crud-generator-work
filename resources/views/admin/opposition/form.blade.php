<div class="form-group required {{ $errors->has('noms') ? 'has-error' : ''}}">
    <label for="noms" class="control-label">{{ 'Noms' }}</label>
    <input class="form-control" name="noms" type="text" id="noms" value="{{ isset($opposition->noms) ? $opposition->noms : ''}}" required>
    {!! $errors->first('noms', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('motif') ? 'has-error' : ''}}">
    <label for="motif" class="control-label">{{ 'Motif' }}</label>
    <textarea class="form-control" rows="5" name="motif" type="textarea" id="motif" required>{{ isset($opposition->motif) ? $opposition->motif : ''}}</textarea>
    {!! $errors->first('motif', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('reglement') ? 'has-error' : ''}}">
    <label for="reglement" class="control-label">{{ 'Reglement' }}</label>
    <input class="form-control" name="reglement" type="text" id="reglement" value="{{ isset($opposition->reglement) ? $opposition->reglement : ''}}" >
    {!! $errors->first('reglement', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('Date_Intitule') ? 'has-error' : ''}}">
    <label for="Date_Intitule" class="control-label">{{ 'Date Intitule' }}</label>
    <input class="form-control" name="Date_Intitule" type="date" id="Date_Intitule" value="{{ isset($opposition->Date_Intitule) ? $opposition->Date_Intitule : ''}}" required>
    {!! $errors->first('Date_Intitule', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('descente') ? 'has-error' : ''}}">
    <label for="descente" class="control-label">{{ 'Descente' }}</label>
    <input class="form-control" name="descente" type="number" min="0" id="descente" value="{{ isset($opposition->descente) ? $opposition->descente : ''}}" >
    {!! $errors->first('descente', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
