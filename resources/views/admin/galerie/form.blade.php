<div class="form-group required {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image', 'Image', ['class' => 'control-label']) !!}
    {!! Form::file('image', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('id_visite') ? 'has-error' : ''}}">
    {!! Form::label('id_visite', 'Id Visite', ['class' => 'control-label']) !!}
    {!! Form::number('id_visite', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('id_visite', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Modifier' : 'Ajouter', ['class' => 'btn btn-primary']) !!}
</div>
