
                        	<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                        		<label class="control-label">Image</label>
                        		<input class="form-control" type="file" name="image" required="required">
                        		{!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                        	</div>
                        	@if (!empty($site))
                        	<div class="form-group {{ $errors->has('id_visite') ? 'has-error' : ''}}">
                        		<label class="control-label"> Choix du site</label>
                        		<select name="id_visite" class="form-control">
                        			@foreach($site as $item)
                        				<option value="{{ $item->id }}">{{ $item->quartier }}</option>
                                	@endforeach
                        		</select>
                        	</div>
                        	@endif
                        	@if (empty($site))
                        		<div class="form-group {{ $errors->has('id_visite') ? 'has-error' : ''}}">
                        		<label class="control-label"> Choix du site</label>
                        		<select name="id_visite" disabled="disabled" class="form-control">
                        				<option value="">Aucun site enregisté</option>
                        		</select>
                        	</div>
	                        @endif

	                        <div class="form-group">
    							{!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary']) !!}
							</div>
