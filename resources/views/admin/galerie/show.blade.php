@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Galerie {{ $galerie->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/galerie') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/galerie/' . $galerie->id . '/edit') }}" title="Edit Galerie"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>
                        
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $galerie->id }}</td>
                                    </tr>
                                    <tr><th> Image </th><td> {{ $galerie->image }} </td></tr><tr><th> Id Visite </th><td> {{ $galerie->id_visite }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
