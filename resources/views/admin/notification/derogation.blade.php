@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
<?php 
  use App\Assistan\Assistan;
  $notification = Assistan::getNotification();
  $ariane = ['derogation(s) urgente(s)'];
?>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h5 align="center">Derogation(s) prevue(s) dans moins de {{$notification->duree}} jours Dans l'ordre croissant</h5></div>
                    <div class="card-body">
                        <br>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>N°</th><th>Date Derogation</th><th>Frais</th><th>Processus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($notification->derogations as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->date_derogation }}</td><td>{{ $item->frais }} Fcfa</td><td>{{ $item->nom_processus }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
