@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
<?php 
  use App\Assistan\Assistan;
  $notification = Assistan::getNotification();
  $ariane = ['demande(s) urgente(s)'];
?>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"> <h5 align="center">Demande(s) prevue(s) dans moins de {{$notification->duree}} jours Dans l'ordre croissant</h5></div>
                    <div class="card-body">
                        <br>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Date Demande</th><th>Frais</th><th>Nature</th><th>Processus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($notification->demandes as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->date_demande }}</td><td>{{ $item->frais }} FCFA</td><td>{{ $item->nature }}</td><td>{{ $item->nom_processus }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
