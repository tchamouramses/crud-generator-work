@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
<?php 
  use App\Assistan\Assistan;
  $notification = Assistan::getNotification();
  $ariane = ['descente(s) urgente'];
?>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h5 align="center">Descente(s) prevue(s) dans moins de {{$notification->duree}} jours Dans l'ordre croissant</h5></div>
                    <div class="card-body">
                        <br>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Date Descente</th><th>Frais Descente</th><th>Chef De Quartier</th><th>Processus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($notification->descentes as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->date_descente }}</td><td>{{ $item->Frais_Descente }} FCFA</td><td>{{ $item->Chef_De_Quartier }}</td><td>{{ $item->nom_processus }}</td>
                                        
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
