@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
<?php 
  use App\Assistan\Assistan;
  $notification = Assistan::getNotification();
  $ariane = ['bornage(s) urgent(s)'];
?>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h5 align="center">Bornage(s) prevue(s) dans moins de {{$notification->duree}} jours Dans l'ordre croissant</h5></div>
                    <div class="card-body">
                        <br>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>N°</th><th>Date Bornage</th><th>Frais</th><th>Geometre</th><th>Processus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($notification->bornages as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->date_bornage }}</td><td>{{ $item->frais }} FCFA</td><td>{{ $item->nom_geo }} ({{ $item->tel }}) </td><td>{{ $item->nom_processus }}</td>
                                        
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
