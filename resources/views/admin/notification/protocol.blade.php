@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
<?php 
  use App\Assistan\Assistan;
  $notification = Assistan::getNotification();
  $ariane = ['protocol(s) urgent(s)'];
?>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"> <h5 align="center">Protocol(s) prevue(s) dans moins de {{$notification->duree}} jours Dans l'ordre croissant</h5></div>
                    <div class="card-body">
                        <br>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Date Signature</th><th>Cabinet Notaire</th><th>Clerc</th><th>Frais</th><th>Processus</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($notification->protocols as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->date_signature }}</td><td>{{ $item->Cabinet_Notaire }}</td><td>{{ $item-> clerc }}</td><td>{{ $item->Frais }} FCFA</td><td>{{ $item->nom_processus }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
