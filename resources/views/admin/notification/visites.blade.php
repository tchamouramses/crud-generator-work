@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
<?php 
  use App\Assistan\Assistan;
  $notification = Assistan::getNotification();
  $ariane = ['visite(s) urgente'];
?>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h5 align="center">Visite(s) prevue(s) dans moins de {{$notification->duree}} jours Dans l'ordre croissant</h5></div>
                    <div class="card-body">
                        <br>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Date Visite</th><th>Frais</th><th>Surface</th><th>Processus</th><th>Effectuer</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($notification->visites as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->Date_Visite }}</td><td>{{ $item->frais }} Fcfa</td><td>{{ $item->surface }} m<sup>2</sup></td><td>{{ $item->nom_processus }}</td><td> 
                                            @if($item->Effectuer == 0)
                                                Non 
                                            @endif
                                            @if($item->Effectuer != 0)
                                                oui 
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
