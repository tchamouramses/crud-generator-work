@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
<?php 
  use App\Assistan\Assistan;
  $notification = Assistan::getNotification();
  $ariane = ['reception(s) urgente(s)'];
?>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"> <h5 align="center">Reception(s) prevue(s) dans moins de {{$notification->duree}} jours Dans l'ordre croissant</h5></div>
                    <div class="card-body">
                        <br>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Date Reception</th><th>Frais</th><th>Descente</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($notification->receptions as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->date_de_reception }}</td><td>{{ $item->frais_de_reception }} FCFA</td><td>{{ $item->processus->nom_processus }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
