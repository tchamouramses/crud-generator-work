@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Demarcheur {{ $demarcheur->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/demarcheur') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/demarcheur/' . $demarcheur->id . '/edit') }}" title="Edit Demarcheur"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $demarcheur->id }}</td>
                                    </tr>
                                    <tr><th> Noms </th><td> {{ $demarcheur->noms }} </td></tr><tr><th> Telephone1 </th><td> {{ $demarcheur->telephone1 }} </td></tr><tr><th> Telephone2 </th><td> {{ $demarcheur->telephone2 }} </td></tr>
                                    <tr><th> Email </th><td> {{ $demarcheur->email }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
