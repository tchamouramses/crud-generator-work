<div class="form-group required {{ $errors->has('noms') ? 'has-error' : ''}} ">
    <label for="noms" class="control-label">{{ 'Noms' }}</label>
    <input class="form-control" name="noms" type="text" id="noms" value="{{ isset($demarcheur->noms) ? $demarcheur->noms : ''}}" required="required">
    {!! $errors->first('noms', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('telephone1') ? 'has-error' : ''}}">
    <label for="telephone1" class="control-label">{{ 'Telephone1' }}</label>
    
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-phone"></i></span>
        </div>
        <input class="form-control" name="telephone1" type="text" id="telephone1" value="{{ isset($demarcheur->telephone1) ? $demarcheur->telephone1 : ''}}" required>
    {!! $errors->first('telephone1', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('telephone2') ? 'has-error' : ''}}">
    <label for="telephone2" class="control-label">{{ 'Telephone2' }}</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-phone"></i></span>
        </div>
        <input class="form-control" name="telephone2" type="text" id="telephone2" value="{{ isset($demarcheur->telephone2) ? $demarcheur->telephone2 : ''}}" >
    {!! $errors->first('telephone2', '<p class="help-block">:message</p>') !!}

    </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
         <span class="input-group-text"><i class="fas fa-envelope"></i></span>
         </div>
        <input class="form-control" name="email" type="text" id="email" value="{{ isset($demarcheur->email) ? $demarcheur->email : ''}}" >
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
