@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Commission {{ $commission->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/commission') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/commission/' . $commission->id . '/edit') }}" title="Edit Commission"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $commission->id }}</td>
                                    </tr>
                                    <tr><th> Commission Facilitateurs </th><td> {{ $commission->Commission_Facilitateurs }} FCFA</td></tr><tr><th> Commission Demarcheur </th><td> {{ $commission->Commission_Demarcheur }} FCFA</td></tr><tr><th> Autres Commissions </th><td> {{ $commission->Autres_Commissions }} FCFA</td></tr><tr><th> Processus </th><td> {{ $processus->nom_processus }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
