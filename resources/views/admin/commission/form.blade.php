<div class="form-group {{ $errors->has('Commission_Facilitateurs') ? 'has-error' : ''}}">
    <label for="Commission_Facilitateurs" class="control-label">{{ 'Commission Facilitateurs' }}</label>
    <div class="input-group mb-3">
    <input class="form-control" name="Commission_Facilitateurs" type="number" min="0" id="Commission_Facilitateurs" value="{{ isset($commission->Commission_Facilitateurs) ? $commission->Commission_Facilitateurs : '0'}}" >
    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
    {!! $errors->first('Commission_Facilitateurs', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('Commission_Demarcheur') ? 'has-error' : ''}}">
    <label for="Commission_Demarcheur" class="control-label">{{ 'Commission Demarcheur' }}</label>
    <div class="input-group mb-3">
    <input class="form-control" name="Commission_Demarcheur" type="number" min="0" id="Commission_Demarcheur" value="{{ isset($commission->Commission_Demarcheur) ? $commission->Commission_Demarcheur : '0'}}" >

    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
    {!! $errors->first('Commission_Demarcheur', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('Autres_Commissions') ? 'has-error' : ''}}">
    <label for="Autres_Commissions" class="control-label">{{ 'Autres Commissions' }}</label>
    <div class="input-group mb-3">
    <input class="form-control" name="Autres_Commissions" type="number" min="0" id="Autres_Commissions" value="{{ isset($commission->Autres_Commissions) ? $commission->Autres_Commissions : '0'}}" >
    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
    {!! $errors->first('Autres_Commissions', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('processus') ? 'has-error' : ''}}">
    <label for="processus" class="control-label">{{ 'processus' }}</label>
    @if(!empty($processus))
    <select class="form-control" name="processus" required >
        @foreach($processus as $item)
            <option value=" {{ $item->id }} "> {{ $item->nom_processus }} </option>
        @endforeach
    </select>
    @else
         <select class="form-control" name="processus" disabled="disabled">
            <option value=""> Aucun processus </option>
        </select>
    @endif

    {!! $errors->first('processus', '<p class="help-block">:message</p>') !!}
</div>

@if(!empty($processus))
<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
@else
<div class="form-group">
    <button class="btn btn-primary" disabled="disabled" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}"></button>
</div>
@endif