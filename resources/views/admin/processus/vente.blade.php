@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Vente Ou Achat  {{ isset($vente->nom_processus) ?  $vente->nom_processus : ''}} </div>
                    <div class="card-body">
                        @if(isset($vente))
                        <a href="{{ url('/admin/processus/'.$processus->id) }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>


<button type="button" @if(isset($vente->is_finish) && $vente->is_finish) disabled @endif title="Ajouter d'un Nouveau Document" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#staticBackdrop1"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                          Modifier
                        </button>

                        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop1" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Modifier la vente ou l'achat</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

<form method="post" action="{{ url('/admin/processus/'.$vente->id.'/updatevente') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

              <div class="modal-body">

{{ csrf_field() }}

<div class="form-group required {{ $errors->has('date_vente') ? 'has-error' : ''}}">
    <label for="date_vente" class="control-label">{{ 'Date De Vente' }}</label>
    <input class="form-control" name="date_vente" type="date" id="date_vente" value="{{ isset($vente->date_vente) ? $vente->date_vente : ''}}" required>
    {!! $errors->first('date_vente', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('frais') ? 'has-error' : ''}}">
    <label for="frais" class="control-label">{{ 'Frais' }}</label>
   
    <div class="input-group mb-3">
     <input class="form-control" name="frais" type="number" min="0" id="frais" value="{{ isset($vente->frais) ? $vente->frais : ''}}" required>
    {!! $errors->first('frais', '<p class="help-block">:message</p>') !!}

    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
</div>
<div class="form-group required {{ $errors->has('nature') ? 'has-error' : ''}}">
    <label for="nature" class="control-label">{{ 'Nature' }}</label>
    <select name="nature" class="form-control" id="nature" required>
        <option @if($vente->nature == 'Achat') selected @endif value="Achat">Achat</option>
        <option @if($vente->nature == 'Vente') selected @endif value="Vente">Vente</option>
    </select>
    {!! $errors->first('nature', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group required {{ $errors->has('processus') ? 'has-error' : ''}}">
    <label for="processus" class="control-label">{{ 'Processus' }}</label>
    <input type="hidden" name="processus" value=" {{ $processus->id }} ">
    <input class="form-control" type="text" readonly="readonly" value="{{ $processus->nom_processus }}">
    {!! $errors->first('processus', '<p class="help-block">:message</p>') !!}
</div>

</div>
                              <div class="modal-footer">
                                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-primary">Modifie</button>
                              </div>
</form>
    </div>
  </div>
</div>






                        <button type="submit" @if($vente->is_finish) disabled @endif class="btn btn-danger btn-sm  deleted_element" title="Supprimer La Vente Ou l'Achat" onclick="return alertDeleteElement({{ $vente->id }},'/admin/processus/' + {{ $vente->id }} + '/deletevente')"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr><th> Date De Vente </th><td> {{ $vente->date_vente }} </td></tr><tr><th> Frais </th><td> {{ $vente->frais }} FCFA</td></tr><tr><th> Processus </th><td> {{ $vente->nom_processus }} </td></tr><tr><th> Nature </th><td> {{ $vente->nature }} </td></tr>
                                </tbody>
                            </table>
                        </div>
                        @else

                            <div class="container" align="center">
                                <h4>Aucune Vente Ou Achat Pour ce processus</h4>
<button type="button" @if(isset($vente->is_finish) && $vente->is_finish) disabled @endif title="Ajouter d'un Nouveau Document" class="btn btn-success btn-sm" data-toggle="modal" data-target="#staticBackdrop"><i class="fa fa-plus" aria-hidden="true"></i>
                          Ajouter Une Nouvelle vente
                        </button>

                        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Ajouter Une  Vente ou un Achat</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

<form method="POST" action="{{ url('/admin/processus/vente') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

              <div class="modal-body">

{{ csrf_field() }}

<div class="form-group required {{ $errors->has('date_vente') ? 'has-error' : ''}}">
    <label for="date_vente" class="control-label">{{ 'Date De Vente' }}</label>
    <input class="form-control" name="date_vente" type="date" id="date_vente" required>
    {!! $errors->first('date_vente', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('frais') ? 'has-error' : ''}}">
    <label for="frais" class="control-label">{{ 'Frais' }}</label>
   
    <div class="input-group mb-3">
     <input class="form-control" name="frais" type="number" min="0" id="frais" required>
    {!! $errors->first('frais', '<p class="help-block">:message</p>') !!}

    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
</div>
<div class="form-group required {{ $errors->has('nature') ? 'has-error' : ''}}">
    <label for="nature" class="control-label">{{ 'Nature' }}</label>
    <select name="nature" class="form-control" id="nature" required>
        <option value="Achat">Achat</option>
        <option value="Vente" selected>Vente</option>
    </select>
    {!! $errors->first('nature', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group required {{ $errors->has('processus') ? 'has-error' : ''}}">
    <label for="processus" class="control-label">{{ 'Processus' }}</label>
    <input type="hidden" name="processus" value=" {{ $processus->id }} ">
    <input class="form-control" type="text" readonly="readonly" value="{{ $processus->nom_processus }}">
    {!! $errors->first('processus', '<p class="help-block">:message</p>') !!}
</div>

</div>
                              <div class="modal-footer">
                                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                              </div>
</form>
    </div>
  </div>
</div>

                            </div>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
