@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">processus {{ $processu->nom_processus }}</div>
                    <div class="card-body">
                      <a href="{{ url('/admin/processus/'.$processu->id) }}" title="Retour" ><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        


<button type="button" @if($processu->is_finish) disabled @endif title="Ajouter d'un Nouveau Document" class="btn btn-success btn-sm" data-toggle="modal" data-target="#staticBackdrop"><i class="fa fa-plus" aria-hidden="true"></i>
                          Nouveau Document
                        </button>

                        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Ajouter Un  Nouveau Document</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

<form method="post" action="{{ url('/admin/processus/archive') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

              <div class="modal-body">

{{ csrf_field() }}

<div class="form-group {{ $errors->has('Nom_Fichier') ? 'has-error' : ''}}">
    <label for="Nom_Fichier" class="control-label">{{ 'Nom Fichier' }} </label>
    <input class="form-control" name="Nom_Fichier" type="text" id="Nom_Fichier" >
    {!! $errors->first('Nom_Fichier', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('fichier') ? 'has-error' : ''}}">
    <label for="fichier" class="control-label">{{ 'Fichier' }}</label>
    <input class="form-control" name="fichier[]" multiple type="file" id="fichier" required>
    {!! $errors->first('fichier', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('processus') ? 'has-error' : ''}}">
    <label for="processus" class="control-label">{{ 'Processus' }}</label>
    <input type="hidden" name="processus" value=" {{ $processu->id }} ">
    <input class="form-control" type="text" readonly="readonly" value="{{ $processu->nom_processus }}">
    {!! $errors->first('processus', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ 'Description' }}</label>
    <textarea class="form-control" rows="5" name="description" type="textarea" id="description" ></textarea>
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>

</div>
                              <div class="modal-footer">
                                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                              </div>
</form>
    </div>
  </div>
</div>


@if(count($archive) != 0)








                        <div class="row col-md-12">
                          @foreach($archive as $item)
                            <div class="col-md-2 border border-success rounded " align="center" style="margin-top: 10px;">
                              <a href="{{ url('storage/'.$item->fichier) }}">
                                <img src="{{ url('/'.$item->images) }}" style="width: 100%; margin-top: 10px;">
                              </a>
                              
                              <label class="form-label col-md-12"> {{ $item->Nom_Fichier }} </label>
                              <div class="row">

                                <div class="col-md-12"  style="margin-bottom: 3px;">
                                  <button type="submit" class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $item->id }},'/admin/archive/' + {{ $item->id }})"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button>
                                </div>

                              </div>
                            </div>
                          @endforeach
                        </div>
                         @else

                          <div class="container" align="center">
                            <h4>Aucun Document Pour ce processus</h4>
                          </div>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
