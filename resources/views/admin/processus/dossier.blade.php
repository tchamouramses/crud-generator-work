@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Membre Dossier Technique Et Frais </div>
                    <div class="card-body">
                        <a href="{{ url('/admin/processus/'.$processus->id) }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>

                        <button type="button" @if($processus->is_finish) disabled @endif class="btn btn-success btn-sm" data-toggle="modal" data-target="#staticBackdrop"><i class="fa fa-plus" aria-hidden="true"></i>
                          Ajouter Nouveau
                        </button>

                        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Ajouter Un Nouveau Frais</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

<form method="POST" action="{{ url('/admin/processus/dossier') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

              <div class="modal-body">

                                
                                    {{ csrf_field() }}
 <div class="form-group required {{ $errors->has('frais') ? 'has-error' : ''}}">
    <label for="frais" class="control-label">Frais</label>

    <div class="input-group mb-3">
    <input class="form-control" name="frais" type="number" min="0" id="frais" required>
    {!! $errors->first('frais', '<p class="help-block">:message</p>') !!}

    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>

</div>
 <div class="form-group required {{ $errors->has('processus') ? 'has-error' : ''}}">
    <label for="processus" class="control-label">Processus</label>
    <input class="form-control" name="processus" type="hidden" id="processus" value="{{ $processus->id }}">

    <input class="form-control" type="text" value="Processus '{{ $processus->nom_processus }}'" readonly="readonly">
</div>
 <div class="form-group required {{ $errors->has('membre') ? 'has-error' : ''}}">
    <label for="membre" class="control-label">Commission Administrative</label>
@if(count($admin) >= 1)
    <select class="form-control" name="membre" required>
        @foreach($admin as $item)
            <option value=" {{ $item->id }} "> {{ $item->noms }} </option>
        @endforeach
    </select>
    @else
         <select class="form-control" name="membre" disabled="disabled">
            <option value=""> Aucun Membre de Commission </option>
        </select>
    @endif
    {!! $errors->first('membre', '<p class="help-block">:message</p>') !!}
</div>



                                
                              </div>
                              <div class="modal-footer">
                                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                              </div>
</form>
                            </div>
                          </div>
                        </div>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>N°</th><th>Noms</th><th>Frais</th><th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($dossier as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->noms }}</td><td>{{ $item->frais }}  FCFA</td>
                                        <td>
                                            <button type="submit" @if($processus->is_finish) disabled @endif class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $item->id }},'/admin/processus/' + {{ $item->id }} + '/deleteDossier')"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
