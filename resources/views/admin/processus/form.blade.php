<div class="form-group required {{ $errors->has('nom_processus') ? 'has-error' : ''}}">
    <label for="nom_processus" class="control-label">{{ 'Nom Du Processus' }}</label>
    <input type="text" class="form-control" name="nom_processus" id="nom_processus" value="{{ isset($processu->nom_processus) ? $processu->nom_processus : ''}}" required >
    {!! $errors->first('nom_processus', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group required {{ $errors->has('nom') ? 'has-error' : ''}}">
    <label for="nom" class="control-label">{{ 'Nature' }}</label>
    <select class="form-control" name="nom" required >
        <option 

        @if(isset($processu->nom) && $processu->nom == 'Immatriculation')
            selected
        @endif value="Immatriculation" selected="selected">Immatriculation</option>
        <option 

        @if(isset($processu->nom) && $processu->nom == 'Lotissement')
            selected
        @endif

        value="Lotissement">Lotissement</option>
        <option 

         
        @if(isset($processu->nom) && $processu->nom == 'Vente_Achat')
            selected
        @endif

        value="Vente_Achat">Vente_Achat</option>
    </select>
    {!! $errors->first('nom', '<p class="help-block">:message</p>') !!}
</div>
    @if (!empty($site))
    <div class="form-group required {{ $errors->has('id_visite') ? 'has-error' : ''}}">
        <label class="control-label"> Choix du site</label>
        <select name="Site" class="form-control" required >
            @foreach($site as $item)
                <option value="{{ $item->id }}">{{ $item->quartier }}</option>
            @endforeach
        </select>
    </div>
    @else
        <div class="form-group {{ $errors->has('id_visite') ? 'has-error' : ''}}">
        <label class="control-label"> Choix du site</label>
        <select name="site" disabled="disabled" class="form-control">
                <option value="">Aucun site enregisté</option>
        </select>
    </div>
    @endif

<div class="form-group {{ $errors->has('Observation') ? 'has-error' : ''}}">
    <label for="Observation" class="control-label">{{ 'Observation' }}</label>
    <textarea class="form-control" rows="5" name="Observation" type="textarea" id="Observation" >{{ isset($processu->Observation) ? $processu->Observation : ''}}</textarea>
    {!! $errors->first('Observation', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <label for="demarcheur" class="control-label">Choix Des Demarcheurs</label>
    <select multiple="multiple"  name="demarcheur[]" id="demarcheur" class="form-control js-example-basic-multiple" >
        @foreach($demarcheur as $item)

            <option value="{{$item->id}}">{{$item->noms}}</option>

        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="geometre" class="control-label">Choix Des Geometres</label>
    <select multiple="multiple"  name="geometre[]" id="geometre" class="form-control js-example-basic-multiple">
        @foreach($geometre as $item)

            <option value="{{$item->id}}">{{$item->noms}}</option>

        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="facilitateur" class="control-label">Choix Des Facilitateur</label>
    <select multiple="multiple"  name="facilitateur[]" id="facilitateur" class="form-control js-example-basic-multiple">
        @foreach($facilitateur as $item)

            <option value="{{$item->id}}">{{$item->noms_Facilitateur}}</option>

        @endforeach
    </select>
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
