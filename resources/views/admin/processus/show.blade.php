@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Processus {{ $processu->nom_processus }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/processus') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/processus/' . $processu->id . '/edit') }}" title="Edit Processu"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifer</button></a>

                        <a href="{{ url('/admin/processus/' . $processu->id . '/archive') }}" title="Edit Processu"><button class="btn btn-primary btn-sm"><i class="fa fa-archive" aria-hidden="true"></i> Documents</button></a>

                        <a href="{{ url('/admin/processus/' . $processu->id . '/commission') }}" title="Commission"><button class="btn btn-primary btn-sm"><i class="fa fa-dollar-sign" aria-hidden="true"></i> Commission</button></a>

                        @if($processu->nom == 'Immatriculation')
                            <a href="{{ url('/admin/processus/' . $processu->id . '/visite') }}" title="Etat de visite du processus"><button class="btn btn-primary btn-sm"><i class="fa fa-calendar-alt" aria-hidden="true"></i> Visite</button></a>

                            <a href="{{ url('/admin/processus/' . $processu->id . '/dossier') }}" title="Dossier Technique"><button class="btn btn-primary btn-sm"><i class="fa fa-folder" aria-hidden="true"></i> Dossier Technique </button></a>

                            <a href="{{ url('/admin/processus/' . $processu->id . '/derogation') }}" title="Derogation"><button class="btn btn-primary btn-sm"><i class="fa fa-cubes" aria-hidden="true"></i> Derogation </button></a>
                            

                            <a href="{{ url('/admin/processus/' . $processu->id . '/demande') }}" title="Demande"><button class="btn btn-primary btn-sm"><i class="fa fa-envelope" aria-hidden="true"></i> Demande </button></a>
                        @endif
                         @if($processu->nom == 'Vente_Achat')
                            <a href="{{ url('/admin/processus/' . $processu->id . '/vente') }}" title="Derogation"><button class="btn btn-primary btn-sm"><i class="fas fa-money-bill-alt" aria-hidden="true"></i> Vente Ou Achat </button></a>
                            
                         @endif

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr><th>Statut</th><td colspan="2">
                                        @if($processu->is_finish)
                                                <span class="fas fa-lock"> Fermé</span>
                                            @else
                                                <span class="fas fa-lock-open"> Ouvert</span>
                                            @endif

                                    </td></tr>
                                    <tr><th> Nom </th><td colspan="2"> {{ $processu->nom_processus }} </td></tr>
                                    <tr><th> Nature </th><td colspan="2"> {{ $processu->nom }} </td></tr><tr><th> Site </th><td colspan="2"> {{ $site->quartier }} </td></tr><tr><th> Observation </th><td colspan="2"> {{ $processu->Observation }} </td></tr>
                                    <tr>
                                        <th>Demarcheurs</th>
                                        <th>Facilitateurs</th>
                                        <th>Geometres</th>
                                    </tr>
                                    <tr>
                                        <td>


                                            <div class="row col-md-12">
                                            @foreach($processu->demarcheur as $dem)
                                                <div class="col-md-10">
                                                    {{ $dem->dem->noms }} ({{ $dem->dem->telephone1 }})
                                                </div>
                                                <div class="col-md-2">
                                                    <button type="submit" class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $dem->id }},'/admin/processus/' + {{ $dem->id }} +'/deleteDem')"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                </div>
                                            @endforeach
                                            </div>
                                        <td>


                                            <div class="row col-md-12">
                                            @foreach($processu->facilitateur as $dem)
                                                <div class="col-md-10">
                                                   {{ $dem->fac->noms_Facilitateur }} ({{ $dem->fac->telephone_Facilitateur1 }})
                                                </div>
                                                <div class="col-md-2">
                                                    <button type="submit" class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $dem->id }},'/admin/processus/' + {{ $dem->id }} +'/deleteFac')"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                </div>
                                            @endforeach
                                            </div>
                                            
                                                

                                        </td>
                                        <td>
                                            <div class="row col-md-12">
                                           @foreach($processu->geometre as $dem)
                                                <div class="col-md-10">
                                                {{ $dem->geo->noms }} ({{ $dem->geo->telephone_geo1 }}) 
                                                </div>
                                                <div class="col-md-2">

                                                    <button type="submit" class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $dem->id }},'/admin/processus/' + {{ $dem->id }} +'/deleteGeo')"><i class="fa fa-trash-o" aria-hidden="true"></i></button>

                                                </div>
                                            @endforeach
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
