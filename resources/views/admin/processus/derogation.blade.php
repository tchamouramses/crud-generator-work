@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Details De la Derogation du processus {{ $processus->nom_processus }} </div>
                    <div class="card-body">
                        <a href="{{ url('/admin/processus/'.$processus->id) }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                      @if($derogation)
                        <button type="submit" @if($processus->is_finish) disabled @endif class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $derogation->id }},'/admin/processus/' + {{ $derogation->id }} + '/deletedero')"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr><th> Date Derogation </th><td> {{ $derogation->date_derogation }} </td></tr><tr><th> Frais </th><td> {{ $derogation->frais }} FCFA</td></tr><tr><th> processus </th><td> {{ $processus->nom_processus }} </td></tr>
                                    <tr><th colspan="2"> <h5 align="center">Facilitateur(s) </h5></th></tr>
                                    @foreach($dero as $item)
                                        <tr>
                                            <th colspan="2">
                                                {{ $item->noms_Facilitateur }} ({{ $item->  telephone_Facilitateur1 }})
                                            </th>
                                        </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else

                          <div class="container" align="center">
                            <h4>Aucune Derogation Pour ce processus</h4>
                            


<button type="button" @if($processus->is_finish) disabled @endif title="Ajouter d'un Nouveau Document" class="btn btn-success btn-sm" data-toggle="modal" data-target="#staticBackdrop"><i class="fa fa-plus" aria-hidden="true"></i>
                          Nouveau Document
                        </button>

                        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Ajouter Une  Derogation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

<form method="POST" action="{{ url('/admin/processus/derogation') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

              <div class="modal-body">

{{ csrf_field() }}

<div class="form-group required {{ $errors->has('date_derogation') ? 'has-error' : ''}}">
    <label for="date_derogation" class="control-label">{{ 'Date Derogation' }}</label>
    <input class="form-control dateramses" name="date_derogation" type="date" id="date_derogation" required>
    {!! $errors->first('date_derogation', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('frais') ? 'has-error' : ''}}">
    <label for="frais" class="control-label">{{ 'Frais' }}</label>
    

    <div class="input-group mb-3">
        <input class="form-control" name="frais" type="number" min="0" id="frais" required>
        {!! $errors->first('frais', '<p class="help-block">:message</p>') !!}

    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
</div>
<div class="form-group required {{ $errors->has('processus') ? 'has-error' : ''}}">
    <label for="processus" class="control-label">{{ 'Processus' }}</label>
    <input type="hidden" name="processus" value=" {{ $processus->id }} ">
    <input class="form-control" type="text" readonly="readonly" value="{{ $processus->nom_processus }}">
    {!! $errors->first('processus', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('facilitateur') ? 'has-error' : ''}}">
    <label for="processus" class="control-label">{{ 'Facilitateurs' }}</label>
    @if(!empty($facilitateur))
    <select multiple="multiple" class="form-control js-example-basic-multiple" name="facilitateur[]" id="facilitateur">
        @foreach($facilitateur as $item)
            <option value="{{ $item->id }}"> {{ $item->noms_Facilitateur }} </option>
        @endforeach
    </select>
    @else
         <select class="form-control" name="facilitateur" disabled="disabled">
            <option value=""> Aucun Facilitateur </option>
        </select>
    @endif

    {!! $errors->first('processus', '<p class="help-block">:message</p>') !!}
</div>

</div>
                              <div class="modal-footer">
                                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                              </div>
</form>
    </div>
  </div>
</div>


                        </div>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
