@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Demande du processus {{ $processus->nom_processus }} </div>
                    <div class="card-body">
                        <a href="{{ url('/admin/processus/'.$processus->id) }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        @if(count($demande) != 0)
                        
                            <button type="submit" @if($processus->is_finish) disabled @endif class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $demande[0]->id }},'/admin/processus/' + {{ $demande[0]->id }} + '/deletedemande')"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr><th> Date Demande </th><td> {{ $demande[0]->date_demande }} </td></tr><tr><th> Frais </th><td> {{ $demande[0]->frais }}</td></tr><tr><th> Nature </th><td> {{ $demande[0]->nature }} </td></tr><tr><th> Processus </th><td> {{ $processus->nom_processus }} </td></tr>
                                </tbody>
                            </table>
                        </div>
                        @else

                            <div class="container" align="center">
                                <h4>Aucune Demande Pour ce processus</h4>




<button type="button" @if($processus->is_finish) disabled @endif title="Ajouter d'un Nouveau Document" class="btn btn-success btn-sm" data-toggle="modal" data-target="#staticBackdrop"><i class="fa fa-plus" aria-hidden="true"></i>
                          Ajouter Une Nouvelle demande
                        </button>

                        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Ajouter Une  Demnade</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

<form method="POST" action="{{ url('/admin/processus/demande') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

              <div class="modal-body">

{{ csrf_field() }}

<div class="form-group required {{ $errors->has('date_demande') ? 'has-error' : ''}}">
    <label for="date_demande" class="control-label">{{ 'Date Demande' }}</label>
    <input class="form-control dateramses" name="date_demande" type="date" id="date_demande" value="{{ isset($demande->date_demande) ? $demande->date_demande : ''}}" required>
    {!! $errors->first('date_demande', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('frais') ? 'has-error' : ''}}">
    <label for="frais" class="control-label">{{ 'Frais' }}</label>
   
    <div class="input-group mb-3">
     <input class="form-control" name="frais" type="number" min="0" id="frais" value="{{ isset($demande->frais) ? $demande->frais : ''}}" required>
    {!! $errors->first('frais', '<p class="help-block">:message</p>') !!}

    <div class="input-group-append">
        <span class="input-group-text">FCFA | M<sup>2</sup></span>
        </div>
    </div>
</div>
<div class="form-group required {{ $errors->has('nature') ? 'has-error' : ''}}">
    <label for="nature" class="control-label">{{ 'Nature' }}</label>
    <select name="nature" class="form-control" id="nature" required>
    @foreach (json_decode('{"Espece":"Espece","Surface":"Surface"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($demande->nature) && $demande->nature == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('nature', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group required {{ $errors->has('processus') ? 'has-error' : ''}}">
    <label for="processus" class="control-label">{{ 'Processus' }}</label>
    <input type="hidden" name="processus" value=" {{ $processus->id }} ">
    <input class="form-control" type="text" readonly="readonly" value="{{ $processus->nom_processus }}">
    {!! $errors->first('processus', '<p class="help-block">:message</p>') !!}
</div>

</div>
                              <div class="modal-footer">
                                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                              </div>
</form>
    </div>
  </div>
</div>

                            </div>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
