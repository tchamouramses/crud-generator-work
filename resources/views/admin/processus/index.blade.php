@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Processus </div>
                   <!--  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-primary">
                    Launch Primary Modal
                    </button>
                     -->
                    <div class="card-body">
                        <a href="{{ url('/admin/processus/create') }}" class="btn btn-success btn-sm" title="Add New Processu">
                            <i class="fa fa-plus" aria-hidden="true"></i> Ajouter Nouveau
                        </a>

                        <form method="GET" action="{{ url('/admin/processus') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Nom</th><th>Nature</th><th>Site</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                @foreach($processus as $item)
                                    <tr>
                                        <td>@if($item->is_finish)
                                                <span class="fas fa-lock text-danger"></span>
                                            @else
                                                <span class="fas fa-lock-open text-success"></span>
                                            @endif
                                        </td>
                                        <td>{{ $item->nom_processus }}</td><td>{{ $item->nom }}</td><td>{{ $item->sites->quartier }}</td>
                                        <td>
                                            <a href="{{ url('/admin/processus/' . $item->id) }}" title="Details du processus"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> </button></a>
                                            <a href="{{ url('/admin/processus/' . $item->id . '/edit') }}" title="Modifier le processus"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>

                                            @if(Auth::user()->admin == 'ADMIN')
                                            <a href="{{ url('/admin/processus/' . $item->id.'/close') }}" title="Cloturer le processus"><button class="btn btn-success btn-sm"><i class="fa fa-key" aria-hidden="true"></i></button></a>
                                            @endif
                                            <button type="submit" @if($item->is_finish) disabled @endif class="btn btn-danger btn-sm  deleted_element" title="Delete Demarcheur" onclick="return alertDeleteElement({{ $item->id }},'/admin/processus/' + {{ $item->id }})"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $processus->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
