@extends('layouts.app')

@section('content')
<div  data-select2-id="49">
    <div class="container"  data-select2-id="48">
        <div class="row" data-select2-id="47">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Ajouter un Processus</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/processus') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/admin/processus') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            @include ('admin.processus.form', ['formMode' => 'create'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
