@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Commission {{ $processus->nom_processus}} </div>
                    <div class="card-body">
                        <a href="{{ url('/admin/processus/'.$processus->id) }}" title="Retour" ><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        @if(count($commission) != 0)
                        <button type="submit" @if($processus->is_finish) disabled @endif class="btn btn-danger btn-sm  deleted_element" title="Delete Commission" onclick="return alertDeleteElement({{ $commission[0]->id }},'/admin/commission/' + {{ $commission[0]->id }})"><i class="fa fa-trash-o" aria-hidden="true"></i> Supprimer</button>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $commission[0]->id }}</td>
                                    </tr>
                                    <tr><th> Commission Facilitateurs </th><td> {{ $commission[0]->Commission_Facilitateurs  }} Fcfa</td></tr><tr><th> Commission Demarcheur </th><td> {{ $commission[0]->Commission_Demarcheur }} Fcfa</td></tr><tr><th> Autres Commissions </th><td> {{ $commission[0]->Autres_Commissions }} Fcfa</td></tr>
                                    <tr><th> processus </th><td> {{ $processus->nom_processus }} </td></tr>

                                    <tr><th> Montant Total Commission</th><td> {{ $commission[0]->Commission_Geometre + $commission[0]->Commission_Facilitateurs +  $commission[0]->Autres_Commissions + $commission[0]->Commission_Demarcheur}}  FcFa</td></tr>
                                </tbody>
                            </table>
                        </div>

                        @else
                        <div class="container" align="center">
                            <h4>Aucune commission Enregistrée</h4>
                            <!-- <a href="{{ url('/admin/commission/create') }}" class="btn btn-success btn-sm" title="Add New Processu">
                            <i class="fa fa-plus" aria-hidden="true"></i> Ajouter une Nouvelle
                        </a> -->

<button type="button" @if($processus->is_finish) disabled @endif title="Ajouter d'un Nouveau Document" class="btn btn-success btn-sm" data-toggle="modal" data-target="#staticBackdrop"><i class="fa fa-plus" aria-hidden="true"></i>
                          Ajouter Une Nouvelle Commission
                        </button>

                        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Ajouter Un  Nouveau Document</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

<form method="POST" action="{{ url('/admin/processus/commission') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">

              <div class="modal-body">

{{ csrf_field() }}
<div class="form-group {{ $errors->has('Commission_Facilitateurs') ? 'has-error' : ''}}">
    <label for="Commission_Facilitateurs" class="control-label">{{ 'Commission Facilitateurs' }}</label>
    <div class="input-group mb-3">
    <input class="form-control" name="Commission_Facilitateurs" type="number" min="0" id="Commission_Facilitateurs" value="{{ isset($commission->Commission_Facilitateurs) ? $commission->Commission_Facilitateurs : '0'}}" >
    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
    {!! $errors->first('Commission_Facilitateurs', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('Commission_Demarcheur') ? 'has-error' : ''}}">
    <label for="Commission_Demarcheur" class="control-label">{{ 'Commission Demarcheur' }}</label>
    <div class="input-group mb-3">
    <input class="form-control" name="Commission_Demarcheur" type="number" min="0" id="Commission_Demarcheur" value="{{ isset($commission->Commission_Demarcheur) ? $commission->Commission_Demarcheur : '0'}}" >

    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
    {!! $errors->first('Commission_Demarcheur', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('Autres_Commissions') ? 'has-error' : ''}}">
    <label for="Autres_Commissions" class="control-label">{{ 'Autres Commissions' }}</label>
    <div class="input-group mb-3">
    <input class="form-control" name="Autres_Commissions" type="number" min="0" id="Autres_Commissions" value="{{ isset($commission->Autres_Commissions) ? $commission->Autres_Commissions : '0'}}" >
    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
    {!! $errors->first('Autres_Commissions', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('processus') ? 'has-error' : ''}}">
    
<label for="processus" class="control-label">{{ 'Processus' }}</label>
    <input type="hidden" name="processus" value=" {{ $processus->id }} ">
    <input class="form-control" type="text" readonly="readonly" value="{{ $processus->nom_processus }}">
    {!! $errors->first('processus', '<p class="help-block">:message</p>') !!}
</div>
</div>
                              <div class="modal-footer">
                                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                                <button type="submit" class="btn btn-primary">Ajouter</button>
                              </div>
</form>
    </div>
  </div>
</div>







                        </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
