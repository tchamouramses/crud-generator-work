@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Visite {{ $processus->nom_processus }}</div>
                    <div class="card-body">
                        @if(count($visite) != 0)
                        <a href="{{ url('/admin/processus/'. $processus->id) }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>

                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $visite[0]->id }}</td>
                                    </tr>
                                    <tr><th> Date Visite </th><td> {{ $visite[0]->Date_Visite }} </td></tr><tr><th> Frais </th><td> {{ $visite[0]->frais }} FCFA</td></tr><tr><th> Rapport </th><td> @if(!empty($visite[0]->rapport))
                                        <a href="{{ url('storage/'.$visite[0]->rapport) }}"><img src="{{ url('assets/images/eye.png') }}"></a>
                                    @endif </td></tr><tr><th> Surface </th><td> {{ $visite[0]->surface }} m<sup>2</sup></td></tr><tr><th> Nature Du Sol </th><td> {{ $visite[0]->Nature_Du_Sol }} </td></tr><tr><th> Nature Relief </th><td> {{ $visite[0]->Nature_Relief }} </td></tr><tr><th> Observation </th><td> {{ $visite[0]->observation }} </td></tr><tr><th> Processus </th><td> {{ $processus->nom_processus }} </td></tr><tr><th> Visite </th><td>
                                        @if($visite[0]->Effectuer)
                                            Effectuer
                                        @endif
                                        @if(!$visite[0]->Effectuer)
                                            Non Effectuer
                                        @endif
                                         </td></tr>
                                </tbody>
                            </table>
                        </div>
                        @endif

                         @if(count($visite) == 0)
                        <div class="container" align="center">
                            <h4>Aucune Visite Programmée ou effectuée</h4>
                            <a href="{{ url('/admin/visite/create') }}" class="btn btn-success btn-sm" title="Ajouter Une Nouvelle Visite">
                            <i class="fa fa-plus" aria-hidden="true"></i> Ajouter une Nouvelle
                        </a>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
