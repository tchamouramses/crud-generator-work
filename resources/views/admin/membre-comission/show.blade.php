@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Membre De Commission {{ $membrecomission->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/membre-comission') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/membre-comission/' . $membrecomission->id . '/edit') }}" title="Modifier MembreComission"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $membrecomission->id }}</td>
                                    </tr>
                                    <tr><th> Noms </th><td> {{ $membrecomission->noms }} </td></tr><tr><th> Contact </th><td> {{ $membrecomission->Contact }} </td></tr><tr><th> Status </th><td> {{ $membrecomission->status }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
