<div class="form-group {{ $errors->has('noms') ? 'has-error' : ''}}">
    <label for="noms" class="control-label">{{ 'Noms' }}</label>
    <input class="form-control" name="noms" type="text" id="noms" value="{{ isset($membrecomission->noms) ? $membrecomission->noms : ''}}" required>
    {!! $errors->first('noms', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('Contact') ? 'has-error' : ''}}">
    <label for="Contact" class="control-label">{{ 'Contact' }}</label>
    <input class="form-control" name="Contact" type="text" id="Contact" value="{{ isset($membrecomission->Contact) ? $membrecomission->Contact : ''}}" >
    {!! $errors->first('Contact', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group required {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">{{ 'Status' }}</label>
    <select class="form-control" name="status" required >
            <option value="DESCENTE">Membre Descente</option>
            <option value="DOSSIER_TECHNIQUE">Dossier Technique</option>
            <option value="DEUX">Participant Des deux</option>
    </select>

    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
