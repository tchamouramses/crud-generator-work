@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Bornage {{ $bornage->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/bornage') }}" title="Retour"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/bornage/' . $bornage->id . '/edit') }}" title="Modifier Bornage"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $bornage->id }}</td>
                                    </tr>
                                    <tr><th> Geometre </th><td> {{ $geo->noms }} ( {{ $geo->telephone_geo1 }} ) </td></tr><tr><th> Frais </th><td> {{ $bornage->frais }} FCFA</td></tr><tr><th> Date Bornage </th><td> {{ $bornage->date_bornage }} </td></tr><tr><th> Processus </th><td> {{ $processus->nom_processus }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
