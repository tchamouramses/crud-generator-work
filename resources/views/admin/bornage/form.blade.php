<div class="form-group  required {{ $errors->has('Geometre') ? 'has-error' : ''}}">
    <label for="geometre" class="control-label">{{ 'Geometre' }}</label>
    @if(count($geometre) >= 1)
    <select class="form-control" name="Geometre" id="geometre">
        @foreach($geometre as $item)
            <option 
                @if(isset($bornage->Geometre) && $item->id == $bornage->Geometre)
                    selected
                @endif
            value=" {{ $item->id }} "> {{ $item->noms }} </option>
        @endforeach
    </select>
    @else
         <select class="form-control" name="Geometre" disabled="disabled">
            <option value=""> Aucun Geometre </option>
        </select>
    @endif

    {!! $errors->first('geometre', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('frais') ? 'has-error' : ''}}">
    <label for="frais" class="control-label">{{ 'Frais' }}</label>
    

    
    <div class="input-group mb-3">
        <input class="form-control" name="frais" type="number" id="frais" value="{{ isset($bornage->frais) ? $bornage->frais : ''}}" required min="0">
    {!! $errors->first('frais', '<p class="help-block">:message</p>') !!}

    <div class="input-group-append">
        <span class="input-group-text">FCFA</span>
        </div>
    </div>
</div>
<div class="form-group required {{ $errors->has('date_bornage') ? 'has-error' : ''}}">
    <label for="date_bornage" class="control-label">{{ 'Date Bornage' }}</label>
    <input class="form-control dateramses" name="date_bornage" type="date" id="date_bornage" value="{{ isset($bornage->date_bornage) ? $bornage->date_bornage : ''}}" required>
    {!! $errors->first('date_bornage', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('Processus') ? 'has-error' : ''}}">
    <label for="processus" class="control-label">{{ 'Processus' }}</label>
    @if(count($processus) >= 1)
    <select class="form-control" name="processus" required>
        @foreach($processus as $item)
            <option
                @if(isset($bornage->processus) && $item->id == $bornage->processus)
                    selected
                @endif

             value=" {{ $item->id }} "> {{ $item->nom_processus }} </option>
        @endforeach
    </select>
    @else
         <select class="form-control" name="processus" disabled="disabled">
            <option value=""> Aucun Processus </option>
        </select>
    @endif

    {!! $errors->first('Processus', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input

    @if(count($processus) < 1 || count($geometre) < 1)
        disabled
    @endif

     class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Modifier' : 'Ajouter' }}">
</div>
