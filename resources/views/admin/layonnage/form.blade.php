<div class="form-group required {{ $errors->has('noms_Layonneur') ? 'has-error' : ''}}">
    {!! Form::label('noms_Layonneur', 'Noms Layonneur', ['class' => 'control-label']) !!}
    {!! Form::text('noms_Layonneur', null, ('required' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('noms_Layonneur', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group required {{ $errors->has('telephone__Layonneur1') ? 'has-error' : ''}}">
    {!! Form::label('telephone__Layonneur1', 'Telephone  Layonneur1', ['class' => 'control-label']) !!}
    
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-phone"></i></span>
        </div>
       {!! Form::text('telephone__Layonneur1', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('telephone__Layonneur1', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('telephone__Layonneur2') ? 'has-error' : ''}}">
    {!! Form::label('telephone__Layonneur2', 'Telephone  Layonneur2', ['class' => 'control-label']) !!}
    
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-phone"></i></span>
        </div>
        {!! Form::text('telephone__Layonneur2', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('telephone__Layonneur2', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email_Layonneur') ? 'has-error' : ''}}">
    {!! Form::label('email_Layonneur', 'Email Layonneur', ['class' => 'control-label']) !!}
    
    <div class="input-group mb-3">
        <div class="input-group-prepend">
         <span class="input-group-text"><i class="fas fa-envelope"></i></span>
         </div>
        {!! Form::text('email_Layonneur', null, ('' == 'required') ? ['class' => 'form-control', 'required' => 'required'] : ['class' => 'form-control']) !!}
    {!! $errors->first('email_Layonneur', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    {!! Form::submit($formMode === 'edit' ? 'Modifier' : 'Ajouter', ['class' => 'btn btn-primary']) !!}
</div>
