@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Layonneur {{ $layonnage->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/layonnage') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</button></a>
                        <a href="{{ url('/admin/layonnage/' . $layonnage->id . '/edit') }}" title="Edit Layonnage"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Modifier</button></a>
                        
                        
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $layonnage->id }}</td>
                                    </tr>
                                    <tr><th> Noms Layonneur </th><td> {{ $layonnage->noms_Layonneur }} </td></tr><tr><th> Telephone  Layonneur1 </th><td> {{ $layonnage->telephone__Layonneur1 }} </td></tr><tr><th> Telephone  Layonneur2 </th><td> {{ $layonnage->telephone__Layonneur2 }} </td></tr>
                                    <tr><th> Email </th><td> {{ $layonnage->email_Layonneur }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
