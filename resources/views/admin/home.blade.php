@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Home</div>
                    <div class="card-body">


    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ $site }}</h3>

                <p><b>Site(s) enregistré(s)</b></p>
              </div>
              <div class="icon">
                <i class="fas fa-globe"></i>
              </div>
              <a href=" {{ url('admin/site') }} " class="small-box-footer">Voir les Sites <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3> {{ $pro }} </h3>

                <p><b>Processus Stocker</b></p>
              </div>
              <div class="icon">
                <i class="fas fa-atom"></i>
              </div>
              <a href="{{ url('admin/processus') }}" class="small-box-footer">Voir les Processus <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3> {{ $dem }} </h3>

                <p><b>Demarcheur(s)</b></p>
              </div>
              <div class="icon">
                <i class="fas fa-users"></i>
              </div>
              <a href="{{ url('admin/demarcheur') }}" class="small-box-footer">Voir les Demarcheurs &nbsp;<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3> {{ $fac }} </h3>

                <p><b>Facilitateur(s)</b></p>
              </div>
              <div class="icon">
                <i class="fas fa-users"></i>
              </div>
              <a href="{{ url('admin/facilitateur') }}" class="small-box-footer">Voir les Facilitateurs&nbsp;<i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->



<div class="row col-md-12 mt-3">
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-gray">
              <div class="inner">
                <h3> {{ $immatriculation }} </h3>

                <p>Dossier d'Immatriculations</p>
              </div>
              <div class="icon">
                <i class="fas fa-folder-open"></i>
              </div>
              <a href="#" class="small-box-footer">Immatriculation</a>
            </div>
          </div>
          <!-- ./col -->

          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-gray">
              <div class="inner">
                <h3> {{ $lotissement }} </h3>

                <p>Dossier de Lotissement</p>
              </div>
              <div class="icon">
                <i class="fas fa-folder-open"></i>
              </div>
              <a href="#" class="small-box-footer">Lotissement</i></a>
            </div>
          </div>
          <!-- ./col -->

          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-gray">
              <div class="inner">
                <h3> {{ $vente }} </h3>

                <p>Dossier d'Achat et de Vente</p>
              </div>
              <div class="icon">
                <i class="fas fa-folder-open"></i>
              </div>
              <a href="#" class="small-box-footer">Achat et de Vente</i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>
</div>


        

      </div><!-- /.container-fluid -->
    </section>



                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
