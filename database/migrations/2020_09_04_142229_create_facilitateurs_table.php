<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFacilitateursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilitateurs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('noms_Facilitateur');
            $table->string('telephone_Facilitateur1');
            $table->string('telephone_Facilitateur2')->nullable();
            $table->string('email_Facilitateur')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('facilitateurs');
    }
}
