<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMembreComissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membre_comissions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('noms');
            $table->enum('status',['DESCENTE','DOSSIER_TECHNIQUE','DEUX']);
            $table->string('Contact')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('membre_comissions');
    }
}
