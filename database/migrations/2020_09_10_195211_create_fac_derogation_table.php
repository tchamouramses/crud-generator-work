<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacDerogationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fac_derogations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('facilitateur')->index();
            $table->unsignedInteger('derogation')->index();
            $table->foreign('derogation')->references('id')->on('derogations')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('facilitateur')->references('id')->on('facilitateurs')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fac_derogations');
    }
}
