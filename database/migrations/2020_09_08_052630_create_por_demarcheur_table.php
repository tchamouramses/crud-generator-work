<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePorDemarcheurTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('por_demarcheur', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedInteger('processus');
            $table->unsignedInteger('demarcheur');
             $table->foreign('processus')->references('id')->on('processuses')->onDelete('cascade')->onUpdate('cascade');
              $table->foreign('demarcheur')->references('id')->on('demarcheurs')->onDelete('cascade')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('por_demarcheur');
    }
}
