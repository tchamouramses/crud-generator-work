<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sites', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('region');
            $table->string('departement');
            $table->string('arrondissement');
            $table->string('village');
            $table->string('quartier');
            $table->double('surface');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('situation_Geopraphique');
            $table->string('situation_De_Ville');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sites');
    }
}
