<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visites', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('Date_Visite');
            $table->double('frais')->default(0);
            $table->string('rapport')->nullable();
            $table->double('surface')->default(0);
            $table->string('Nature_Du_Sol');
            $table->string('Nature_Relief');
            $table->text('observation')->nullable();
            $table->unsignedInteger('processus');
            $table->boolean('Effectuer')->default(0);
            $table->foreign('processus')->references('id')->on('processuses')->onDelete('cascade')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('visites');
    }
}
