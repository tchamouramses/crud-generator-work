<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDemarcheursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demarcheurs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('noms');
            $table->string('telephone1');
            $table->string('telephone2')->nullable();
            $table->string('email')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('demarcheurs');
    }
}
