<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonLayonageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person_layonages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('layonner');
            $table->unsignedInteger('layonnage');
            $table->foreign('layonner')->references('id')->on('layonners')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('layonnage')->references('id')->on('layonnages')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('person_layonages');
    }
}
