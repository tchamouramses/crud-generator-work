<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProcessusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processuses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->enum('nom',['Immatriculation','Lotissement','Vente_Achat']);
            $table->string('nom_processus')->unique();
            $table->unsignedInteger('Site')->index();
            $table->text('Observation')->nullable();
            $table->boolean('is_finish')->nullable()->default(0);
            $table->foreign('Site')->references('id')->on('sites')->onDelete('cascade')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('processuses');
    }
}
