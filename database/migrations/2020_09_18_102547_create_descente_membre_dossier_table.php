<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDescenteMembreDossierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membre_dossiers', function (Blueprint $table) {
            $table->increments('id');
            $table->double('frais')->default(0);
            $table->unsignedInteger('membre');
            $table->unsignedInteger('processus');
            $table->foreign('membre')->references('id')->on('membre_comissions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('processus')->references('id')->on('processuses')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membre_dossiers');
    }
}
