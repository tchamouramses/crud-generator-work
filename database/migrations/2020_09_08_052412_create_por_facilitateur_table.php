<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePorFacilitateurTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('por_facilitateur', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedInteger('processus');
            $table->unsignedInteger('facilitateur');
             $table->foreign('processus')->references('id')->on('processuses')->onDelete('cascade')->onUpdate('cascade');
              $table->foreign('facilitateur')->references('id')->on('facilitateurs')->onDelete('cascade')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('por_facilitateur');
    }
}
