<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacDescenteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fac_descentes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('facilitateur');
            $table->unsignedInteger('descente');
            $table->foreign('facilitateur')->references('id')->on('facilitateurs')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('descente')->references('id')->on('descentes')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fac_descentes');
    }
}
