<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePorGeometreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('por_geometre', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedInteger('processus');
            $table->unsignedInteger('geometre');
             $table->foreign('processus')->references('id')->on('processuses')->onDelete('cascade')->onUpdate('cascade');
              $table->foreign('geometre')->references('id')->on('geometres')->onDelete('cascade')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('por_geometre');
    }
}
