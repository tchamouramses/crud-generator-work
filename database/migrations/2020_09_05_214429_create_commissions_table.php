<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->double('Commission_Facilitateurs')->default(0);
            $table->double('Commission_Demarcheur')->default(0);
            $table->double('Autres_Commissions')->default(0);
            $table->unsignedInteger('processus')->index();
            $table->foreign('processus')->references('id')->on('processuses')->onDelete('cascade')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('commissions');
    }
}
