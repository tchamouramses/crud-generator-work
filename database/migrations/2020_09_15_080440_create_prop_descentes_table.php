<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropDescentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prop_descentes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('proprietaire');
            $table->unsignedInteger('descente');
            $table->foreign('proprietaire')->references('id')->on('proprietaires')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('descente')->references('id')->on('descentes')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prop_descentes');
    }
}
