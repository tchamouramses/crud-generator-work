<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRiverinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riverins', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('noms');
            $table->string('telephone')->nullable();
            $table->unsignedInteger('descente');
            $table->enum('status',['Notable','Riverin']);
            $table->foreign('descente')->references('id')->on('descentes')->onDelete('cascade')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('riverins');
    }
}
