<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDescenteMembreCommissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('descente_membre_commissions', function (Blueprint $table) {
            $table->increments('id');
            $table->double('frais')->default(0);
            $table->unsignedInteger('membre');
            $table->unsignedInteger('descente');
            $table->foreign('membre')->references('id')->on('membre_comissions')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('descente')->references('id')->on('descentes')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('descente_membre_commissions');
    }
}
