<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLayonnagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layonnages', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('noms_Layonneur');
            $table->string('telephone__Layonneur1');
            $table->string('telephone__Layonneur2')->nullable();
            $table->string('email_Layonneur')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('layonnages');
    }
}
