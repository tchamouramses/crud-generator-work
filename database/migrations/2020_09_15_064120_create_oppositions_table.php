<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOppositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oppositions', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('noms');
            $table->text('motif');
            $table->string('reglement')->nullable();
            $table->date('Date_Intitule');
            $table->unsignedInteger('descente');
            $table->foreign('descente')->references('id')->on('descentes')->onDelete('cascade')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('oppositions');
    }
}
