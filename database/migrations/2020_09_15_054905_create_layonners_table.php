<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLayonnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('layonners', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->date('date_debut');
            $table->unsignedInteger('processus');
            $table->double('frais');
            $table->integer('duree');
            $table->float('Surface');
            $table->foreign('processus')->references('id')->on('processuses')->onDelete('cascade')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('layonners');
    }
}
