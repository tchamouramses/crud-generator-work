<?php

use Illuminate\Database\Seeder;
use App\MembreComission;

class AdministrationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	MembreComission::truncate();
        $membre = [
            [
                'noms' => 'AFFAIRE FONCIERE DEPARTEMENTAL',
                'status' => "DOSSIER_TECHNIQUE"
            ],
            [
                'noms' => 'DELEGUE DEPARTEMENTAL DU MINDCAF',
                'status' => "DOSSIER_TECHNIQUE"
            ],
            [
                'noms' => 'SERVICE REGIONAL CADASTRE',
                'status' => "DEUX"
            ],
            [
                'noms' => 'DELEGUE DEPARTEMENTAL DU MINDCAF',
                'status' => "DOSSIER_TECHNIQUE"
            ],
            [
                'noms' => 'DELEGUE REGIONAL',
                'status' => "DOSSIER_TECHNIQUE"
            ],
            [
                'noms' => 'AFFAIRE FONCIERE REGIONAL',
                'status' => "DOSSIER_TECHNIQUE"
            ],
            [
                'noms' => 'DAF REGIONAL',
                'status' => "DOSSIER_TECHNIQUE"
            ],
            [
                'noms' => 'PUBLICATION JOURNAL OFFICIEL',
                'status' => "DOSSIER_TECHNIQUE"
            ],
            [
                'noms' => 'CONSERVATEUR ',
                'status' => "DOSSIER_TECHNIQUE"
            ],
            [
                'noms' => 'GEOMETRE',
                'status' => "DESCENTE"
            ],
            [
                'noms' => 'SERVICE REGIONAL DE L\'URBANISME',
                'status' => "DESCENTE"
            ],
            [
                'noms' => 'MAIRIE',
                'status' => "DESCENTE"
            ]
        ];

        foreach ($membre as $membre) {
            MembreComission::create($membre);
        }


    }
}
