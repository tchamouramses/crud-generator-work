<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	User::truncate();
        $user = [
                'name' => 'SOB NOUPA',
                'admin' => 'ADMIN',
                'email' => "jpnoupa@yahoo.fr",
                'quartier' => 'Bojongo',
                'ville' => "Douala",
                'telephone' => "699091867",
                'password' => bcrypt('magelong2020'),
            ];
        User::create($user);
    }
}
