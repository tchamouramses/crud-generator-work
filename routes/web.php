<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth','controlfinish']], function () {

	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/admin', 'HomeController@index')->name('home');
	Route::get('/', 'HomeController@index')->name('home');


	Route::group(['prefix'=>'admin'], function(){


		Route::get('/calendar',function(){
			return view('admin.calendar');
		});


		Route::get('/evenmanager',function(){
			return view('admin.event.eventManagement');
		});

		Route::post('/createEvent', 'EventController@store');
		Route::post('/addEvent', 'EventController@create');
		Route::post('/event/delete', 'EventController@delete');

		// route for galerie
		Route::group(['prefix'=>'/galerie'], function(){
			Route::post('/', 'Admin\\GalerieController@store');
			Route::get('/create', 'Admin\\GalerieController@create');
			Route::delete('/{id}', 'Admin\\GalerieController@destroy');
		});

		// route for sites
		Route::group(['prefix'=>'/site'], function(){
			Route::post('/', 'Admin\\SiteController@store');
			Route::get('/create', 'Admin\\SiteController@create');
			Route::get('/{id}/edit', 'Admin\\SiteController@edit');
			Route::patch('/{id}', 'Admin\\SiteController@update');
			Route::delete('/{id}', 'Admin\\SiteController@destroy');
			Route::get('/{id}', 'Admin\\SiteController@show');
			Route::get('/', 'Admin\\SiteController@index');
			Route::get('/{id}/galerie','Admin\\SiteController@galerie');
			Route::delete('/{id}/delete','Admin\\SiteController@deleteProp');
		});


		// route for processus
		Route::group(['prefix'=>'/processus'], function(){
			Route::post('/', 'Admin\\ProcessusController@store');
			Route::get('/create', 'Admin\\ProcessusController@create');
			Route::get('/{id}', 'Admin\\ProcessusController@show');
			Route::get('/', 'Admin\\ProcessusController@index');

			Route::get('/{id}/vente', 'Admin\\ProcessusController@vente');
			Route::post('/vente', 'Admin\\ProcessusController@createvente');
			Route::delete('/{id}/deletevente', 'Admin\\ProcessusController@deletevente');
			Route::post('/{id}/updatevente', 'Admin\\ProcessusController@updatevente');


			Route::get('/{id}/close', 'Admin\\ProcessusController@closeprocess');
			Route::get('/{id}/archive','Admin\\ProcessusController@archive');
			Route::get('/{id}/commission','Admin\\ProcessusController@commission');

			Route::group(['middleware' => ['actionFilter']], function () {
				Route::get('/{id}/derogation','Admin\\ProcessusController@derogation');
				Route::get('/{id}/visite','Admin\\ProcessusController@visite');
				Route::get('/{id}/demande','Admin\\ProcessusController@demande');
				Route::get('/{id}/dossier','Admin\\ProcessusController@dossier');
			});

				Route::get('/{id}/edit', 'Admin\\ProcessusController@edit');
				Route::patch('/{id}', 'Admin\\ProcessusController@update');
				Route::delete('/{id}', 'Admin\\ProcessusController@destroy');
				Route::post('/demande', 'Admin\\ProcessusController@createdemande');
				Route::post('/derogation', 'Admin\\ProcessusController@createderogation');

				Route::post('/commission', 'Admin\\ProcessusController@createcommission');

				Route::post('/dossier','Admin\\ProcessusController@createdossier')->middleware('proccescontrol');
		
				Route::post('/archive','Admin\\ProcessusController@createarchive');
				Route::delete('/{id}/deleteDem','Admin\\ProcessusController@deleteDem');
				Route::delete('/{id}/deleteGeo','Admin\\ProcessusController@deleteGeo');
				Route::delete('/{id}/deleteFac','Admin\\ProcessusController@deleteFac');
				Route::delete('/{id}/deleteDossier','Admin\\ProcessusController@deletedossier');
				Route::delete('/{id}/deletefacdero','Admin\\ProcessusController@deletefacdero');
				Route::delete('/{id}/deletedero','Admin\\ProcessusController@deletedero');
				Route::delete('/{id}/deletedemande','Admin\\ProcessusController@deletedemande');

			
		});


		// route for commission
		Route::group(['prefix'=>'/commission'], function(){
			Route::post('/', 'Admin\\CommissionController@store');
			Route::get('/create', 'Admin\\CommissionController@create');
			Route::get('/{id}/edit', 'Admin\\CommissionController@edit');
			Route::patch('/{id}', 'Admin\\CommissionController@update');
			Route::delete('/{id}', 'Admin\\CommissionController@destroy');
			Route::get('/{id}', 'Admin\\CommissionController@show');
			Route::get('/', 'Admin\\CommissionController@index');
		});



		// route for layonneur
		Route::group(['prefix'=>'/layonner'], function(){
			Route::group(['middleware' => ['proccescontrol']],function(){
				Route::post('/', 'Admin\\LayonnerController@store');
				Route::patch('/{id}', 'Admin\\LayonnerController@update');
			});
			Route::get('/create', 'Admin\\LayonnerController@create');
			Route::get('/{id}/edit', 'Admin\\LayonnerController@edit');
			Route::delete('/{id}', 'Admin\\LayonnerController@destroy');
			Route::get('/{id}', 'Admin\\LayonnerController@show');
			Route::get('/', 'Admin\\LayonnerController@index');
			Route::delete('/{id}/delete', 'Admin\\LayonnerController@delete');

		});


		// route for layonage
		Route::group(['prefix'=>'/layonnage'], function(){
			Route::post('/', 'Admin\\LayonnageController@store');
			Route::get('/create', 'Admin\\LayonnageController@create');
			Route::get('/{id}/edit', 'Admin\\LayonnageController@edit');
			Route::patch('/{id}', 'Admin\\LayonnageController@update');
			Route::delete('/{id}', 'Admin\\LayonnageController@destroy');
			Route::get('/{id}', 'Admin\\LayonnageController@show');
			Route::get('/', 'Admin\\LayonnageController@index');

		});


		// route for demanrcheur
		Route::group(['prefix'=>'/demarcheur'], function(){
			Route::post('/', 'Admin\\DemarcheurController@store');
			Route::get('/create', 'Admin\\DemarcheurController@create');
			Route::get('/{id}/edit', 'Admin\\DemarcheurController@edit');
			Route::patch('/{id}', 'Admin\\DemarcheurController@update');
			Route::delete('/{id}', 'Admin\\DemarcheurController@destroy');
			Route::get('/{id}', 'Admin\\DemarcheurController@show');
			Route::get('/', 'Admin\\DemarcheurController@index');

		});



		// route for geometre
		Route::group(['prefix'=>'/geometre'], function(){
			Route::post('/', 'Admin\\GeometreController@store');
			Route::get('/create', 'Admin\\GeometreController@create');
			Route::get('/{id}/edit', 'Admin\\GeometreController@edit');
			Route::patch('/{id}', 'Admin\\GeometreController@update');
			Route::delete('/{id}', 'Admin\\GeometreController@destroy');
			Route::get('/{id}', 'Admin\\GeometreController@show');
			Route::get('/', 'Admin\\GeometreController@index');

		});


		// route for facilitateur
		Route::group(['prefix'=>'/facilitateur'], function(){
			Route::post('/', 'Admin\\FacilitateurController@store');
			Route::get('/create', 'Admin\\FacilitateurController@create');
			Route::get('/{id}/edit', 'Admin\\FacilitateurController@edit');
			Route::patch('/{id}', 'Admin\\FacilitateurController@update');
			Route::delete('/{id}', 'Admin\\FacilitateurController@destroy');
			Route::get('/{id}', 'Admin\\FacilitateurController@show');
			Route::get('/', 'Admin\\FacilitateurController@index');

		});



		// route for proprietaire
		Route::group(['prefix'=>'/proprietaire'], function(){
			Route::post('/', 'Admin\\ProprietaireController@store');
			Route::get('/create', 'Admin\\ProprietaireController@create');
			Route::get('/{id}/edit', 'Admin\\ProprietaireController@edit');
			Route::patch('/{id}', 'Admin\\ProprietaireController@update');
			Route::delete('/{id}', 'Admin\\ProprietaireController@destroy');
			Route::get('/{id}', 'Admin\\ProprietaireController@show');
			Route::get('/', 'Admin\\ProprietaireController@index');

		});

		// route for archive
		Route::group(['prefix'=>'/archive'], function(){
			Route::post('/', 'Admin\\ArchiveController@store');
			Route::get('/create', 'Admin\\ArchiveController@create');
			Route::get('/{id}/edit', 'Admin\\ArchiveController@edit');
			Route::patch('/{id}', 'Admin\\ArchiveController@update');
			Route::delete('/{id}', 'Admin\\ArchiveController@destroy');
			Route::get('/{id}', 'Admin\\ArchiveController@show');
			Route::get('/', 'Admin\\ArchiveController@index');

		});


		// route for derogation
		Route::group(['prefix'=>'/derogation'], function(){
			Route::group(['middleware' => ['proccescontrol']],function(){
				Route::post('/', 'Admin\\DerogationController@store');
				Route::patch('/{id}', 'Admin\\DerogationController@update');
			});
			Route::get('/create', 'Admin\\DerogationController@create');
			Route::get('/{id}/edit', 'Admin\\DerogationController@edit');
			Route::delete('/{id}', 'Admin\\DerogationController@destroy');
			Route::get('/{id}', 'Admin\\DerogationController@show');
			Route::get('/', 'Admin\\DerogationController@index');

		});

		// route for demande
		Route::group(['prefix'=>'/demande'], function(){
			Route::group(['middleware' => ['proccescontrol']],function(){
				Route::post('/', 'Admin\\DemandeController@store');
				Route::patch('/{id}', 'Admin\\DemandeController@update');
			});
			Route::get('/create', 'Admin\\DemandeController@create');
			Route::get('/{id}/edit', 'Admin\\DemandeController@edit');
			Route::delete('/{id}', 'Admin\\DemandeController@destroy');
			Route::get('/{id}', 'Admin\\DemandeController@show');
			Route::get('/', 'Admin\\DemandeController@index');

		});


		// route for riverin
		Route::group(['prefix'=>'/riverin'], function(){
			Route::post('/', 'Admin\\RiverinController@store');
			Route::get('/create', 'Admin\\RiverinController@create');
			Route::get('/{id}/edit', 'Admin\\RiverinController@edit');
			Route::patch('/{id}', 'Admin\\RiverinController@update');
			Route::delete('/{id}', 'Admin\\RiverinController@destroy');
			Route::get('/{id}', 'Admin\\RiverinController@show');
			Route::get('/', 'Admin\\RiverinController@index');

		});


		// route for opposition
		Route::group(['prefix'=>'/opposition'], function(){
			// Route::post('/', 'Admin\\OppositionController@store');
			// Route::get('/create', 'Admin\\OppositionController@create');
			Route::get('/{id}/edit', 'Admin\\OppositionController@edit');
			Route::patch('/{id}', 'Admin\\OppositionController@update');
			Route::delete('/{id}', 'Admin\\OppositionController@destroy');
			Route::get('/{id}', 'Admin\\OppositionController@show');
			Route::get('/', 'Admin\\OppositionController@index');

		});


		// route for bornage
		Route::group(['prefix'=>'/bornage'], function(){
			Route::post('/', 'Admin\\BornageController@store');
			Route::get('/create', 'Admin\\BornageController@create');
			Route::get('/{id}/edit', 'Admin\\BornageController@edit');
			Route::patch('/{id}', 'Admin\\BornageController@update');
			Route::delete('/{id}', 'Admin\\BornageController@destroy');
			Route::get('/{id}', 'Admin\\BornageController@show');
			Route::get('/', 'Admin\\BornageController@index');

		});


		// route for membre-commission
		Route::group(['prefix'=>'/membre-comission'], function(){
			Route::post('/', 'Admin\\MembreComissionController@store');
			Route::get('/create', 'Admin\\MembreComissionController@create');
			Route::get('/{id}/edit', 'Admin\\MembreComissionController@edit');
			Route::patch('/{id}', 'Admin\\MembreComissionController@update');
			Route::delete('/{id}', 'Admin\\MembreComissionController@destroy');
			Route::get('/{id}', 'Admin\\MembreComissionController@show');
			Route::get('/', 'Admin\\MembreComissionController@index');

		});


		// route for protocol
		Route::group(['prefix'=>'/protocol'], function(){
			Route::post('/', 'Admin\\ProtocolController@store');
			Route::get('/create', 'Admin\\ProtocolController@create');
			Route::get('/{id}/edit', 'Admin\\ProtocolController@edit');
			Route::patch('/{id}', 'Admin\\ProtocolController@update');
			Route::delete('/{id}', 'Admin\\ProtocolController@destroy');
			Route::get('/{id}', 'Admin\\ProtocolController@show');
			Route::get('/', 'Admin\\ProtocolController@index');

		});


		// route for reception
		Route::group(['prefix'=>'/reception'], function(){
			Route::post('/', 'Admin\\ReceptionController@store');
			Route::get('/create', 'Admin\\ReceptionController@create');
			Route::get('/{id}/edit', 'Admin\\ReceptionController@edit');
			Route::patch('/{id}', 'Admin\\ReceptionController@update');
			Route::delete('/{id}', 'Admin\\ReceptionController@destroy');
			Route::get('/{id}', 'Admin\\ReceptionController@show');
			Route::get('/', 'Admin\\ReceptionController@index');
		});



		// route for user
		Route::post('/user/update', 'Admin\\UserController@update');
		Route::post('/user/password', 'Admin\\UserController@updatepassword');
		Route::get('/user/profile', 'Admin\\UserController@profile');
		Route::group(['middleware' => ['admin']], function(){
			Route::group(['prefix'=>'/user'], function(){
				Route::post('/', 'Admin\\UserController@store');
				Route::get('/create', 'Admin\\UserController@create');
				Route::delete('/{id}', 'Admin\\UserController@destroy');
				Route::get('/', 'Admin\\UserController@index');
			});
		});



		// route for image
		Route::group(['prefix'=>'/image'], function(){
			Route::post('/', 'Admin\\ImageController@store');
			Route::get('/create', 'Admin\\ImageController@create');
			Route::get('/{id}/edit', 'Admin\\ImageController@edit');
			Route::patch('/{id}', 'Admin\\ImageController@update');
			Route::delete('/{id}', 'Admin\\ImageController@destroy');
			Route::get('/{id}', 'Admin\\ImageController@show');
			Route::get('/', 'Admin\\ImageController@index');
		});


		// route for visite
		Route::group(['prefix'=>'/visite'], function(){
			Route::post('/', 'Admin\\VisiteController@store');
			Route::get('/create', 'Admin\\VisiteController@create');
			Route::get('/{id}/edit', 'Admin\\VisiteController@edit');
			Route::patch('/{id}', 'Admin\\VisiteController@update');
			Route::delete('/{id}', 'Admin\\VisiteController@destroy');
			Route::get('/{id}', 'Admin\\VisiteController@show');
			Route::get('/', 'Admin\\VisiteController@index');
		});



		// route for descente
		Route::group(['prefix'=>'/descente'], function(){
			Route::post('/', 'Admin\\DescenteController@store');
			Route::get('/create', 'Admin\\DescenteController@create');
			Route::get('/{id}/edit', 'Admin\\DescenteController@edit');
			Route::patch('/{id}', 'Admin\\DescenteController@update');
			Route::delete('/{id}', 'Admin\\DescenteController@destroy');
			Route::get('/{id}', 'Admin\\DescenteController@show');
			Route::get('/', 'Admin\\DescenteController@index');


			Route::post('/opposition', 'Admin\\DescenteController@createopposition');
			Route::post('/riverin', 'Admin\\DescenteController@createparticipant');
			Route::post('/reception', 'Admin\\DescenteController@createreception');

			Route::delete('/{id}/deleteMembre', 'Admin\\DescenteController@deleteMembre');
			Route::delete('/{id}/deleteFacDesc', 'Admin\\DescenteController@deleteFacDesc');
			Route::delete('/{id}/deletePropDesc', 'Admin\\DescenteController@deletePropDesc');
			Route::post('/admin', 'Admin\\DescenteController@admin');
			Route::get('/{id}/reception', 'Admin\\DescenteController@reception');
			Route::get('/{id}/opposition', 'Admin\\DescenteController@opposition');
			Route::get('/{id}/administration', 'Admin\\DescenteController@administration');
			Route::get('/{id}/participant', 'Admin\\DescenteController@participant');
		});





		// route for notification
		Route::group(['prefix'=>'/notification'], function(){
					
			Route::get('/demande',function(){
				return view('admin.notification.demande');
			});

			Route::get('/visite',function(){
				return view('admin.notification.visites');
			});

			Route::get('/descente',function(){
				return view('admin.notification.descente');
			});

			Route::get('/protocols',function(){
				return view('admin.notification.protocol');
			});

			Route::get('/derogations',function(){
				return view('admin.notification.derogation');
			});

			Route::get('/bornages',function(){
				return view('admin.notification.bornage');
			});


			Route::get('/receptions',function(){
				return view('admin.notification.receptions');
			});
		});

	});


	
});