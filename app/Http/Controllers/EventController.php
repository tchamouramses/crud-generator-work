<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use Illuminate\Support\Carbon;

class EventController extends Controller
{
    
    public function store(Request $request)
    {
    	$this->validate($request, [
			'value' => 'required'
		]);


    	foreach ($request->value as $item) {
    		if(isset($item['id'])){
                $item['allDay'] = 1;
    			$event = Event::findOrFail($item['id']);
    			$id = $item['start'];
    			$event->update($item);
    		}else{
    			$item['allDay'] = 1;
	    		Event::create($item);
    		}
    	}
		return response()->json(['status'=>'Mise a jour effectué avec succes!!!']);
    }



    public function delete(Request $request)
    {
        for ($i=0; $i < count($request->value); $i++) { 
            # code...
            Event::destroy($request->value[$i]);
        }
        return response()->json(['status'=>'Suppréssion effectué avec succes!!!']);
    }

    public function create(Request $request)
    {
        # code...
        $this->validate($request, [
            'title' => 'required',
            'start' => 'required'
        ]);

        $requestData = $request->all();
        Event::create($requestData);
        return response()->json(['status'=>'Evenement Crée avec succes!!!']);
    }
}
