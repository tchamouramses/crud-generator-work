<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Visite;
use App\Processu;
use App\Archive;
use Illuminate\Http\Request;

class VisiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $visite = Visite::where('Date_Visite', 'LIKE', "%$keyword%")
                ->orWhere('frais', 'LIKE', "%$keyword%")
                ->orWhere('rapport', 'LIKE', "%$keyword%")
                ->orWhere('surface', 'LIKE', "%$keyword%")
                ->orWhere('Nature_Du_Sol', 'LIKE', "%$keyword%")
                ->orWhere('Nature_Relief', 'LIKE', "%$keyword%")
                ->orWhere('observation', 'LIKE', "%$keyword%")
                ->orWhere('processus', 'LIKE', "%$keyword%")
                ->orWhere('Effectuer', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $visite = Visite::latest()->paginate($perPage);
        }


        foreach ($visite as $item) {
            # code...
            $process = Processu::find($item->processus);
            $item->Process = $process;
        }

        $ariane = ['visite'];
        return view('admin.visite.index', compact('visite','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $processus = Processu::whereIsFinish(0)->get();
        $ariane = ['visite','ajout'];
        return view('admin.visite.create',compact('processus','ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'Date_Visite' => 'required',
            'processus' => 'required|exists:processuses,id',
            'surface' => 'required'
		]);

        $requestData = $request->all();

        if ($request->hasFile('rapport')) {
            $requestData['rapport'] = $request->file('rapport')
            ->store('uploads', 'public');
        }

        $vis = Visite::create($requestData);

        if ($request->hasFile('rapport')){
            $archive = new Archive();
            $archive->fichier = $vis->rapport;
            $archive->Nom_Fichier = 'Rapport de la visite du: '.$vis->created_at;
            $archive->processus = $vis->processus;
            $archive->save();
        }
        return redirect('admin/visite')->with('flash_message', 'Visite Ajouter Avec Succes!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $visite = Visite::findOrFail($id);
        $process = Processu::find($visite->processus);
        $visite->Process = $process;

        $ariane = ['visite','details'];
        return view('admin.visite.show', compact('visite','ariane'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $visite = Visite::findOrFail($id);
        $processus = Processu::whereIsFinish(0)->get();

        $ariane = ['visite','Modification'];
        return view('admin.visite.edit', compact('visite','processus','ariane'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'Date_Visite' => 'required',
			'processus' => 'required|exists:processuses,id'
		]);
        $requestData = $request->all();
                if ($request->hasFile('rapport')) {
            $requestData['rapport'] = $request->file('rapport')
                ->store('uploads', 'public');
        }

        $visite = Visite::findOrFail($id);
        $visite->update($requestData);

        return redirect('admin/visite')->with('flash_message', 'Visite Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $visite = Visite::findOrFail($id);

        $processus = Processu::findOrFail($visite->processus);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            Visite::destroy($id);
            return response()->json(['status'=>'Visite Supprimer Avec Succes','type'=>'success']);
        }
    }
}
