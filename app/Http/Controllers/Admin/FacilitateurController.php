<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Facilitateur;
use Illuminate\Http\Request;
use App\Event;

class FacilitateurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $facilitateur = Facilitateur::where('noms_Facilitateur', 'LIKE', "%$keyword%")
                ->orWhere('telephone__Facilitateur1', 'LIKE', "%$keyword%")
                ->orWhere('telephone__Facilitateur2', 'LIKE', "%$keyword%")
                ->orWhere('email_Facilitateur', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $facilitateur = Facilitateur::latest()->paginate($perPage);
        }

        $ariane = ['facilitateur'];
        return view('admin.facilitateur.index', compact('facilitateur','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $ariane = ['facilitateur','ajout'];
        return view('admin.facilitateur.create',compact('ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $this->validate($request, [
			'noms_Facilitateur' => 'required',
            'telephone_Facilitateur1' => 'required'
		]);

        $requestData = $request->all();
        Facilitateur::create($requestData);

        return redirect('admin/facilitateur')->with('flash_message', 'Facilitateur Ajouter Avec Succes!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $facilitateur = Facilitateur::findOrFail($id);

        $ariane = ['facilitateur','details'];
        return view('admin.facilitateur.show', compact('facilitateur','ariane'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $facilitateur = Facilitateur::findOrFail($id);
        $ariane = ['facilitateur','Modification'];
        return view('admin.facilitateur.edit', compact('facilitateur','ariane'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'noms_Facilitateur' => 'required',
			'telephone_Facilitateur1' => 'required'
		]);
        $requestData = $request->all();
        
        $facilitateur = Facilitateur::findOrFail($id);
        $facilitateur->update($requestData);

        return redirect('admin/facilitateur')->with('flash_message', 'Facilitateur Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Facilitateur::destroy($id);

        return response()->json(['status'=>'Facilitateur Supprimer Avec Succes','type'=>'success']);
    }
}
