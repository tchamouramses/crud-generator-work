<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Geometre;
use Illuminate\Http\Request;

class GeometreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $geometre = Geometre::where('noms', 'LIKE', "%$keyword%")
                ->orWhere('telephone_geo1', 'LIKE', "%$keyword%")
                ->orWhere('telephone_geo2', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $geometre = Geometre::latest()->paginate($perPage);
        }

        $ariane = ['geometre'];
        return view('admin.geometre.index', compact('geometre','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $ariane = ['geometre','ajout'];
        return view('admin.geometre.create',compact('ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'noms' => 'required',
			'telephone_geo1' => 'required'
		]);
        $requestData = $request->all();
        
        Geometre::create($requestData);

        return redirect('admin/geometre')->with('flash_message', 'Geometre Ajouter Avec Succes!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $geometre = Geometre::findOrFail($id);

        $ariane = ['geometre','details'];
        return view('admin.geometre.show', compact('geometre','ariane'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $geometre = Geometre::findOrFail($id);

        $ariane = ['geometre','Modification'];
        return view('admin.geometre.edit', compact('geometre','ariane'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'noms' => 'required',
			'telephone_geo1' => 'required'
		]);
        $requestData = $request->all();
        
        $geometre = Geometre::findOrFail($id);
        $geometre->update($requestData);

        return redirect('admin/geometre')->with('flash_message', 'Geometre Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Geometre::destroy($id);

        return response()->json(['status'=>'Geometre Supprimer Avec Succes','type'=>'success']);
    }
    
}
