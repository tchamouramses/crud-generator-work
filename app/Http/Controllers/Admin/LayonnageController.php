<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Layonnage;
use Illuminate\Http\Request;

class LayonnageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $layonnage = Layonnage::where('noms_Layonneur', 'LIKE', "%$keyword%")
                ->orWhere('telephone__Layonneur1', 'LIKE', "%$keyword%")
                ->orWhere('telephone__Layonneur2', 'LIKE', "%$keyword%")
                ->orWhere('email_Layonneur', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $layonnage = Layonnage::latest()->paginate($perPage);
        }

        $ariane = ['layonnage'];
        return view('admin.layonnage.index', compact('layonnage','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $ariane = ['layonnage','ajout'];
        return view('admin.layonnage.create',compact('ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'noms_Layonneur' => 'required',
			'telephone__Layonneur1' => 'required'
		]);
        $requestData = $request->all();
        
        Layonnage::create($requestData);

        return redirect('admin/layonnage')->with('flash_message', 'Layonneur Ajouter Avec Succes!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $layonnage = Layonnage::findOrFail($id);

        $ariane = ['layonnage','details'];
        return view('admin.layonnage.show', compact('layonnage','ariane'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $layonnage = Layonnage::findOrFail($id);

        $ariane = ['layonnage','Modification'];
        return view('admin.layonnage.edit', compact('layonnage','ariane'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'noms_Layonneur' => 'required',
			'telephone__Layonneur1' => 'required'
		]);
        $requestData = $request->all();
        
        $layonnage = Layonnage::findOrFail($id);
        $layonnage->update($requestData);

        return redirect('admin/layonnage')->with('flash_message', 'Layonneur Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Layonnage::destroy($id);

        return response()->json(['status'=>'Layonneur Supprimer Avec Succes','type'=>'success']);
    }
}
