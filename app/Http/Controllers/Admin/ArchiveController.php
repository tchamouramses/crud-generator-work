<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Archive;
use App\Processu;
use Illuminate\Http\Request;

class ArchiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $archive = Archive::where('Nom_Fichier', 'LIKE', "%$keyword%")
                ->orWhere('fichier', 'LIKE', "%$keyword%")
                ->orWhere('processus', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $archive = Archive::latest()->paginate($perPage);
        }

        foreach ($archive as $item) {
            # code...
            $process = Processu::find($item->processus);
            $item->process = $process;
        }
        $ariane = ['archive'];
        return view('admin.archive.index', compact('archive','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $processus = Processu::whereIsFinish(0)->get();
        $ariane = ['archive','Ajout'];
        return view('admin.archive.create', compact('processus','ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'processus' => 'required|exists:processuses,id',
			'fichier' => 'required'
		]);

        $requestData = $request->all();
        $val = 0;
        if(!isset($requestData['Nom_Fichier']))
            $val = 1;

        if(!empty($request->fichier)){
            for ($i=0; $i < count($request->fichier); $i++) { 
                if($val == 1)
                    $requestData['Nom_Fichier'] = $request->file('fichier')[$i]->getClientOriginalName();
                $requestData['fichier'] = $request->file('fichier')[$i]
                    ->store('uploads', 'public');
                Archive::create($requestData);
            }
        }

        return redirect('/admin/archive')->with('flash_message', 'Archive Ajouter Avec Succes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $archive = Archive::findOrFail($id);
            $process = Processu::find($archive->processus);
            $archive->process = $process;
        $ariane = ['archive','details'];
        return view('admin.archive.show', compact('archive','ariane'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $archive = Archive::findOrFail($id);
        $processus = Processu::whereIsFinish(0)->get();
        $ariane = ['archive','Modification'];
        return view('admin.archive.edit', compact('archive','processus','ariane'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'processus' => 'required|exists:processuses,id',
			'fichier' => 'required'
		]);
        $requestData = $request->all();
                if ($request->hasFile('fichier')) {
            $requestData['fichier'] = $request->file('fichier')
                ->store('uploads', 'public');
        }

        $archive = Archive::findOrFail($id);
        $archive->update($requestData);

        return redirect('/admin/archive')->with('flash_message', 'Archive Modifié Avec Succes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $archive = Archive::findOrFail($id);
        $processus = Processu::findOrFail($archive->processus);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            Archive::destroy($id);
            return response()->json(['status'=>'Document Supprimer Avec Succes','type'=>'success']);
        }
        
    }
}
