<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Demarcheur;
use Illuminate\Http\Request;

class DemarcheurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $demarcheur = Demarcheur::where('noms', 'LIKE', "%$keyword%")
                ->orWhere('telephone1', 'LIKE', "%$keyword%")
                ->orWhere('telephone2', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $demarcheur = Demarcheur::latest()->paginate($perPage);
        }

        $ariane = ['demarcheur'];
        return view('admin.demarcheur.index', compact('demarcheur','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $ariane = ['demarcheur','ajout'];
        return view('admin.demarcheur.create',compact('ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'noms' => 'required',
			'telephone1' => 'required'
		]);
        $requestData = $request->all();
        Demarcheur::create($requestData);
        return redirect('admin/demarcheur')->with('flash_message', 'Ajout du demarcheur effectué avec Succes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $demarcheur = Demarcheur::findOrFail($id);

        $ariane = ['demarcheur','details'];
        return view('admin.demarcheur.show', compact('demarcheur','ariane'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $demarcheur = Demarcheur::findOrFail($id);

        $ariane = ['demarcheur','Modification'];
        return view('admin.demarcheur.edit', compact('demarcheur','ariane'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'noms' => 'required',
			'telephone1' => 'required'
		]);
        $requestData = $request->all();
        
        $demarcheur = Demarcheur::findOrFail($id);
        $demarcheur->update($requestData);

        return redirect('admin/demarcheur')->with('flash_message', 'Demarcheur Modifié avec Succes!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Demarcheur::destroy($id);

        return response()->json(['status'=> 'Suppression effectue avec succes','type'=>'success']);
    }
}
