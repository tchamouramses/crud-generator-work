<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Opposition;
use Illuminate\Http\Request;
use App\Descente;
use App\Processu;

class OppositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $opposition = Opposition::where('noms', 'LIKE', "%$keyword%")
                ->orWhere('motif', 'LIKE', "%$keyword%")
                ->orWhere('reglement', 'LIKE', "%$keyword%")
                ->orWhere('Date_Intitule', 'LIKE', "%$keyword%")
                ->orWhere('descente', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $opposition = Opposition::latest()->paginate($perPage);
        }
        $ariane = ['opposition'];
        return view('admin.opposition.index', compact('opposition','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $descente = Descente::all();
        $ariane = ['opposition','Ajouter'];
        return view('admin.opposition.create',compact('ariane','descente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'noms' => 'required',
			'motif' => 'required',
            'Date_Intitule' => 'required|after:yesterday',
            'descente' => 'required|exists:descentes,id'
		]);

        $requestData = $request->all();
        $res = Opposition::create($requestData);
        return redirect('admin/descente/'.$res->descente.'/opposition')->with('flash_message', 'Opposition Ajouter Avec Succes!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $opposition = Opposition::findOrFail($id);
        // $descente = Descente::findOrFail($opposition->descente);
        $descente = Descente::select('descentes.*','processuses.nom_processus as proces')
                                ->join('processuses','processuses.id','=','descentes.processus')
                                ->where('descentes.id','=',$opposition->descente)
                                ->first();
        $ariane = ['descente','descente/'.$descente->id,'Details Opposition'];
        return view('admin.opposition.show', compact('opposition','ariane','descente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $opposition = Opposition::findOrFail($id);
        $ariane = ['descente','descente/'.$opposition->descente,'Modification opposition'];
        $descente = Descente::all();

        return view('admin.opposition.edit', compact('opposition','ariane','descente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'noms' => 'required',
			'motif' => 'required',
			'Date_Intitule' => 'required|after:yesterday'
		]);
        $requestData = $request->all();
        
        $opposition = Opposition::findOrFail($id);
        $opposition->update($requestData);

        return redirect('admin/descente/'.$opposition->descente.'/opposition')->with('flash_message', 'Opposition Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Opposition::destroy($id);

        return response()->json(['status'=>'Opposition Supprimer Avec Succes','type'=>'success']);
    }


}
