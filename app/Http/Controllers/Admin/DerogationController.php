<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Derogation;
use App\Processu;
use App\FacDerogation;
use App\Facilitateur;
use Illuminate\Http\Request;

class DerogationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $derogation = Derogation::where('date_derogation', 'LIKE', "%$keyword%")
                ->orWhere('frais', 'LIKE', "%$keyword%")
                ->orWhere('processus', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $derogation = Derogation::latest()->paginate($perPage);
        }

        foreach ($derogation as $item) {
            # code...
            $process = Processu::find($item->processus);
            $item->Process = $process;
        }

        $ariane = ['derogation'];
        return view('admin.derogation.index', compact('derogation','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $proces = Processu::select('processuses.*')
                        ->join('derogations','derogations.processus','!=','processuses.id')
                        ->where('processuses.nom','=','Immatriculation')
                        ->get();

        if(count($proces) == 0){
            $proces = Processu::whereIsFinish(0)->get();
        }
        $facDero = [new FacDerogation];
        $facilitateur = Facilitateur::all();
        $ariane = ['derogation','ajout'];
        return view('admin.derogation.create',compact('proces','facDero','facilitateur','ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'date_derogation' => 'required',
			'frais' => 'numeric|min:0',
			'processus' => 'required|exists:processuses,id'
		]);
        $requestData = $request->all();

        $facilitateur = $request->facilitateur;
        $dero = Derogation::create($requestData);

        if(!empty($facilitateur)){
        for ($i=0; $i < count($facilitateur); $i++) {
            $geo = new FacDerogation();
            $geo->derogation = $dero->id;
            $geo->facilitateur = $facilitateur[$i];
            $geo->save();
         } 
        }

        return redirect('admin/derogation')->with('flash_message', 'Derogation Ajouter Avec Succes!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $derogation = Derogation::findOrFail($id);
        $process = Processu::find($derogation->processus);
        $derogation->Process = $process;

        $ariane = ['derogation','details'];
        return view('admin.derogation.show', compact('derogation','ariane'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $derogation = Derogation::findOrFail($id);
        $facDero = FacDerogation::whereDerogation($id)->get();
        $facilitateur = Facilitateur::all();
        $processus = Processu::findOrFail($derogation->processus);
        $proces = Processu::whereIsFinish(0)->get();
        $ariane = ['derogation','Modification'];
        return view('admin.derogation.edit', compact('derogation','processus','facDero','facilitateur','ariane','proces'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'date_derogation' => 'required',
			'frais' => 'numeric|min:0',
			'processus' => 'required|exists:processuses,id'
		]);
        $requestData = $request->all();
        $facilitateur = $request->facilitateur;
        
        $derogation = Derogation::findOrFail($id);
        $derogation->update($requestData);

        if(!empty($facilitateur)){
        for ($i=0; $i < count($facilitateur); $i++) {
            $geo = new FacDerogation();
            $geo->derogation = $derogation->id;
            $geo->facilitateur = $facilitateur[$i];
            $geo->save();
         } 
        }


        return redirect('admin/derogation')->with('flash_message', 'Derogation Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $derogation = Derogation::findOrFail($id);

        $processus = Processu::findOrFail($derogation->processus);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            Derogation::destroy($id);
            return response()->json(['status'=>'Derogation Supprimer Avec Succes','type'=>'success']);
        }
    }


}
