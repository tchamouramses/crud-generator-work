<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Reception;
use Illuminate\Http\Request;
use App\Descente;
use App\Processu;

class ReceptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $reception = Reception::where('date_de_reception', 'LIKE', "%$keyword%")
                ->orWhere('frais_de_reception', 'LIKE', "%$keyword%")
                ->orWhere('descente', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $reception = Reception::latest()->paginate($perPage);
        }

        foreach ($reception as $item) {
            # code...
            $proc = Descente::select('processuses.*')
                    ->join('processuses','processuses.id','=','descentes.id')
                    ->where('descentes.id','=',$item->descente)
                    ->first();
            $item->proc = $proc;
        }

        $ariane = ['reception'];
        return view('admin.reception.index', compact('reception','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $descente = Descente::select('descentes.id','processuses.nom_processus')
                    ->join('processuses','processuses.id','=','descentes.processus')
                    ->get();
        $ariane = ['reception','Ajouter'];
        return view('admin.reception.create',compact('ariane','descente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'date_de_reception' => 'required',
            'frais_de_reception' => 'numeric|min:0',
            'descente' => 'required|exists:descentes,id'
		]);
        $requestData = $request->all();
        
        $res = Reception::create($requestData);

        return redirect('admin/reception')->with('flash_message', 'Reception Ajouter Avec Succes!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $reception = Reception::findOrFail($id);
        $ariane = ['reception','Details'];
        $descente = Descente::select('descentes.*','processuses.nom_processus')
                    ->join('processuses','processuses.id','=','descentes.id')
                    ->where('descentes.id','=',$reception->descente)
                    ->first();
        return view('admin.reception.show', compact('reception','ariane','descente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $reception = Reception::findOrFail($id);
        $ariane = ['reception','Modification'];
        $descente = Descente::select('descentes.id','processuses.nom_processus')
                    ->join('processuses','processuses.id','=','descentes.processus')
                    ->get();

        return view('admin.reception.edit', compact('reception','ariane','descente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'date_de_reception' => 'required',
			'frais_de_reception' => 'numeric|min:0',
            'descente' => 'required|exists:descentes,id'
		]);
        $requestData = $request->all();
        
        $reception = Reception::findOrFail($id);
        $reception->update($requestData);

        return redirect('admin/reception')->with('flash_message', 'Reception Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Reception::destroy($id);

        return response()->json(['status'=>'Reception Supprimer Avec Succes','type'=>'success']);
    }


}
