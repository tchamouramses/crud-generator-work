<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Descente;
use Illuminate\Http\Request;
use App\Processu;
use App\Facilitateur;
use App\Proprietaire;
use App\FacDescente;
use App\PropDescente;
use App\Reception;
use App\Opposition;
use App\Riverin;
use App\MembreComission;
use App\DescenteMembreCommission;

class DescenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $descente = Descente::where('date_descente', 'LIKE', "%$keyword%")
                ->orWhere('Frais_Descente', 'LIKE', "%$keyword%")
                ->orWhere('Chef_De_Quartier', 'LIKE', "%$keyword%")
                ->orWhere('processus', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $descente = Descente::latest()->paginate($perPage);
        }

        foreach ($descente as $item) {
            # code...
            $proc = Processu::find($item->processus);
            $item->proc = $proc;
        }

        $ariane = ['descente'];
        return view('admin.descente.index', compact('descente','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $processus = Processu::whereIsFinish(0)->get();
        $facilitateur = Facilitateur::all();
        $proprietaire = Proprietaire::all();
        $ariane = ['descente','Ajouter'];
        return view('admin.descente.create',compact('ariane','processus','proprietaire','facilitateur'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'date_descente' => 'required',
			'Frais_Descente' => 'numeric|min:0',
            'processus' => 'required|exists:processuses,id',
            'facilitateur' => 'required',
            'proprietaire' => 'required'
		]);
        $requestData = $request->all();
        
        $descente = Descente::create($requestData);

        $facilitateur = $request->facilitateur;
        $proprietaire = $request->proprietaire;

        if(!empty($facilitateur)){
        for ($i=0; $i < count($facilitateur); $i++) {
            $fac = new FacDescente();
            $fac->descente = $descente->id;
            $fac->facilitateur = $facilitateur[$i];
            $fac->save();
         } 
        }
        if(!empty($proprietaire)){
           for ($i=0; $i < count($proprietaire); $i++) {
                $prop = new PropDescente();
                $prop->descente = $descente->id;
                $prop->proprietaire = $proprietaire[$i];
                $prop->save();
            }  
        }

        return redirect('admin/descente')->with('flash_message', 'Descente Ajouter Avec Succes!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $descente = Descente::findOrFail($id);
        $ariane = ['descente','Details'];
        $processus = Processu::findOrFail($descente->processus);
        $proprietaire = PropDescente::select('prop_descentes.*','proprietaires.noms','proprietaires.telephone')
                        ->join('proprietaires','proprietaires.id','=','prop_descentes.proprietaire')
                        ->where('prop_descentes.descente','=',$id)
                        ->get();


        $facilitateur = FacDescente::select('fac_descentes.*','facilitateurs.noms_Facilitateur as noms','facilitateurs.telephone_Facilitateur1 as telephone')
                        ->join('facilitateurs','facilitateurs.id','=','fac_descentes.facilitateur')
                        ->where('fac_descentes.descente','=',$id)
                        ->get();
        return view('admin.descente.show', compact('descente','ariane','processus','proprietaire','facilitateur'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $descente = Descente::findOrFail($id);
        $ariane = ['descente','Modification'];
        $processus = Processu::whereIsFinish(0)->get();
        $facilitateur = Facilitateur::all();
        $proprietaire = Proprietaire::all();

        return view('admin.descente.edit', compact('descente','ariane','processus','proprietaire','facilitateur'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'date_descente' => 'required',
			'Frais_Descente' => 'numeric|min:0',
			'processus' => 'required|exists:processuses,id'
		]);
        $requestData = $request->all();
        
        $descente = Descente::findOrFail($id);
        $descente->update($requestData);

        $facilitateur = $request->facilitateur;
        $proprietaire = $request->proprietaire;

        if(!empty($facilitateur)){
        for ($i=0; $i < count($facilitateur); $i++) {
            $fac = new FacDescente();
            $fac->descente = $descente->id;
            $fac->facilitateur = $facilitateur[$i];
            $fac->save();
         } 
        }
        if(!empty($proprietaire)){
           for ($i=0; $i < count($proprietaire); $i++) {
                $prop = new PropDescente();
                $prop->descente = $descente->id;
                $prop->proprietaire = $proprietaire[$i];
                $prop->save();
            }  
        }

        return redirect('admin/descente')->with('flash_message', 'Descente Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $descente = Descente::findOrFail($id);

        $processus = Processu::findOrFail($descente->processus);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            Descente::destroy($id);
            return response()->json(['status'=>'Descente Supprimer Avec Succes','type'=>'success']);
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function reception(Request $request,$id)
    {
        $descente = Descente::select('descentes.*','processuses.nom_processus')
                    ->join('processuses','processuses.id','=','descentes.processus')
                    ->where('descentes.id','=',$id)
                    ->first();

        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $reception = Reception::where('date_de_reception', 'LIKE', "%$keyword%")
                ->orWhere('frais_de_reception', 'LIKE', "%$keyword%")
                ->orWhere('descente', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $reception = Reception::latest()->paginate($perPage);
        }
        foreach ($reception as $item) {
            # code...
            $proc = Processu::find($descente->processus);
            $item->proc = $proc;
        }

        $ariane = ['descente','descente/'.$id,'reception'];
        return view('admin.descente.reception', compact('reception','ariane','descente'));
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function opposition(Request $request,$id)
    {
        $descente = Descente::select('descentes.*','processuses.nom_processus')
                    ->join('processuses','processuses.id','=','descentes.processus')
                    ->where('descentes.id','=',$id)
                    ->first();

        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $opposition = Opposition::where('noms', 'LIKE', "%$keyword%")
                ->orWhere('motif', 'LIKE', "%$keyword%")
                ->orWhere('reglement', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $opposition = Opposition::latest()->paginate($perPage);
        }

        foreach ($opposition as $item) {
            # code...
            $proc = Processu::find($descente->processus);
            $item->proc = $proc;
        }
        $ariane = ['descente','descente/'.$id,'opposition'];
        return view('admin.descente.opposition', compact('opposition','ariane','descente'));
    }

    


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function participant(Request $request,$id)
    {
        $descente = Descente::select('descentes.*','processuses.nom_processus')
                    ->join('processuses','processuses.id','=','descentes.processus')
                    ->where('descentes.id','=',$id)
                    ->first();

        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $participant = Riverin::where('noms', 'LIKE', "%$keyword%")
                ->orWhere('telephone', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $participant = Riverin::latest()->paginate($perPage);
        }

        foreach ($participant as $item) {
            # code...
            $proc = Processu::find($descente->processus);
            $item->proc = $proc;
        }
        $ariane = ['descente','descente/'.$id,'participant'];
        return view('admin.descente.participant', compact('participant','ariane','descente'));
    }


     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function administration($id)
    {
        $descente = Descente::select('descentes.*','processuses.nom_processus')
                    ->join('processuses','processuses.id','=','descentes.processus')
                    ->where('descentes.id','=',$id)
                    ->first();
                    
        $membrecomission = DescenteMembreCommission::select('descente_membre_commissions.id as com','membre_comissions.*','descente_membre_commissions.frais')
                    ->join('membre_comissions','membre_comissions.id','=','descente_membre_commissions.membre')
                    ->where('descente_membre_commissions.descente','=',$id)
                    ->get();

        $admin = MembreComission::whereStatus('DESCENTE')  
                ->orWhere('status','=','DEUX')
                ->get();

        $ariane = ['descente','descente/'.$id,'Membre Commission'];
        return view('admin.descente.administration', compact('ariane','descente','membrecomission','admin'));
    }


       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function admin(Request $request)
    {
        $this->validate($request, [
            'frais' => 'required',
            'descente' => 'required|exists:descentes,id',
            'membre' => 'required|exists:membre_comissions,id'
        ]);
        $requestData = $request->all();
        $res = DescenteMembreCommission::create($requestData);

        return redirect('admin/descente/'.$res->descente.'/administration');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteMembre($id)
    {
        DescenteMembreCommission::destroy($id);

       return response()->json(['status'=>'Frais Supprimer Avec Succes','type'=>'success']);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteFacDesc($id)
    {
        FacDescente::destroy($id);

       return response()->json(['status'=>'Facilitateur retiré Avec Succes','type'=>'success']);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deletePropDesc($id)
    {
        PropDescente::destroy($id);

       return response()->json(['status'=>'Proprietaire retiré Avec Succes','type'=>'success']);
    }



    public function createreception(Request $request)
    {
        # code...
        $this->validate($request, [
            'date_de_reception' => 'required',
            'frais_de_reception' => 'required',
            'descente' => 'required|exists:descentes,id'
        ]);
        $requestData = $request->all();
        
        $res = Reception::create($requestData);

        return redirect('admin/descente/'.$res->descente.'/reception')->with('flash_message', 'Reception Ajouter Avec Succes!');
    }


    public function createparticipant(Request $request)
    {
        # code...
        $this->validate($request, [
            'noms' => 'required',
            'descente' => 'required|exists:descentes,id',
            'status' => 'required|in:Riverin,Notable'
        ]);
        $requestData = $request->all();
        
        $riverin = Riverin::create($requestData);

        return redirect('admin/descente/'.$riverin->descente.'/participant')->with('flash_message', 'Participant updated!');
    }


    public function createopposition(Request $request)
    {
        # code...
        $this->validate($request, [
            'noms' => 'required',
            'motif' => 'required',
            'Date_Intitule' => 'required',
            'descente' => 'required|exists:descentes,id'
        ]);

        $requestData = $request->all();
        $res = Opposition::create($requestData);
        return redirect('admin/descente/'.$res->descente.'/opposition')->with('flash_message', 'Opposition added!');
    }
}
