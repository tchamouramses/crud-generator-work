<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $image = Image::where('fichier', 'LIKE', "%$keyword%")
                ->orWhere('visite', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $image = Image::latest()->paginate($perPage);
        }

        $ariane = ['image'];
        return view('admin.image.index', compact('image','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $ariane = ['image','ajout'];
        return view('admin.image.create',compact('ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'fichier' => 'required',
			'visite' => 'required'
		]);
        $requestData = $request->all();
                if ($request->hasFile('fichier')) {
            $requestData['fichier'] = $request->file('fichier')
                ->store('uploads', 'public');
        }

        Image::create($requestData);

        return redirect('admin/image')->with('flash_message', 'Image Ajouter Avec Succes!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $image = Image::findOrFail($id);

        $ariane = ['image','details'];
        return view('admin.image.show', compact('image','ariane'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $image = Image::findOrFail($id);

        $ariane = ['image','Modification'];
        return view('admin.image.edit', compact('image','ariane'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'fichier' => 'required',
			'visite' => 'required'
		]);
        $requestData = $request->all();
                if ($request->hasFile('fichier')) {
            $requestData['fichier'] = $request->file('fichier')
                ->store('uploads', 'public');
        }

        $image = Image::findOrFail($id);
        $image->update($requestData);

        return redirect('admin/image')->with('flash_message', 'Image Modifié Avec Succes!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Image::destroy($id);

        return redirect('admin/image')->with('flash_message', 'Image A!');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function galerie($id)
    {
        $image = Image::whereIdVisite($id);

        $ariane = ['image','galerie'];
        return view('admin.image.galerie', compact('image','ariane'));
    }
}
