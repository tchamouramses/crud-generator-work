<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Commission;
use App\Processu;
use Illuminate\Http\Request;

class CommissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $commission = Commission::where('Commission_Facilitateurs', 'LIKE', "%$keyword%")
                ->orWhere('Commission_Demarcheur', 'LIKE', "%$keyword%")
                ->orWhere('Autres_Commissions', 'LIKE', "%$keyword%")
                ->orWhere('processus', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $commission = Commission::latest()->paginate($perPage);
        }
        foreach ($commission as $item) {
            # code...
            $process = Processu::find($item->processus);
            $item->process = $process;
        }

        $ariane = ['commission'];
        return view('admin.commission.index', compact('commission','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $processus = Processu::whereIsFinish(0)->get();
        $ariane = ['commission','Ajout'];
        return view('admin.commission.create', compact('processus','ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'processus' => 'required|exists:processuses,id'
		]);
        $requestData = $request->all();
        $id = $request->processus;
        Commission::create($requestData);

        return redirect('admin/commission')->with('flash_message', 'Commission Ajouter Avec Succes!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $commission = Commission::findOrFail($id);
        $ariane = ['commission','details'];
        $processus = Processu::findOrFail($commission->processus);
        return view('admin.commission.show', compact('commission','ariane','processus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $commission = Commission::findOrFail($id);
        $processus = Processu::whereIsFinish(0)->get();
        $ariane = ['commission','Modification'];
        return view('admin.commission.edit', compact('commission','processus','ariane'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'processus' => 'required|exists:processuses,id'
		]);
        $requestData = $request->all();
        
        $commission = Commission::findOrFail($id);
        $commission->update($requestData);

        return redirect('admin/commission')->with('flash_message', 'Commission Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $commission = Commission::findOrFail($id);

        $processus = Processu::findOrFail($commission->processus);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            Commission::destroy($id);
            return response()->json(['status'=>'Commission Supprimer Avec Succes','type'=>'success']);
        }
    }
}
