<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Riverin;
use Illuminate\Http\Request;
use App\Descente;

class RiverinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $riverin = Riverin::where('noms', 'LIKE', "%$keyword%")
                ->orWhere('telephone', 'LIKE', "%$keyword%")
                ->orWhere('descente', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $riverin = Riverin::latest()->paginate($perPage);
        }


        foreach ($riverin as $item) {
            # code...
            $desc = Descente::select('descentes.*','processuses.nom_processus')
                    ->join('processuses','processuses.id','=','descentes.processus')
                    ->where('descentes.id','=',$item->descente)
                    ->first();
            $item->desc = $desc;
        }

        $ariane = ['riverin'];
        return view('admin.riverin.index', compact('riverin','ariane','descente'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $descente = Descente::select('descentes.id','processuses.nom_processus')
                    ->join('processuses','processuses.id','=','descentes.processus')
                    ->get();
        $ariane = ['riverin','Ajouter'];
        return view('admin.riverin.create',compact('ariane','descente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'noms' => 'required',
            'descente' => 'required|exists:descentes,id',
            'status' => 'required|in:Riverin,Notable'
		]);
        $requestData = $request->all();
        
        $riverin = Riverin::create($requestData);

        return redirect('admin/riverin')->with('flash_message', 'Participant Ajouter Avec Succes!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $riverin = Riverin::findOrFail($id);
        $ariane = ['riverin','Details'];
        $descente = Descente::select('descentes.*','processuses.nom_processus as noms')
                        ->join('processuses','processuses.id','=','descentes.processus')
                        ->where('descentes.id','=',$riverin->descente)
                        ->first();
        return view('admin.riverin.show', compact('riverin','ariane','descente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $riverin = Riverin::findOrFail($id);
        $ariane = ['riverin','Modification'];
        $descente = Descente::select('descentes.id','processuses.nom_processus')
                    ->join('processuses','processuses.id','=','descentes.processus')
                    ->get();

        return view('admin.riverin.edit', compact('riverin','ariane','descente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'noms' => 'required',
            'descente' => 'required|exists:descentes,id',
            'status' => 'required|in:Riverin,Notable'
		]);
        $requestData = $request->all();
        
        $riverin = Riverin::findOrFail($id);
        $riverin->update($requestData);

        return redirect('admin/riverin')->with('flash_message', 'Participant Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Riverin::destroy($id);

       return response()->json(['status'=>'Participant Supprimer Avec Succes','type'=>'success']);
    }


}
