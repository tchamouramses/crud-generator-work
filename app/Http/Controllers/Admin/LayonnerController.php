<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Layonner;
use Illuminate\Http\Request;
use App\Processu;
use App\Layonnage;
use App\PersonLayonage;

class LayonnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $layonner = Layonner::where('date_debut', 'LIKE', "%$keyword%")
                ->orWhere('processus', 'LIKE', "%$keyword%")
                ->orWhere('frais', 'LIKE', "%$keyword%")
                ->orWhere('duree', 'LIKE', "%$keyword%")
                ->orWhere('Surface', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $layonner = Layonner::latest()->paginate($perPage);
        }

        foreach ($layonner as $item) {
            # code...
            $proc = Processu::find($item->processus);
            $item->proc = $proc;
        }

        $ariane = ['layonner'];
        return view('admin.layonner.index', compact('layonner','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $processus = Processu::whereIsFinish(0)->get();
        $layon = Layonnage::all();
        $processus->layon = $layon;
        $ariane = ['layonner','Ajouter'];
        return view('admin.layonner.create',compact('ariane','processus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'processus' => 'required|exists:processuses,id',
			'date_debut' => 'required',
			'frais' => 'required',
			'duree' => 'required',
            'Surface' => 'required',
            'layonnages' => 'required'
		]);
        $requestData = $request->all();
        
        $layon = Layonner::create($requestData);

        $layonner = $request->layonnages;
        if(!empty($layonner)){
        for ($i=0; $i < count($layonner); $i++) {
            $lay = new PersonLayonage();
            $lay->layonner = $layon->id;
            $lay->layonnage = $layonner[$i];
            $lay->save();
         } 
        }

        return redirect('admin/layonner')->with('flash_message', 'Layonnage Ajouter Avec Succes!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $layonner = Layonner::findOrFail($id);
        $ariane = ['layonner','Details'];
        $processus = Processu::findOrFail($layonner->processus);

        $layonneur = PersonLayonage::select('person_layonages.*','layonnages.noms_Layonneur','layonnages.telephone__Layonneur1')
            ->join('layonnages','layonnages.id','=','person_layonages.layonnage')
            ->where('person_layonages.layonner','=',$id)
            ->get();
        return view('admin.layonner.show', compact('layonner','ariane','processus','layonneur'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $layonner = Layonner::findOrFail($id);
        $ariane = ['layonner','Modification'];
        $processus = Processu::whereIsFinish(0)->get();
        $layon = Layonnage::all();
        $processus->layon = $layon;
        return view('admin.layonner.edit', compact('layonner','ariane','processus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'processus' => 'required',
			'date_debut' => 'required',
			'frais' => 'numeric|min:0',
			'duree' => 'numeric|min:0',
			'Surface' => 'numeric|min:0'
		]);
        $requestData = $request->all();
        
        $layonner = $request->layonnages;
        $layon = Layonner::findOrFail($id);
        $layon->update($requestData);

        if(!empty($layonner)){
        for ($i=0; $i < count($layonner); $i++) {
            $lay = new PersonLayonage();
            $lay->layonner = $layon->id;
            $lay->layonnage = $layonner[$i];
            $lay->save();
         } 
        }

        return redirect('admin/layonner')->with('flash_message', 'Layonnage Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Layonner::destroy($id);

       return response()->json(['status'=>'Layonneur Supprimer Avec Succes','type'=>'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        $person = PersonLayonage::findOrFail($id);
        $layon = Layonner::select('processuses.*')
                ->join('processuses','processuses.id','=','layonners.processus')
                ->where('layonners.id','=',$person->layonner)
                ->first();
        if ($layon->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            PersonLayonage::destroy($id);
            return response()->json(['status'=>'PersonLayonage Supprimer Avec Succes','type'=>'success']);
        }
    }



}
