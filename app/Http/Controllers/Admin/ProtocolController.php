<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Protocol;
use Illuminate\Http\Request;
use App\Processu;
use App\Archive;
use Illuminate\Support\Carbon;

class ProtocolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $protocol = Protocol::where('date_signature', 'LIKE', "%$keyword%")
                ->orWhere('Cabinet_Notaire', 'LIKE', "%$keyword%")
                ->orWhere('clerc', 'LIKE', "%$keyword%")
                ->orWhere('Frais', 'LIKE', "%$keyword%")
                ->orWhere('Fichier_Signe', 'LIKE', "%$keyword%")
                ->orWhere('processus', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $protocol = Protocol::latest()->paginate($perPage);
        }

        foreach ($protocol as $item) {
            # code...
            $proc = Processu::find($item->processus);
            $item->proc = $proc;
        }

        $ariane = ['protocol'];
        return view('admin.protocol.index', compact('protocol','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $processus = Processu::whereIsFinish(0)->get();
        $ariane = ['protocol','Ajouter'];
        return view('admin.protocol.create',compact('ariane','processus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'date_signature' => 'required',
			'Cabinet_Notaire' => 'required',
			'Frais' => 'numeric|min:0',
			'processus' => 'required|exists:processuses,id'
		]);
        $requestData = $request->all();
                if ($request->hasFile('Fichier_Signe')) {
            $requestData['Fichier_Signe'] = $request->file('Fichier_Signe')
                ->store('uploads', 'public');
        }
        $protocol = Protocol::create($requestData);
    if ($request->hasFile('Fichier_Signe')) {
        $archive = new Archive();
        $archive->Nom_Fichier = "Rapport Du Protocol DU ".Carbon::now();
        $archive->fichier = $protocol->Fichier_Signe;
        $archive->Processus = $protocol->processus;
        $archive->save();
    }
        return redirect('admin/protocol')->with('flash_message', 'Protocol Ajouter Avec Succes!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $protocol = Protocol::findOrFail($id);
        $ariane = ['protocol','Details'];
        $processus = Processu::findOrFail($protocol->processus);

        return view('admin.protocol.show', compact('protocol','ariane','processus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $protocol = Protocol::findOrFail($id);
        $ariane = ['protocol','Modification'];
        $processus = Processu::whereIsFinish(0)->get();

        return view('admin.protocol.edit', compact('protocol','ariane','processus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'date_signature' => 'required',
			'Cabinet_Notaire' => 'required',
			'Frais' => 'numeric|min:0',
			'processus' => 'required|exists:processuses,id'
		]);
        $requestData = $request->all();
                if ($request->hasFile('Fichier_Signe')) {
            $requestData['Fichier_Signe'] = $request->file('Fichier_Signe')
                ->store('uploads', 'public');
        }

        $protocol = Protocol::findOrFail($id);
        $protocol->update($requestData);
    if ($request->hasFile('Fichier_Signe')) {
        $archive = new Archive();
        $archive->Nom_Fichier = "Nouveau Rapport Du Protocol DU ".Carbon::now();
        $archive->fichier = $protocol->Fichier_Signe;
        $archive->Processus = $protocol->processus;
        $archive->save();
    }
        return redirect('admin/protocol')->with('flash_message', 'Protocol Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $protocol = Protocol::findOrFail($id);

        $processus = Processu::findOrFail($protocol->processus);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            Protocol::destroy($id);
            return response()->json(['status'=>'Protocol Supprimer Avec Succes','type'=>'success']);
        }
    }


}
