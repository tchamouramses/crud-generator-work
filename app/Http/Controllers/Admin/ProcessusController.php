<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Processu;
use App\Site;
use App\Archive; 
use App\Geometre;
use App\Commission;
use App\Demarcheur;
use App\Facilitateur;
use App\PorGeometre;
use App\PorDemarcheur;
use App\Visite;
use App\PorFacilitateur;
use App\FacDerogation;
use App\Derogation;
use App\Demande;
use App\MembreDossier;
use App\Vente;
use App\MembreComission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProcessusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $processus = Processu::where('nom', 'LIKE', "%$keyword%")
                ->orWhere('Site', 'LIKE', "%$keyword%")
                ->orWhere('nom_processus', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $processus = Processu::latest()->paginate($perPage);
        }
        foreach ($processus as $item) {
            # code...
            $sites = Site::find($item->Site);
            $item->sites = $sites;
        }

        $ariane = ['processus'];
        return view('admin.processus.index', compact('processus','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $site = Site::all();
        $geometre = Geometre::all();
        $demarcheur = Demarcheur::all();
        $facilitateur = Facilitateur::all();
        $ariane = ['processus','ajout'];
        return view('admin.processus.create', compact('site','geometre','demarcheur','facilitateur','ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'nom' => 'required',
			'Site' => 'required|exists:sites,id',
            'nom_processus' => 'required|unique:processuses'
		]);
        $requestData = $request->all();

        
        $geometre = $request->geometre;
        $demarcheur = $request->demarcheur;
        $facilitateur = $request->facilitateur;


        $processus = Processu::create($requestData);

        if(!empty($geometre)){
        for ($i=0; $i < count($geometre); $i++) {
            $geo = new PorGeometre();
            $geo->processus = $processus->id;
            $geo->geometre = $geometre[$i];
            $geo->save();
         } 
        }
        if(!empty($demarcheur)){
           for ($i=0; $i < count($demarcheur); $i++) {
                $dem = new PorDemarcheur();
                $dem->processus = $processus->id;
                $dem->demarcheur = $demarcheur[$i];
                $dem->save();
            }  
        } 
        

        if(!empty($facilitateur)){
        for ($i=0; $i < count($facilitateur); $i++) { 
            $fac = new PorFacilitateur();
            $fac->processus = $processus->id;
            $fac->facilitateur = $facilitateur[$i];
            $fac->save();
         }
         } 
        return redirect('admin/processus')->with('flash_message', 'Processus Ajouter Avec Succes!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $processu = Processu::findOrFail($id);
        $geometre = PorGeometre::whereProcessus($id)->get();
        $facilitateur = PorFacilitateur::whereProcessus($id)->get();
        $demarcheur = PorDemarcheur::whereProcessus($id)->get();

        foreach ($geometre as $key) {
            # code...
            $geo = Geometre::find($key->geometre);
            $key->geo = $geo;
        }


        foreach ($demarcheur as $key) {
            # code...
            $dem = Demarcheur::find($key->demarcheur);
            $key->dem = $dem;
        }


        foreach ($facilitateur as $key) {
            # code...
            $fac = Facilitateur::find($key->facilitateur);
            $key->fac = $fac;
        }


        $processu->geometre = $geometre;
        $processu->demarcheur = $demarcheur;
        $processu->facilitateur = $facilitateur;

        $site = Site::findOrFail($processu->Site);

        $ariane = ['processus','details'];
        return view('admin.processus.show', compact('processu','site','ariane'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $processu = Processu::findOrFail($id);
        $site = Site::all();
        $geometre = Geometre::all();
        $demarcheur = Demarcheur::all();
        $facilitateur = Facilitateur::all();

        $ariane = ['processus','Modification'];
        return view('admin.processus.edit', compact('processu','site','geometre','demarcheur','facilitateur','ariane'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'nom' => 'required',
			'Site' => 'required|exists:sites,id',
            'nom_processus' => 'required'
		]);
        $requestData = $request->all();

        $geometre = $request->geometre;
        $demarcheur = $request->demarcheur;
        $facilitateur = $request->facilitateur;

        
        $processu = Processu::findOrFail($id);
        $processu->update($requestData);

        if(!empty($geometre)){
        for ($i=0; $i < count($geometre); $i++) {
            $geo = new PorGeometre();
            $geo->processus = $processu->id;
            $geo->geometre = $geometre[$i];
            $geo->save();
         } 
        }
        if(!empty($demarcheur)){
           for ($i=0; $i < count($demarcheur); $i++) {
                $dem = new PorDemarcheur();
                $dem->processus = $processu->id;
                $dem->demarcheur = $demarcheur[$i];
                $dem->save();
            }  
        }
        

        if(!empty($facilitateur)){
        for ($i=0; $i < count($facilitateur); $i++) { 
            $fac = new PorFacilitateur();
            $fac->processus = $processu->id;
            $fac->facilitateur = $facilitateur[$i];
            $fac->save();
         }
         } 

        return redirect('admin/processus/'.$processu->id)->with('flash_message', 'Processus Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $processus = Processu::findOrFail($id);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'ce Processus est deja cloturé','type'=>'error']);
        }else{
            Processu::destroy($id);
            return response()->json(['status'=>'Processu Supprimer Avec Succes','type'=>'success']);
        }
    }


/**
     * Show the archives for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function archive($id)
    {
        $processu = Processu::findOrFail($id);
        $archive = Archive::whereProcessus($id)->get();
        foreach ($archive as $key) {
            if(preg_match("/.pdf$/",$key->fichier)){
                $key->images = "assets/images/pdf.png";
            }elseif (preg_match("/.doc(x)?$/",$key->fichier)) {
                $key->images = "assets/images/word.png";
            }elseif (preg_match("/.xls(x)?$/",$key->fichier)) {
                $key->images = "assets/images/excel.png";
            }elseif (preg_match("/.(jpg|jpeg|png|gif)$/",$key->fichier)) {
                $key->images = "storage/".$key->fichier;
            }else{
                $key->images = "assets/images/file.png";
            }
        }
        $ariane = ['processus','processus/'.$id,'archive'];
        return view('admin.processus.archive', compact('processu','archive','ariane'));
    }

/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteDem($id)
    {
        $dem = PorDemarcheur::findOrFail($id);

        $processus = Processu::findOrFail($dem->processus);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            PorDemarcheur::destroy($id);
            return response()->json(['status'=>'Demarcheur Supprimer Avec Succes','type'=>'success']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteGeo($id)
    {
        $dem = PorGeometre::find($id);
        $processus = Processu::findOrFail($dem->processus);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            PorGeometre::destroy($id);
            return response()->json(['status'=>'Geometre Supprimer Avec Succes','type'=>'success']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteFac($id)
    {
        $dem = PorFacilitateur::find($id);
        $processus = Processu::findOrFail($dem->processus);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            PorFacilitateur::destroy($id);
            return response()->json(['status'=>'Facilitateur Supprimer Avec Succes','type'=>'success']);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function commission($id)
    {
        $processus = Processu::find($id);
        $commission = Commission::whereProcessus($id)->get();
        $ariane = ['processus','processus/'.$id,'commission'];
        return view('admin.processus.commission', compact('processus','commission','ariane'));
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createcommission(Request $request)
    {
        $this->validate($request, [
            'processus' => 'required|exists:processuses,id'
        ]);
        $requestData = $request->all();
        $id = $request->processus;
        Commission::create($requestData);

        return redirect('admin/processus/'.$id.'/commission')->with('flash_message', 'Commission added!');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function visite($id)
    {
        $processus = Processu::find($id);
        $visite = Visite::whereProcessus($id)->get();
        $ariane = ['processus','processus/'.$id,'visite'];
        return view('admin.processus.visite', compact('processus','visite','ariane'));
    }





/**
     * Show the form for creating a new resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function derogation($id)
    {

        $facilitateur = Facilitateur::all();
        $processus = Processu::findOrFail($id);
        $derogation = Derogation::whereProcessus($id)->first();
        $dero = [];
        if($derogation){
            $dero = FacDerogation::select('facilitateurs.*','fac_derogations.id as dero')
                ->join('facilitateurs','facilitateurs.id','=','fac_derogations.facilitateur')
                ->where(['fac_derogations.derogation' => $derogation->id])
                ->get();
        }
        $ariane = ['processus','processus/'.$id,'derogation'];
        return view('admin.processus.derogation', compact('processus','derogation','dero','ariane','facilitateur'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deletefacdero($id)
    {
        $dem = FacDerogation::find($id);
        $dero = Derogation::findOrFail($dem->derogation);
        $processus = Processu::findOrFail($dero->processus);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            FacDerogation::destroy($id);
            return response()->json(['status'=>'Facilitateur Supprimer Avec Succes','type'=>'success']);
        }
    }


 /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deletedero($id)
    {
        $dero = Derogation::findOrFail($id);
        $processus = Processu::findOrFail($dero->processus);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            Derogation::destroy($id);
            return response()->json(['status'=>'Derogation Supprimée Avec Succes','type'=>'success']);
        }
    }


/**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function demande($id)
    {
        $processus = Processu::findOrFail($id);
        $demande = Demande::whereProcessus($id)->get();
        $ariane = ['processus','processus/'.$id,'demande'];
        return view('admin.processus.demande',compact('processus','demande','ariane'));
    }


 /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deletedemande($id)
    {
        $dero = Demande::findOrFail($id);
        $processus = Processu::findOrFail($dero->processus);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            Demande::destroy($id);
            return response()->json(['status'=>'Demande Supprimée Avec Succes','type'=>'success']);
        }
    }


 /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function deletedossier($id)
    {
        MembreDossier::destroy($id);

       return response()->json(['status'=>'Dossier Supprimer Avec Succes','type'=>'success']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function dossier($id)
    {
        $dossier = MembreDossier::select('membre_dossiers.*','membre_comissions.noms','membre_comissions.Contact')
                    ->join('membre_comissions','membre_comissions.id','=','membre_dossiers.membre')
                    ->where('membre_dossiers.processus','=',$id)
                    ->get();
        $processus = Processu::findOrFail($id);
        $admin = MembreComission::whereStatus('DOSSIER_TECHNIQUE')  
                ->orWhere('status','=','DEUX')
                ->get();
        $ariane = ['processus','processus/'.$id,'Frais dossier'];
        return view('admin.processus.dossier', compact('ariane','dossier','admin','processus'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createdossier(Request $request)
    {
        $this->validate($request, [
            'frais' => 'required',
            'processus' => 'required|exists:processuses,id',
            'membre' => 'required|exists:membre_comissions,id'
        ]);

        $requestData = $request->all();
        $res = MembreDossier::create($requestData);
        return redirect('admin/processus/'.$res->processus.'/dossier')->with('flash_message', 'Frais Ajouter Avec Succes!');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createarchive(Request $request)
    {
        $this->validate($request, [
            'processus' => 'required|exists:processuses,id',
            'fichier' => 'required'
        ]);

        $requestData = $request->all();
        $val = 0;
        if(!isset($requestData['Nom_Fichier']))
            $val = 1;

        if(!empty($request->fichier)){
            for ($i=0; $i < count($request->fichier); $i++) { 
                if($val == 1){
                    $requestData['Nom_Fichier'] = $request->file('fichier')[$i]->getClientOriginalName();
                }
                $requestData['fichier'] = $request->file('fichier')[$i]
                    ->store('uploads', 'public');
                Archive::create($requestData);
            }
        }

        return redirect('/admin/processus/'.$request->processus.'/archive')->with('flash_message', 'Archive Ajouter Avec Succes!');
    }



    public function createderogation(Request $request)
    {
        # code...
        $this->validate($request, [
            'date_derogation' => 'required',
            'frais' => 'required',
            'processus' => 'required|exists:processuses,id'
        ]);
        $requestData = $request->all();

        $facilitateur = $request->facilitateur;
        $dero = Derogation::create($requestData);

        if(!empty($facilitateur)){
        for ($i=0; $i < count($facilitateur); $i++) {
            $geo = new FacDerogation();
            $geo->derogation = $dero->id;
            $geo->facilitateur = $facilitateur[$i];
            $geo->save();
         } 
        }

        return redirect('admin/processus/'.$dero->processus.'/derogation')->with('flash_message', 'Derogation Ajouter Avec Succes!');
    }

    public function createdemande(Request $request)
    {
        # code...
        $this->validate($request, [
            'date_demande' => 'required',
            'frais' => 'required',
            'nature' => 'required',
            'processus' => 'required|exists:processuses,id'
        ]);
        $requestData = $request->all();
        
        $demande = Demande::create($requestData);

        return redirect('admin/processus/'.$demande->processus.'/demande')->with('flash_message', 'Demande Ajouter Avec Succes!');
    }


    public function closeprocess($id)
    {
        # code...
        $processus = Processu::findOrFail($id);
        $val = 1;
        $message = 'processus cloturé avec succes';
        if($processus->is_finish){
            $val = 0;
            $message = 'processus ouvert avec succes';
        }

        $value = [
            'is_finish' => $val
        ];

        if(Auth::user()->admin == 'ADMIN'){
            $processus->update($value);
            return redirect('/admin/processus')->with('flash_message', $message);
        }else{
            return back()->with('error_message', 'Vous n\'êtes pas habilité a cloturé ou ouvrir un processus');
        }
    }


    public function vente($id)
    {
        # code...
        $vente = Vente::select('ventes.*','processuses.nom_processus','processuses.is_finish','processuses.id as proces')
                    ->join('processuses','processuses.id','=','ventes.processus')
                    ->where('ventes.processus','=',$id)
                    ->first();
        $processus = Processu::findOrFail($id);
        $ariane = ['processus','processus/'.$id,'Vente Ou Achat'];
        return view('admin.processus.vente', compact('ariane','vente','processus'));
    }

    public function deletevente($id)
    {
        # code...
        $vente = Vente::findOrFail($id);
        $processus = Processu::findOrFail($vente->processus);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            Vente::destroy($id);
            return response()->json(['status'=>'Vente Ou Achat Supprimé Avec Succes','type'=>'success']);
        }

    }

    public function createvente(Request $request)
    {
        # code...
        $this->validate($request, [
            'date_vente' => 'required',
            'frais' => 'required',
            'nature' => 'required',
            'processus' => 'required|exists:processuses,id'
        ]);
        $requestData = $request->all();
        
        $vente = Vente::create($requestData);

        return redirect('admin/processus/'.$vente->processus.'/vente')->with('flash_message', 'Vente Ajoutée Avec Succes!');
    }

    public function updatevente(Request $request, $id)
    {
        # code...
        $vente = Vente::findOrFail($id);
        $this->validate($request, [
            'date_vente' => 'required',
            'frais' => 'required',
            'nature' => 'required|in:Vente,Achat',
            'processus' => 'required|exists:processuses,id'
        ]);
        $requestData = $request->all();
        $vente->update($requestData);

        return redirect('admin/processus/'.$vente->processus.'/vente')->with('flash_message', 'Vente Modifiée Avec Succes!');

    }
}
