<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Bornage;
use App\Processu;
use App\Geometre;
use Illuminate\Http\Request;

class BornageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $bornage = Bornage::where('Geometre', 'LIKE', "%$keyword%")
                ->orWhere('frais', 'LIKE', "%$keyword%")
                ->orWhere('date_bornage', 'LIKE', "%$keyword%")
                ->orWhere('processus', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $bornage = Bornage::latest()->paginate($perPage);
        }

        foreach ($bornage as $item) {
            # code...
            $born = Processu::find($item->processus);
            $geo = Geometre::find($item->Geometre);
            $item->born = $born;
            $item->geo = $geo;
        }

        $ariane = ['bornage'];
        return view('admin.bornage.index', compact('bornage','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $processus = Processu::whereIsFinish(0)->get();
        $geometre = Geometre::all();
        $ariane = ['bornage','Ajout'];
        return view('admin.bornage.create',compact('processus','geometre','ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'Geometre' => 'required',
			'frais' => 'numeric|min:0',
			'date_bornage' => 'required',
			'processus' => 'required|exists:processuses,id'
		]);
        $requestData = $request->all();
        
        Bornage::create($requestData);

        return redirect('admin/bornage')->with('flash_message', 'Bornage Ajouter Avec Succes!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $bornage = Bornage::findOrFail($id);
        $geo = Geometre::findOrFail($bornage->Geometre);
        $ariane = ['bornage','details'];
        $processus = Processu::findOrFail($bornage->processus);
        return view('admin.bornage.show', compact('bornage','ariane','geo','processus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $bornage = Bornage::findOrFail($id);
        $processus = Processu::whereIsFinish(0)->get();;
        $geometre = Geometre::all();
        $ariane = ['bornage','Modification'];
        return view('admin.bornage.edit', compact('bornage','processus','geometre','ariane'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'Geometre' => 'required',
			'frais' => 'numeric|min:0',
			'date_bornage' => 'required',
			'processus' => 'required|exists:processuses,id'
		]);
        $requestData = $request->all();
        
        $bornage = Bornage::findOrFail($id);
        $bornage->update($requestData);

        return redirect('admin/bornage')->with('flash_message', 'Bornage Modifié Avec Succes!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $bornage = Bornage::findOrFail($id);

        $processus = Processu::findOrFail($bornage->processus);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            Bornage::destroy($id);
            return response()->json(['status'=>'Bornage Supprimer Avec Succes','type'=>'success']);
        }
    }


}
