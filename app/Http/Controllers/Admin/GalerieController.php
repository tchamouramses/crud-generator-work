<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Galerie;
use Illuminate\Http\Request;
use App\Site;

class GalerieController extends Controller
{
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $site = Site::all();

        $ariane = ['galerie','ajout'];
        return view('admin.galerie.create', compact('site','ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'image' => 'required',
			'id_visite' => 'required'
		]);
        $requestData = $request->all();
                if ($request->hasFile('image')) {
            $requestData['image'] = $request->file('image')
                ->store('uploads', 'public');
        }

        $galerie = Galerie::create($requestData);

        return redirect('admin/site/'.$galerie->id_visite.'/galerie')->with('flash_message', 'Image Crée avec Succes');
    }

 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $galerie = Galerie::findOrFail($id);
        Galerie::destroy($id);
        return response()->json(['status'=>'Image Supprimer Avec Succes','type'=>'success']);
    }
}
