<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\MembreComission;
use Illuminate\Http\Request;
use App\Processu;

class MembreComissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $membrecomission = MembreComission::where('noms', 'LIKE', "%$keyword%")
                ->orWhere('Contact', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $membrecomission = MembreComission::latest()->paginate($perPage);
        }
        $ariane = ['membre-comission'];
        return view('admin.membre-comission.index', compact('membrecomission','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $processus = Processu::whereIsFinish(0)->get();
        $ariane = ['membre-comission','Ajouter'];
        return view('admin.membre-comission.create',compact('ariane','processus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'noms' => 'required',
            'status' => 'required|in:DESCENTE,DOSSIER_TECHNIQUE,DEUX'
		]);
        $requestData = $request->all();
        
        MembreComission::create($requestData);

        return redirect('admin/membre-comission')->with('flash_message', 'MembreComission Ajouter Avec Succes!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $membrecomission = MembreComission::findOrFail($id);
        $ariane = ['membre-comission','Details'];
        $processus = Processu::findOrFail($membrecomission->processus);

        return view('admin.membre-comission.show', compact('membrecomission','ariane','processus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $membrecomission = MembreComission::findOrFail($id);
        $ariane = ['membre-comission','Modification'];
        $processus = Processu::whereIsFinish(0)->get();

        return view('admin.membre-comission.edit', compact('membrecomission','ariane','processus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'noms' => 'required',
            'status' => 'required|in:DESCENTE,DOSSIER_TECHNIQUE,DEUX'
		]);
        $requestData = $request->all();
        
        $membrecomission = MembreComission::findOrFail($id);
        $membrecomission->update($requestData);

        return redirect('admin/membre-comission')->with('flash_message', 'MembreComission Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     */
    public function destroy($id)
    {

        MembreComission::destroy($id);

        return response()->json(['status'=>'Membre De Comission Supprimer avec Succes','type'=>'success']);
    }


}
