<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Site;
use App\Galerie;
use App\Proprietaire;
use App\PropSite;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $site = Site::where('region', 'LIKE', "%$keyword%")
                ->orWhere('departement', 'LIKE', "%$keyword%")
                ->orWhere('arrondissement', 'LIKE', "%$keyword%")
                ->orWhere('village', 'LIKE', "%$keyword%")
                ->orWhere('quartier', 'LIKE', "%$keyword%")
                ->orWhere('surface', 'LIKE', "%$keyword%")
                ->orWhere('situation_Geopraphique', 'LIKE', "%$keyword%")
                ->orWhere('situation_De_Ville', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $site = Site::latest()->paginate($perPage);
        }

        $ariane = ['site'];
        return view('admin.site.index', compact('site','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $proprietaire = Proprietaire::all();
        $val = [new PropSite()];
        $ariane = ['site','ajout'];
        return view('admin.site.create',compact('proprietaire','val','ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'region' => 'required',
			'departement' => 'required',
			'arrondissement' => 'required',
			'village' => 'required',
			'quartier' => 'required',
			'surface' => 'required',
			'situation_Geopraphique' => 'required',
			'situation_De_Ville' => 'required'
		]);
        $requestData = $request->all();
        $proprietaire = $request->proprietaire;
        $site = Site::create($requestData);

        if(!empty($request->file)){
            for ($i=0; $i < count($request->file); $i++) { 
                $gal = new Galerie();
                $gal->image = $request->file('file')[$i]
                    ->store('uploads', 'public');
                $gal->id_visite = $site->id;
                $gal->save();
            }
        }

        if(!empty($proprietaire)){
           for ($i=0; $i < count($proprietaire); $i++) {
                $prop = new PropSite();
                $prop->site = $site->id;
                $prop->proprietaire = $proprietaire[$i];
                $prop->save();
            }  
        }

        return redirect('admin/site')->with('flash_message', 'Site Ajouter Avec Succes!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $site = Site::findOrFail($id);
        $proprietaire = PropSite::whereSite($site->id)->get();
        foreach ($proprietaire as $item) {
            # code...
            $prop = Proprietaire::find($item->proprietaire);
            $item->prop = $prop;
        }

        $ariane = ['site','details'];
        return view('admin.site.show', compact('site','proprietaire','ariane'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $site = Site::findOrFail($id);
        $proprietaire = Proprietaire::all();
        $val = PropSite::whereSite($id)->get();

        $ariane = ['site','Modification'];
        return view('admin.site.edit', compact('site','proprietaire','val','ariane'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'region' => 'required',
			'departement' => 'required',
			'arrondissement' => 'required',
			'village' => 'required',
			'quartier' => 'required',
			'surface' => 'required',
			'situation_Geopraphique' => 'required',
			'situation_De_Ville' => 'required'
		]);
        $requestData = $request->all();
        $proprietaire = $request->proprietaire;
        
        $site = Site::findOrFail($id);
        $site->update($requestData);

        if(!empty($proprietaire)){
           for ($i=0; $i < count($proprietaire); $i++) {
                $prop = new PropSite();
                $prop->site = $site->id;
                $prop->proprietaire = $proprietaire[$i];
                $prop->save();
            }  
        }

        return redirect('admin/site')->with('flash_message', 'Site Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Site::destroy($id);

        return response()->json(['status'=> 'Site Supprimé avec succes','type'=>'success']);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function galerie($id)
    {
        $site = Site::findOrFail($id);
        $ariane = ['site','site/'.$id,'galerie'];
        $galerie = Galerie::whereIdVisite($id)->get();
        return view('admin.site.galerie', compact('galerie','site','ariane'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteProp($id)
    {
        $prop = PropSite::findOrFail($id);
        PropSite::destroy($id);
        return response()->json(['status'=> 'Proprietaire Supprimé avec succes','type'=>'success']);
    }

    


}
