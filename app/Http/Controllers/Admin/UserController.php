<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 5;

        if (!empty($keyword)) {
            $user = User::where('name', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('password', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $user = User::latest()->paginate($perPage);
        }

        $ariane = ['user'];
        return view('admin.user.index', compact('user','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $ariane = ['user','ajout'];
        return view('admin.user.create',compact('ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required',
			'email' => 'required|unique:users',
			'password' => 'required|confirmed',
            'ville' => 'required',
            'quartier' => 'required'
		]);
        $requestData = $request->all();
        $user = new User();

        if ($request->hasFile('profile')) {
            $user->profile = $request->file('profile')
                ->store('uploads/profile', 'public');
        }
        $user->name = $request->name;
        $user->email = $request->email;
        $user->ville = $request->ville;
        $user->quartier = $request->quartier;
        $user->telephone = $request->telephone;
        $user->password = bcrypt($request->password);
        $user->save();
        return redirect('admin/user')->with('flash_message', 'user Ajouter Avec Succes!');
        
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'ville' => 'required',
            'quartier' => 'required'
        ]);

        $user = User::findOrFail(Auth::user()->id);
        if ($request->hasFile('profile')) {
            $user->profile = $request->file('profile')
                ->store('uploads/profile', 'public');
        }
        $userup = array(
            'name' => $request->name,
            'email' => $request->email,
            'ville' => $request->ville,
            'quartier' => $request->quartier,
            'telephone' => $request->telephone,
            'profile' => $user->profile
        );
        $user->update($userup);
        return redirect('admin/user/profile')->with('flash_message', 'profile modifié avec succes!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        $ariane = ['user','details'];
        return view('admin.user.show', compact('user','ariane'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        return redirect('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user->admin == 'ADMIN') {
            # code...
            return response()->json(['status'=>'Cet Utilisateur Ne Peut etre Supprimer','type'=>'error']);
        }else{
            User::destroy($id);
            return response()->json(['status'=>'Utilisateur Supprimer Avec Succes','type'=>'success']);
        }
    }


    public function profile()
    {
        $user = new User();
        $user->name = Auth::user()->name;
        $user->email = Auth::user()->email;
        $user->ville = Auth::user()->ville;
        $user->quartier = Auth::user()->quartier;
        $user->telephone = Auth::user()->telephone;
        $user->profile = Auth::user()->profile;
        $ariane = ['profile'];
        return view('admin.user.profile', compact('user','ariane'));
    }



    
    public function updatepassword(Request $request)
    {
        $this->validate($request, [
            'oldpassword' => 'required',
            'password' => 'required|confirmed|min:8'
        ]);
        $user = User::findOrFail(Auth::user()->id);
        if(Hash::check($request->oldpassword, $user->password)){
            $userup = array(
                'password' => bcrypt($request->password)
            );
            $user->update($userup);
            return redirect('admin/user/profile')->with('flash_message', 'Mot De Passe Modifier Avec Succes');
        }else{
            return redirect('admin/user/profile')->with('error_message', 'echec de Modification du Mot de passe (Mot de passe actuel incorect)');
        }
    }

}
