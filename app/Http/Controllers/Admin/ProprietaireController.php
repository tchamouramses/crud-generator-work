<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Proprietaire;
use Illuminate\Http\Request;

class ProprietaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;

        if (!empty($keyword)) {
            $proprietaire = Proprietaire::where('noms', 'LIKE', "%$keyword%")
                ->orWhere('telephone', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $proprietaire = Proprietaire::latest()->paginate($perPage);
        }

        $ariane = ['proprietaire'];
        return view('admin.proprietaire.index', compact('proprietaire','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $ariane = ['proprietaire','ajout'];
        return view('admin.proprietaire.create',compact('ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'noms' => 'required',
			'telephone' => 'required'
		]);
        $requestData = $request->all();
        
        Proprietaire::create($requestData);

        return redirect('admin/proprietaire')->with('flash_message', 'Proprietaire Ajouter Avec Succes!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $proprietaire = Proprietaire::findOrFail($id);

        $ariane = ['proprietaire','details'];
        return view('admin.proprietaire.show', compact('proprietaire','ariane'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $proprietaire = Proprietaire::findOrFail($id);

        $ariane = ['proprietaire','Modification'];
        return view('admin.proprietaire.edit', compact('proprietaire','ariane'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'noms' => 'required',
			'telephone' => 'required'
		]);
        $requestData = $request->all();
        
        $proprietaire = Proprietaire::findOrFail($id);
        $proprietaire->update($requestData);

        return redirect('admin/proprietaire')->with('flash_message', 'Proprietaire Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Proprietaire::destroy($id);

        return response()->json(['status'=>'Proprietaire Supprimer Avec Succes','type'=>'success']);
    }
}
