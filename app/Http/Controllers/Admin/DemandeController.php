<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Demande;
use App\Processu;
use Illuminate\Http\Request;

class DemandeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 8;
        if (!empty($keyword)) {
            $demande = Demande::where('date_demande', 'LIKE', "%$keyword%")
                ->orWhere('frais', 'LIKE', "%$keyword%")
                ->orWhere('nature', 'LIKE', "%$keyword%")
                ->orWhere('processus', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $demande = Demande::latest()->paginate($perPage);
        }

        foreach ($demande as $item) {
            # code...
            $process = Processu::find($item->processus);
            $item->process = $process;
        }

        $ariane = ['demande'];
        return view('admin.demande.index', compact('demande','ariane'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $processus = Processu::whereIsFinish(0)->get();
        $demande = new Demande();
        $ariane = ['demande','ajout'];
        return view('admin.demande.create',compact('processus','demande','ariane'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'date_demande' => 'required',
			'frais' => 'numeric|min:0',
			'nature' => 'required',
			'processus' => 'required|exists:processuses,id'
		]);
        $requestData = $request->all();
        
        $demande = Demande::create($requestData);

        return redirect('admin/demande')->with('flash_message', 'Demande Ajouter Avec Succes!
Modifié Avec Succes!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $demande = Demande::findOrFail($id);
        $processus = Processu::findOrFail($demande->processus);
        $ariane = ['demande','details'];

        return view('admin.demande.show', compact('demande','ariane','processus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $demande = Demande::findOrFail($id);
        $processus = Processu::whereIsFinish(0)->get();
        $ariane = ['demande','Modification'];
        return view('admin.demande.edit', compact('demande','processus','ariane'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'date_demande' => 'required',
			'frais' => 'numeric|min:0',
			'nature' => 'required',
			'processus' => 'required|exists:processuses,id'
		]);
        $requestData = $request->all();
        
        $demande = Demande::findOrFail($id);
        $demande->update($requestData);

        return redirect('admin/demande')->with('flash_message', 'Demande Ajouter Avec Succes!
Modifié Avec Succes!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $demande = Demande::findOrFail($id);

        $processus = Processu::findOrFail($demande->processus);

        if ($processus->is_finish) {
            # code...
            return response()->json(['status'=>'Le Processus concerné est deja cloturé','type'=>'error']);
        }else{
            Demande::destroy($id);
            return response()->json(['status'=>'Demande Supprimer Avec Succes','type'=>'success']);
        }
    }


}
