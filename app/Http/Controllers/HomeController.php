<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Processu;
use App\Site;
use App\Facilitateur;
use App\Demarcheur;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $fac = count(Facilitateur::all());
        $dem = count(Demarcheur::all());
        $site = count(Site::all());
        $pro = count(Processu::all());
        $immatriculation = count(Processu::whereNom('Immatriculation')->get());
        $lotissement = count(Processu::whereNom('Lotissement')->get());
        $vente = count(Processu::whereNom('Vente_Achat')->get());
        $ariane = [];
        return view('admin.home',compact('fac','dem','site','pro','ariane','immatriculation','lotissement','vente'));;
    }
}
