<?php

namespace App\Http\Middleware;

use Closure;
use App\Processu;

class ControllAction
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $processus = Processu::find($request->id);
        if($processus->nom != 'Immatriculation'){
            return back()->with('error_message', 'Veuillez selectionner un processus d\'imatriculation');
        }
        return $next($request);
    }
}
