<?php

namespace App\Http\Middleware;

use Closure;
use App\Processu;

class ControllProcessFinish
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $processus = $request->processus;
        if(isset($processus)){
            $proces = Processu::findOrFail($processus);
            if ($proces->is_finish) {
                # code...
                // return Redirect::back()->with('message','Operation Successful !');
                return back()->with('error_message', 'Ce Processus a deja été cloturé');
            }
           
        }
        return $next($request); 
    }
}
