<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Processu extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'processuses';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nom_processus','nom', 'Site', 'Observation','is_finish'];

    
}
