<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'commissions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Commission_Facilitateurs', 'Commission_Demarcheur', 'Commission_Geometre', 'Autres_Commissions', 'processus'];

    
}
