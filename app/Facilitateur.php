<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facilitateur extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'facilitateurs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['noms_Facilitateur', 'telephone_Facilitateur1', 'telephone_Facilitateur2', 'email_Facilitateur'];

    
}
