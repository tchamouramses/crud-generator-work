<?php 

namespace App\Providers;

use App\Demande;
use App\Visite;
use App\Descente;
use App\Notification;
use Illuminate\Support\Carbon;


class HelpersServiceProvider extends AppServiceProvider
{


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once __DIR__ . '/HelpersServiceProvider.php';
    }


    function getNotification ()
    {
	   $notif = new Notification();

        $dem = Demande::select('demandes.*')
                    ->where('demandes.date_demande','>=',Carbon::today())
                    ->where('demandes.date_demande','<=', Carbon::today()->addDays(2))
                    ->get();
        $notif->demandes = count($dem);

        $visites = Visite::select('visites.*')
                    ->where('visites.Date_Visite','>=',Carbon::today())
                    ->where('visites.Date_Visite','<=', Carbon::today()->addDays(2))
                    ->get();
        $notif->visites = count($visites);

        $descentes = Descente::select('descentes.*')
                    ->where('descentes.date_descente','>=',Carbon::today())
                    ->where('descentes.date_descente','<=', Carbon::today()->addDays(2))
                    ->get();
        $notif->descentes = count($descentes);

        $notif->total = count($dem) + count($visites) + count($descentes);


	   return $notif;
    }

}

