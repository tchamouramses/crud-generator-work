<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Riverin extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'riverins';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['noms', 'telephone', 'descente', 'status'];

    
}
