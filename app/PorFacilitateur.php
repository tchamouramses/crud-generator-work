<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PorFacilitateur extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'Por_facilitateur';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['processus', 'facilitateur'];
    
}
