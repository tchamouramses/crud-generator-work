<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Layonner extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'layonners';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['date_debut', 'processus', 'frais', 'duree', 'Surface'];

    
}
