<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reception extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'receptions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['date_de_reception', 'frais_de_reception', 'descente'];

    
}
