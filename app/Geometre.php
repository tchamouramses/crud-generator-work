<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Geometre extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'geometres';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['noms', 'telephone_geo1', 'telephone_geo2', 'email'];

    
}
