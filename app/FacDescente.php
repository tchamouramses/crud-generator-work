<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacDescente extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fac_descentes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['facilitateur', 'descente'];
}
