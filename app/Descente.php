<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Descente extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'descentes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['date_descente', 'Frais_Descente', 'Chef_De_Quartier', 'processus'];

    
}
