<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Protocol extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'protocols';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['date_signature', 'Cabinet_Notaire', 'clerc', 'Frais', 'Fichier_Signe', 'processus'];

    
}
