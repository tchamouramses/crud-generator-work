<?php

namespace App\Assistan;

use App\Demande;
use App\Visite;
use App\Descente;
use App\Notification;
use App\Derogation;
use App\Bornage;
use App\Protocol;
use App\Reception;
use App\Event;
use Illuminate\Support\Carbon;

class Assistan
{
    
    static function getNotification ()
    {
        $dure = 7;
      $notif = new Notification();

        $dem = Demande::select('demandes.*','processuses.nom_processus','processuses.nom','processuses.id as proces')
                    ->join('processuses','processuses.id','=','demandes.processus')
                    ->where('demandes.date_demande','>=',Carbon::today())
                    ->where('demandes.date_demande','<=', Carbon::today()->addDays($dure))
                    ->orderBy('demandes.date_demande','ASC')
                    ->get();
        $notif->demandes = $dem;

        $visites = Visite::select('visites.*','processuses.nom_processus','processuses.nom','processuses.id as proces')
                    ->join('processuses','processuses.id','=','visites.processus')
                    ->where('visites.Date_Visite','>=',Carbon::today())
                    ->where('visites.Date_Visite','<=', Carbon::today()->addDays($dure))
                    ->orderBy('visites.Date_Visite','ASC')
                    ->get();
        $notif->visites = $visites;

        $descentes = Descente::select('descentes.*','processuses.nom_processus','processuses.nom','processuses.id as proces')
                    ->join('processuses','processuses.id','=','descentes.processus')
                    ->where('descentes.date_descente','>=',Carbon::today())
                    ->where('descentes.date_descente','<=', Carbon::today()->addDays($dure))
                    ->orderBy('descentes.date_descente','ASC')
                    ->get();
        $notif->descentes = $descentes;

        $dero = Derogation::select('derogations.*','processuses.nom_processus','processuses.nom','processuses.id as proces')
                    ->join('processuses','processuses.id','=','derogations.processus')
                    ->where('derogations.date_derogation','>=',Carbon::today())
                    ->where('derogations.date_derogation','<=', Carbon::today()->addDays($dure))
                    ->orderBy('derogations.date_derogation','ASC')
                    ->get();
        $notif->derogations = $dero;

        $born = Bornage::select('bornages.*','processuses.nom_processus','processuses.nom','processuses.id as proces','geometres.noms as nom_geo','geometres.telephone_geo1 as tel')
                    ->join('processuses','processuses.id','=','bornages.processus')
                    ->join('geometres','geometres.id','=','bornages.Geometre')
                    ->where('bornages.date_bornage','>=',Carbon::today())
                    ->where('bornages.date_bornage','<=', Carbon::today()->addDays($dure))
                    ->orderBy('bornages.date_bornage','ASC')
                    ->get();
        $notif->bornages = $born;

        $proto = Protocol::select('protocols.*','processuses.nom_processus','processuses.nom','processuses.id as proces')
                    ->join('processuses','processuses.id','=','protocols.processus')
                    ->where('protocols.date_signature','>=',Carbon::today())
                    ->where('protocols.date_signature','<=', Carbon::today()->addDays($dure))
                    ->orderBy('protocols.date_signature','ASC')
                    ->get();
        $notif->protocols = $proto;


        $recep = Reception::select('receptions.*','descentes.id as descente')
                    ->join('descentes','descentes.id','=','receptions.descente')
                    ->where('receptions.date_de_reception','>=',Carbon::today())
                    ->where('receptions.date_de_reception','<=', Carbon::today()->addDays($dure))
                    ->orderBy('receptions.date_de_reception','ASC')
                    ->get();
                    foreach ($recep as $item) {
                        $processus = Descente::select('processuses.nom_processus')
                                    ->join('processuses','processuses.id','=','descentes.processus')
                                    ->where('descentes.id','=',$item->descente)
                                    ->first();
                        $item->processus = $processus;
                    }
        $notif->receptions = $recep;

        $notif->total = count($dem) + count($visites) + count($descentes) + count($born) + count($proto)+ count($dero)+ count($recep);

        $notif->duree = $dure;

     return $notif;
    }



    static function getCalendarEvent()
    {
        # code...

        $cal = Event::all();
        session()->put('cal' , $cal);
    }
}
