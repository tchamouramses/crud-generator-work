<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sites';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['region', 'departement', 'arrondissement', 'village', 'quartier', 'surface', 'situation_Geopraphique', 'situation_De_Ville', 'latitude', 'longitude'];

    
}
