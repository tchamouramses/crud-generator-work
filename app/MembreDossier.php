<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MembreDossier extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'membre_dossiers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['frais','membre', 'processus'];
}
