<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vente extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ventes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['date_vente', 'frais', 'nature', 'processus'];
}
