<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visite extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'visites';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Date_Visite', 'frais', 'rapport', 'surface', 'Nature_Du_Sol', 'Nature_Relief', 'observation', 'processus', 'Effectuer'];

    
}
