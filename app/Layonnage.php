<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Layonnage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'layonnages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['noms_Layonneur', 'telephone__Layonneur1', 'telephone__Layonneur2', 'email_Layonneur'];

    
}
