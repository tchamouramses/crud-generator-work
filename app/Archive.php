<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Archive extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'archives';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Nom_Fichier', 'fichier', 'processus', 'description','is_archive'];

    
}
