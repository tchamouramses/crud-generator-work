<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bornage extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bornages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Geometre', 'frais', 'date_bornage', 'processus'];

    
}
